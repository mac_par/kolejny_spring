use micro3;

drop table if exists categories_micro;
drop table if exists publishers_micro;
drop table if exists books_micro;


create table publishers_micro (
	id INT(11),
	name VARCHAR(45) NOT NULL,
	CONSTRAINT PK_PUBLISHER_ID PRIMARY KEY (id)
);

create table categories_micro (
	id INT(11),
	name VARCHAR(45) NOT NULL,
	CONSTRAINT PK_CATEGORY_ID PRIMARY KEY (id)
);

create table books_micro (
	id INT(11),
	title VARCHAR(45) NOT NULL,
	small_desc VARCHAR(200),
	long_desc VARCHAR(500),
	publisher_id INT(11),
	price DECIMAL (10,2),
	category_id INT(11),
	CONSTRAINT PK_BOOK_ID PRIMARY KEY (id)
);

alter table books_micro add CONSTRAINT FK_BOOK_PUBLISHER_ID FOREIGN KEY (publisher_id) REFERENCES publishers_micro(id);
alter table books_micro add CONSTRAINT FK_BOOK_CATEGORY_ID FOREIGN KEY (category_id) REFERENCES categories_micro(id);