package com.micro.commons.model;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookDTO {

    private Integer bookId;
    @NotNull(message = "Title can not be empty")
    private String title;
    @NotNull(message = "Small Description can not be empty")
    private String smallDesc;
    @NotNull(message = "Long Description can not be empty")
    private String longDesc;
    @NotNull(message = "Category can not be empty")
    private Integer category;
    @NotNull(message = "Publisher can not be empty")
    private Integer publisherId;
    @NotNull(message = "Price can not be Zero")
    private Double price;
    private String port;
}
