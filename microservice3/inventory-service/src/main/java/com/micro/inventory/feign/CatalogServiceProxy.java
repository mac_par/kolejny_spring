package com.micro.inventory.feign;

import com.micro.commons.model.BookDTO;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RibbonClient("catalog-service")
@FeignClient(name = "gateway-service", path = "/api/catalog")
public interface CatalogServiceProxy {
    @GetMapping("/get-book/{bookId}")
    ResponseEntity<BookDTO> getBookFromInventory(@PathVariable Integer bookId);
}
