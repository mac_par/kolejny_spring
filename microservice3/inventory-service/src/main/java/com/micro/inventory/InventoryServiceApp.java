package com.micro.inventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.micro.inventory")
public class InventoryServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(InventoryServiceApp.class, args);
    }
}
