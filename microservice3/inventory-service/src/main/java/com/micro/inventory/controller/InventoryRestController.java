package com.micro.inventory.controller;

import com.micro.commons.model.BookDTO;
import com.micro.inventory.feign.CatalogServiceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/inventory")
public class InventoryRestController {
    private CatalogServiceProxy catalogServiceProxy;

    @Autowired
    public InventoryRestController(CatalogServiceProxy catalogServiceProxy) {
        this.catalogServiceProxy = catalogServiceProxy;
    }

    @GetMapping("/get-inventory/{bookId}")
    ResponseEntity<BookDTO> getBookById(@PathVariable Integer bookId) {
        return catalogServiceProxy.getBookFromInventory(bookId);
    }
}
