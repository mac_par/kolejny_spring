use micro3;

drop table if exists inventory_history_micro;
drop table if exists inventory_micro;

create table inventory_micro(
	id INT(11),
	book_id INT(11),
	in_stock INT(5),
	CONSTRAINT PK_INVENTORY_ID PRIMARY KEY (id)
);

create table inventory_history_micro(
	id INT(11),
	book_id INT(11),
	in_ward INT(5),
	added_on DATETIME,
	CONSTRAINT PK_INVENTORY_HISTORY_ID PRIMARY KEY (id)
);