use micro3;

drop table if exists users_micro;
drop table if exists delivery_addresses_micro;

create table users_micro (
	id INT(11),
	email VARCHAR(45),
	mobile VARCHAR(10),
	password VARCHAR(500),
	username VARCHAR(45),
	CONSTRAINT PK_USER PRIMARY KEY (id)
);

create table delivery_addresses_micro (
	id INT(11),
	line1 VARCHAR(45),
	line2 VARCHAR(45),
	line3 VARCHAR(45),
	city VARCHAR(45),
	state VARCHAR(45),
	country VARCHAR(45),
	pincode INT(6),
	user_id INT(11),
	CONSTRAINT PK_DELIVERY_ADDRESS_ID PRIMARY KEY (id)
);

alter table delivery_addresses_micro add CONSTRAINT FK_USER_ID FOREIGN KEY (user_id) REFERENCES users_micro(id);