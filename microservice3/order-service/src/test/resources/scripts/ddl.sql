use micro3;

drop table if exists orders_micro;
drop table if exists order_items_micro;

create table orders_micro (
	id INT(11),
	user_id INT(11),
	order_date DATETIME,
	total_amount DECIMAL(10,2),
	shipping_address VARCHAR(300),
	CONSTRAINT PK_ORDER_ID PRIMARY KEY (id)
);

create table order_items_micro (
	id INT(11),
	book_id INT(11),
	quantity INT(5),
	price DECIMAL(10,2),
	order_id INT(11),
	CONSTRAINT PK_ORDER_ITEM_ID PRIMARY KEY (id)
);

alter table order_items_micro add CONSTRAINT FK_ITEM_TO_ORDER FOREIGN KEY (order_id) REFERENCES orders_micro(id);