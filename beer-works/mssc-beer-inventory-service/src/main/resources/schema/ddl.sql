create table beer_inventory(
id varchar2(36),
beer_id varchar2(36) not null,
upc varchar2(25) not null,
quantity_on_hand number(9) not null,
version number(9) default 1,
created_date timestamp not null,
last_modified_date timestamp not null,
constraint beer_inventory_pk primary key(id)
);