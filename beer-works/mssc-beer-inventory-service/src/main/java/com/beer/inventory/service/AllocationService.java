package com.beer.inventory.service;

import com.beer.commons.model.dto.BeerOrderDto;

public interface AllocationService {

    Boolean allocateOrder(BeerOrderDto beerOrderDto);

}
