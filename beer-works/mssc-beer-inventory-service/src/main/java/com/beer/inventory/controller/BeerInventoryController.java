package com.beer.inventory.controller;

import com.beer.commons.model.dto.BeerInventoryDto;
import com.beer.commons.model.dto.HttpStatusCode;
import com.beer.commons.model.exception.BreweryException;
import com.beer.commons.model.mappers.BeerInventoryMapper;
import com.beer.inventory.repository.BeerInventoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("api/v1/beer")
public class BeerInventoryController {
    private final BeerInventoryRepository repository;
    private final BeerInventoryMapper mapper;

    @Autowired
    public BeerInventoryController(BeerInventoryRepository repository, BeerInventoryMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @RequestMapping(path = "/{beerId}/inventory", method = RequestMethod.GET)
    public ResponseEntity<List<BeerInventoryDto>> listBeersById(@PathVariable UUID beerId) {
        log.info("Retrieving beers by id {}", beerId);
        return ResponseEntity.ok(repository.findAllByBeerId(beerId)
                .stream().map(mapper::convert).collect(Collectors.toList()));
    }

    @RequestMapping(path = "/inventory", method = RequestMethod.POST)
    public ResponseEntity<BeerInventoryDto> postBeerInventory(@RequestBody BeerInventoryDto beerInventoryDto) {
        Objects.requireNonNull(beerInventoryDto, "Beer Inventory Dto is required");
        log.info("Creating beer inventory {}", beerInventoryDto);
        if (repository.findByBeerId(beerInventoryDto.getBeerId()).isPresent()) {
            throw new BreweryException(HttpStatusCode.BAD_REQUEST, String.format("Beer %s does exist", beerInventoryDto.getBeerId()));
        }
        BeerInventoryDto savedEntity = mapper.convert(repository.save(mapper.convert(beerInventoryDto)));
        return ResponseEntity.ok(savedEntity);
    }
}
