package com.beer.inventory.service;

import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.commons.model.dto.BeerOrderLineDto;
import com.beer.inventory.repository.BeerInventoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service
public class AllocationServiceBean implements AllocationService {
    private final BeerInventoryRepository repository;

    @Autowired
    public AllocationServiceBean(BeerInventoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public Boolean allocateOrder(BeerOrderDto beerOrderDto) {
        log.info("Allocationg Order: {}", beerOrderDto.getId());
        AtomicInteger totalOrdered = new AtomicInteger();
        AtomicInteger totalAllocated = new AtomicInteger();
        beerOrderDto.getBeerOrderLines().forEach(line -> {
            if (isAllocationRequired(line)) {
                allocateBeerOrderLine(line);
            }

            totalOrdered.addAndGet(line.getOrderQuantity());
            totalAllocated.addAndGet(line.getQuantityAllocated());
        });
        log.info("Total Ordered: {}, Total allocated: {}", totalOrdered.get(), totalAllocated.get());
        return totalOrdered.get() == totalAllocated.get();
    }

    private void allocateBeerOrderLine(BeerOrderLineDto line) {
        repository.findAllByUpc(line.getUpc()).forEach(beerInventory -> {
            int qtyToAllocate = line.getOrderQuantity() - line.getQuantityAllocated();
            int inventory = beerInventory.getQuantityOnHand();

            if (inventory >= qtyToAllocate) {
                inventory = inventory - qtyToAllocate;
                line.setQuantityAllocated(line.getOrderQuantity());
                beerInventory.setQuantityOnHand(inventory);
            } else if (inventory > 0) {
                line.setQuantityAllocated(line.getQuantityAllocated() + inventory);
            }

            if (beerInventory.getQuantityOnHand() == 0) {
                repository.delete(beerInventory);
            }
        });

    }

    private boolean isAllocationRequired(BeerOrderLineDto lineDto) {
        return (lineDto.getOrderQuantity() - lineDto.getQuantityAllocated()) > 0;
    }
}
