package com.beer.inventory.service;

import com.beer.commons.model.domain.inventory.BeerInventory;
import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.inventory.repository.BeerInventoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DeallocationServiceBean implements DeallocationService {
    private final BeerInventoryRepository repository;

    @Autowired
    public DeallocationServiceBean(BeerInventoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public void deallocateOrder(BeerOrderDto beerOrderDto) {
        beerOrderDto.getBeerOrderLines().forEach(orderline -> {
            BeerInventory beerInventory = BeerInventory.builder()
                    .beerId(orderline.getBeerId())
                    .quantityOnHand(orderline.getQuantityAllocated())
                    .upc(orderline.getUpc())
                    .build();

            BeerInventory savedBeer = repository.save(beerInventory);
            log.debug("Saved inventory for beer UPC: {}, inventory: id {}", savedBeer.getUpc(), savedBeer.getId());
        });
    }
}
