package com.beer.inventory.config;

import com.beer.commons.model.BeerQueue;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import java.nio.charset.StandardCharsets;
import javax.jms.Queue;

@Configuration
public class JmsConfig {
    @Value("${inventory-service.jms.broker.username}")
    private String brokerUsername;
    @Value("${inventory-service.jms.broker.password}")
    private String brokerPassword;
    @Value("${inventory-service.jms.broker.url}")
    private String brokerUrl;

    @Bean
    MappingJackson2MessageConverter converter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setEncoding(StandardCharsets.UTF_8.name());
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    @Bean
    ActiveMQConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory(brokerUrl, brokerUsername, brokerPassword);
    }

    @Bean
    Queue inventoryServiceQueue() {
        return new ActiveMQQueue(BeerQueue.BEER_INVENTORY_SERVICE_QUEUE);
    }

    @Bean
    Queue deallocateQueue() {
        return new ActiveMQQueue(BeerQueue.DEALLOCATE_ORDER_QUEUE);
    }
}
