package com.beer.inventory.listener;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.events.AllocateOrderRequest;
import com.beer.commons.model.events.AllocateOrderResponse;
import com.beer.inventory.service.AllocationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class OrderAllocationListener {
    private final AllocationService allocationService;
    private final JmsTemplate jmsTemplate;

    @Autowired
    public OrderAllocationListener(AllocationService allocationService, JmsTemplate jmsTemplate) {
        this.allocationService = allocationService;
        this.jmsTemplate = jmsTemplate;
    }

    @JmsListener(destination = BeerQueue.ALLOCATE_ORDER_REQUEST_QUEUE)
    void processAllocationRequest(AllocateOrderRequest request) {
        log.info("Allocation Request received: {}", request);
        AllocateOrderResponse.AllocateOrderResponseBuilder builder = AllocateOrderResponse.builder().orderId(request.getBeerOrderDto().getId());
        try {
            boolean pendingAllocation = !allocationService.allocateOrder(request.getBeerOrderDto());
            builder.pendingInventory(pendingAllocation);
        } catch (Exception exception) {
            log.error("Exception occurred", exception);
            builder.allocationError(true);
        }
        AllocateOrderResponse response = builder.build();
        jmsTemplate.convertAndSend(BeerQueue.ALLOCATE_ORDER_RESULT_QUEUE, response);
    }
}
