package com.beer.inventory.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(basePackages = {"com.beer.inventory.repository"})
public class DatabaseConfig {

    private final DBConfigProperties dbConfig;
    private final ConnectionPoolConfig poolConfig;

    @Autowired
    public DatabaseConfig(DBConfigProperties dbConfig, ConnectionPoolConfig poolConfig) {
        this.dbConfig = dbConfig;
        this.poolConfig = poolConfig;
    }

    @Bean
    DataSource dataSource() {
        HikariConfig cpConfig = new HikariConfig();
        cpConfig.setDriverClassName(dbConfig.getDriver());
        cpConfig.setJdbcUrl(dbConfig.getUrl());
        cpConfig.setUsername(dbConfig.getUsername());
        cpConfig.setPassword(dbConfig.getPassword());
        cpConfig.setMinimumIdle(poolConfig.getMinIdle());
        cpConfig.setConnectionTimeout(poolConfig.getConnectionTimeout());
        cpConfig.setMaxLifetime(poolConfig.getMaxLifeTime());
        cpConfig.setMaximumPoolSize(poolConfig.getMaxPoolSize());
        cpConfig.setPoolName(poolConfig.getPoolName());
        return new HikariDataSource(cpConfig);
    }

    @Bean
    EntityManagerFactory entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter, Properties jpaProperties) {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource);
        emf.setJpaVendorAdapter(jpaVendorAdapter);
        emf.setJpaProperties(jpaProperties);
        emf.setPackagesToScan("com.beer.commons.model.domain.inventory");
        emf.afterPropertiesSet();
        return emf.getNativeEntityManagerFactory();
    }

    @Bean
    PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

    @Bean
    JpaVendorAdapter vendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }

    @Bean
    Properties jpaProperties(DBConfigProperties config) {
        Properties props = new Properties();
        config.setProperties(props);
        return props;
    }
}
