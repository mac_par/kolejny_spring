package com.beer.inventory.repository;

import com.beer.commons.model.domain.inventory.BeerInventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface BeerInventoryRepository extends JpaRepository<BeerInventory, UUID> {

    List<BeerInventory> findAllByBeerId(UUID beerId);

    Optional<BeerInventory> findByBeerId(UUID beerId);

    @Query("select i from BeerInventory i where i.upc = :upc")
    List<BeerInventory> findAllByUpc(@Param("upc") String upc);

}
