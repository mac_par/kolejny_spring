package com.beer.inventory.listener;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.commons.model.events.DeallocateOrderEvent;
import com.beer.inventory.service.DeallocationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DeallocateOrderListener {
    private final DeallocationService deallocationService;

    public DeallocateOrderListener(DeallocationService deallocationService) {
        this.deallocationService = deallocationService;
    }

    @JmsListener(destination = BeerQueue.DEALLOCATE_ORDER_QUEUE)
    public void processDeallocation(@Payload DeallocateOrderEvent event) {
        BeerOrderDto beerOrderDto = event.getBeerOrderDto();
        log.info("Invoking deallocation for order {}", beerOrderDto.getId());
        deallocationService.deallocateOrder(beerOrderDto);
    }
}
