package com.beer.inventory.listener;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.domain.inventory.BeerInventory;
import com.beer.commons.model.dto.BeerDto;
import com.beer.commons.model.events.NewInventoryEvent;
import com.beer.inventory.repository.BeerInventoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BeerNewInventoryListener {
    private final BeerInventoryRepository repository;

    public BeerNewInventoryListener(BeerInventoryRepository repository) {
        this.repository = repository;
    }

    @JmsListener(destination = BeerQueue.BEER_INVENTORY_SERVICE_QUEUE)
    void newInventoryEvent(NewInventoryEvent newInventoryEvent) {
        BeerDto beerDto = newInventoryEvent.getBeerDto();
        BeerInventory newInventory = BeerInventory.builder().beerId(beerDto.getId()).upc(beerDto.getUpc()).quantityOnHand(newInventoryEvent.getAmountToProduce()).build();
        log.info("Adding new item for beer id {}", beerDto.getId());
        repository.saveAndFlush(newInventory);
    }
}
