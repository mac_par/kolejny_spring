package com.beer.inventory.service;

import com.beer.commons.model.dto.BeerOrderDto;

public interface DeallocationService {
    void deallocateOrder(BeerOrderDto beerOrderDto);
}
