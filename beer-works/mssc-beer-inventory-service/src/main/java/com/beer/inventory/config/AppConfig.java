package com.beer.inventory.config;

import com.beer.commons.model.mappers.BeerInventoryMapper;
import com.beer.commons.model.mappers.BeerInventoryMapperBean;
import com.beer.commons.model.mappers.DatesMapper;
import com.beer.commons.model.mappers.DatesMapperBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    DatesMapper datesMapper() {
        return new DatesMapperBean();
    }

    @Bean
    BeerInventoryMapper beerInventoryMapper(DatesMapper datesMapper) {
        return new BeerInventoryMapperBean(datesMapper);
    }
}
