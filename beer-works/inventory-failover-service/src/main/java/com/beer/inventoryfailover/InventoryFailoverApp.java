package com.beer.inventoryfailover;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class InventoryFailoverApp {
    public static void main(String[] args) {
        SpringApplication.run(InventoryFailoverApp.class, args);
    }
}
