package com.beer.inventoryfailover.config;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import com.beer.inventoryfailover.handler.InventoryHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class RouterConfig {
    @Bean
    public RouterFunction<ServerResponse> inventoryRoute(InventoryHandler handler) {
        return route(GET("api/v1/failover/{beerId}/inventory").and(accept(MediaType.APPLICATION_JSON)), handler::listInventory);
    }
}
