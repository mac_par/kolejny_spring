package com.beer.inventoryfailover.handler;

import com.beer.commons.model.dto.BeerInventoryDto;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Component
public class InventoryHandler {

    public Mono<ServerResponse> listInventory(ServerRequest request) {
        String beerId = request.pathVariable("beerId");
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(Mono.just(List.of(getMockedResponseBody(beerId))), List.class);
    }

    private BeerInventoryDto getMockedResponseBody(String uuid) {
        return BeerInventoryDto.builder()
                .beerId(UUID.fromString(uuid))
                .id(UUID.randomUUID())
                .quantityOnHand(999)
                .createdDate(OffsetDateTime.now())
                .lastModifiedDate(OffsetDateTime.now())
                .build();
    }
}
