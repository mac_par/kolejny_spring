package com.beer.orderservice.testcomponents;

import static com.beer.orderservice.testcomponents.TestConstants.VALIDATION_PASSED;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.commons.model.events.BeerOrderValidationRequestEvent;
import com.beer.commons.model.events.BeerOrderValidationResponseEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class BeerOrderValidationTestListener {
    private final JmsTemplate jmsTemplate;

    @Autowired
    public BeerOrderValidationTestListener(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @JmsListener(destination = BeerQueue.BEER_VALIDATION_REQUEST_QUEUE)
    void processValidation(@Payload BeerOrderValidationRequestEvent request) {
        BeerOrderDto beerOrderDto = request.getBeerOrderDto();
        UUID orderId = beerOrderDto.getId();
        log.info("Receiving {} at validation test listener", orderId.toString());

        boolean validationPasses = VALIDATION_PASSED.equals(beerOrderDto.getCustomerRef());
        log.info("Validation Result for order id {}: {}", orderId, validationPasses);
        BeerOrderValidationResponseEvent response = BeerOrderValidationResponseEvent.builder()
                .validationResult(validationPasses)
                .orderId(orderId)
                .build();
        log.debug("Validation response: {}", response);
        jmsTemplate.convertAndSend(BeerQueue.BEER_VALIDATION_RESPONSE_QUEUE, response);
    }
}
