package com.beer.orderservice.testcomponents;

import static com.beer.orderservice.testcomponents.TestConstants.ALLOCATION_FAILED;
import static com.beer.orderservice.testcomponents.TestConstants.PARTIAL_ALLOCATION;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.commons.model.events.AllocateOrderRequest;
import com.beer.commons.model.events.AllocateOrderResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class BeerAllocationTestListener {
    private final JmsTemplate jmsTemplate;

    @Autowired
    public BeerAllocationTestListener(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @JmsListener(destination = BeerQueue.ALLOCATE_ORDER_REQUEST_QUEUE)
    void processAllocation(@Payload AllocateOrderRequest request) {
        log.debug("Allocation request for order {} was received", request.getBeerOrderDto().getId());
        BeerOrderDto beerOrderDto = request.getBeerOrderDto();
        beerOrderDto.getBeerOrderLines().forEach(line -> line.setQuantityAllocated(line.getOrderQuantity()));
        String allocationResult = beerOrderDto.getOrderStatusCallbackUrl();

        boolean pendingInventory = PARTIAL_ALLOCATION.equals(allocationResult);
        boolean allocationError = ALLOCATION_FAILED.equals(allocationResult);
        beerOrderDto.getBeerOrderLines().forEach(orderLine -> {
            if (pendingInventory) {
                orderLine.setQuantityAllocated(orderLine.getOrderQuantity() - 1);
            } else {
                orderLine.setQuantityAllocated(orderLine.getOrderQuantity());
            }
        });
        AllocateOrderResponse response = AllocateOrderResponse.builder()
                .orderId(request.getBeerOrderDto().getId())
                .pendingInventory(pendingInventory)
                .allocationError(allocationError)
                .build();

        jmsTemplate.convertAndSend(BeerQueue.ALLOCATE_ORDER_RESULT_QUEUE, response);
    }
}
