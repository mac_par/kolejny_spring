package com.beer.orderservice.service;

import static com.beer.orderservice.testcomponents.TestConstants.ALLOCATION_FAILED;
import static com.beer.orderservice.testcomponents.TestConstants.ALLOCATION_PASSED;
import static com.beer.orderservice.testcomponents.TestConstants.PARTIAL_ALLOCATION;
import static com.beer.orderservice.testcomponents.TestConstants.VALIDATION_FAILED;
import static com.beer.orderservice.testcomponents.TestConstants.VALIDATION_PASSED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.awaitility.Awaitility.await;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.domain.OrderStatus;
import com.beer.commons.model.domain.beer.BeerStyle;
import com.beer.commons.model.domain.customer.BeerOrder;
import com.beer.commons.model.domain.customer.BeerOrderLine;
import com.beer.commons.model.domain.customer.Customer;
import com.beer.commons.model.dto.BeerDto;
import com.beer.commons.model.events.AllocationFailedEvent;
import com.beer.orderservice.repository.BeerOrderRepository;
import com.beer.orderservice.repository.CustomerRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.jms.Queue;

@SpringBootTest
@ActiveProfiles(profiles = {"test"})
class BeerOrderManagerBeanIT {
    private static final ClientAndServer mockServer = new ClientAndServer("localhost", 9004, 9000);
    private static final String BEER_UPC_PATH = "/api/v1/beerUpc/";

    @Autowired
    private BeerOrderManager beerOrderManager;
    @Autowired
    private BeerOrderRepository repository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private JmsTemplate jmsTemplate;


    private Customer customer;

    @BeforeEach
    void setUp() {
        customer = customerRepository.saveAndFlush(Customer.builder().name("Marco Polo").build());
        mockServer.reset();
        await().timeout(2000, TimeUnit.MILLISECONDS).until(mockServer::hasStarted);
    }

    @AfterAll
    static void tearDown() {
        mockServer.stop(false);
        await().timeout(2000, TimeUnit.MILLISECONDS).until(() -> !mockServer.isRunning());
    }

    BeerOrder getBeerOrder(UUID beerId, String upc, String customerRef, String orderStatusCallbackUrl) {
        BeerOrder order = BeerOrder.builder().customer(customer).customerRef(customerRef).orderStatusCallbackUrl(orderStatusCallbackUrl).build();
        BeerOrderLine orderLine = BeerOrderLine.builder()
                .beerId(beerId)
                .upc(upc)
                .beerOrder(order)
                .orderQuantity(1)
                .build();

        Set<BeerOrderLine> lines = new HashSet<>();
        lines.add(orderLine);
        order.setBeerOrderLines(lines);

        return order;
    }

    @Test
    void newToAllocate() throws JsonProcessingException {
        //given
        String beerUpc = "12345";
        UUID beerId = UUID.randomUUID();
        BeerOrder beerOrder = getBeerOrder(beerId, beerUpc, VALIDATION_PASSED, ALLOCATION_PASSED);
        BeerDto beerDto = BeerDto.builder().id(beerId).upc(beerUpc).beerName("Marne piwo").quantityOnHand(15)
                .price(new BigDecimal("13.04")).beerStyle(BeerStyle.ALE).build();
        mockServer.when(HttpRequest.request("/api/v1/beerUpc/" + beerUpc).withMethod("GET"))
                .respond(HttpResponse.response(objectMapper.writeValueAsString(beerDto)).withStatusCode(200));

        //when
        BeerOrder newBeerOrder = beerOrderManager.newBeerOrder(beerOrder);
        assertThat(newBeerOrder).isNotNull();

        assertOrderStatus(newBeerOrder.getId(), OrderStatus.ALLOCATED);
    }

    @Test
    void newToPickuped() throws JsonProcessingException {
        //given
        String beerUpc = "123654";
        String beerName = "my-beer";
        UUID beerId = UUID.randomUUID();
        BeerOrder beerOrder = getBeerOrder(beerId, beerUpc, VALIDATION_PASSED, ALLOCATION_PASSED);
        BeerDto beerDto = BeerDto.builder().id(beerId).beerName(beerName).quantityOnHand(15).price(new BigDecimal("12.34")).beerStyle(BeerStyle.IPA).build();

        mockServer.when(HttpRequest.request(BEER_UPC_PATH + beerUpc).withMethod("GET"))
                .respond(HttpResponse.response(objectMapper.writeValueAsString(beerDto)).withStatusCode(200));
        //when
        BeerOrder newBeerOrder = beerOrderManager.newBeerOrder(beerOrder);
        assertThat(newBeerOrder).isNotNull();

        assertOrderStatus(newBeerOrder.getId(), OrderStatus.ALLOCATED);

        beerOrderManager.processOrderPickUp(beerOrder.getId());

        assertOrderStatus(beerOrder.getId(), OrderStatus.PICKED_UP);
    }

    @Test
    void newToValidationFailed() throws JsonProcessingException {
        //given
        String beerUpc = "12345";
        UUID beerId = UUID.randomUUID();
        BeerOrder beerOrder = getBeerOrder(beerId, beerUpc, VALIDATION_FAILED, ALLOCATION_PASSED);
        BeerDto beerDto = BeerDto.builder().id(beerId).upc(beerUpc).beerName("Marne piwo").quantityOnHand(15)
                .price(new BigDecimal("13.04")).beerStyle(BeerStyle.ALE).build();
        mockServer.when(HttpRequest.request("/api/v1/beerUpc/" + beerUpc).withMethod("GET"))
                .respond(HttpResponse.response(objectMapper.writeValueAsString(beerDto)).withStatusCode(200));

        //when
        BeerOrder newBeerOrder = beerOrderManager.newBeerOrder(beerOrder);
        assertThat(newBeerOrder).isNotNull();

        assertOrderStatus(newBeerOrder.getId(), OrderStatus.VALIDATION_EXCEPTION);
    }

    @Test
    void newToAllocationFailed() throws JsonProcessingException {
        //given
        String beerUpc = "123654";
        String beerName = "my-beer";
        UUID beerId = UUID.randomUUID();
        BeerOrder beerOrder = getBeerOrder(beerId, beerUpc, VALIDATION_PASSED, ALLOCATION_FAILED);
        BeerDto beerDto = BeerDto.builder().id(beerId).beerName(beerName).quantityOnHand(15).price(new BigDecimal("12.34")).beerStyle(BeerStyle.IPA).build();

        mockServer.when(HttpRequest.request(BEER_UPC_PATH + beerUpc).withMethod("GET"))
                .respond(HttpResponse.response(objectMapper.writeValueAsString(beerDto)).withStatusCode(200));
        //when
        BeerOrder newBeerOrder = beerOrderManager.newBeerOrder(beerOrder);
        assertThat(newBeerOrder).isNotNull();

        assertOrderStatus(newBeerOrder.getId(), OrderStatus.ALLOCATION_EXCEPTION);
        jmsTemplate.receiveAndConvert(BeerQueue.ALLOCATE_ORDER_FAILED_QUEUE);
    }

    @Test
    void newToPartialAllocation() throws JsonProcessingException {
        //given
        String beerUpc = "123654";
        String beerName = "my-beer";
        UUID beerId = UUID.randomUUID();
        BeerOrder beerOrder = getBeerOrder(beerId, beerUpc, VALIDATION_PASSED, PARTIAL_ALLOCATION);
        BeerDto beerDto = BeerDto.builder().id(beerId).beerName(beerName).quantityOnHand(15).price(new BigDecimal("12.34")).beerStyle(BeerStyle.IPA).build();

        mockServer.when(HttpRequest.request(BEER_UPC_PATH + beerUpc).withMethod("GET"))
                .respond(HttpResponse.response(objectMapper.writeValueAsString(beerDto)).withStatusCode(200));
        //when
        BeerOrder newBeerOrder = beerOrderManager.newBeerOrder(beerOrder);
        assertThat(newBeerOrder).isNotNull();

        assertOrderStatus(newBeerOrder.getId(), OrderStatus.PENDING_INVENTORY);
    }

    @Test
    void onFailedAllocationMessageIsSent() throws JsonProcessingException {
        //given
        String beerUpc = "123654";
        String beerName = "my-beer";
        UUID beerId = UUID.randomUUID();
        BeerOrder beerOrder = getBeerOrder(beerId, beerUpc, VALIDATION_PASSED, ALLOCATION_FAILED);
        BeerDto beerDto = BeerDto.builder().id(beerId).beerName(beerName).quantityOnHand(15).price(new BigDecimal("12.34")).beerStyle(BeerStyle.IPA).build();

        mockServer.when(HttpRequest.request(BEER_UPC_PATH + beerUpc).withMethod("GET"))
                .respond(HttpResponse.response(objectMapper.writeValueAsString(beerDto)).withStatusCode(200));
        //when
        BeerOrder newBeerOrder = beerOrderManager.newBeerOrder(beerOrder);
        assertThat(newBeerOrder).isNotNull();

        assertOrderStatus(newBeerOrder.getId(), OrderStatus.ALLOCATION_EXCEPTION);
        Object message = jmsTemplate.receiveAndConvert(BeerQueue.ALLOCATE_ORDER_FAILED_QUEUE);
        assertThat(message).isNotNull();
        assertThat(message).isInstanceOf(AllocationFailedEvent.class);
        AllocationFailedEvent allocationFailedEvent = (AllocationFailedEvent) message;
        assertThat(allocationFailedEvent.getOrderId()).isEqualTo(newBeerOrder.getId());
    }

    @Test
    void newThroughAllocateToCancelled() throws JsonProcessingException {
        //given
        String beerUpc = "12345";
        UUID beerId = UUID.randomUUID();
        BeerOrder beerOrder = getBeerOrder(beerId, beerUpc, VALIDATION_PASSED, ALLOCATION_PASSED);
        BeerDto beerDto = BeerDto.builder().id(beerId).upc(beerUpc).beerName("Marne piwo").quantityOnHand(15)
                .price(new BigDecimal("13.04")).beerStyle(BeerStyle.ALE).build();
        mockServer.when(HttpRequest.request("/api/v1/beerUpc/" + beerUpc).withMethod("GET"))
                .respond(HttpResponse.response(objectMapper.writeValueAsString(beerDto)).withStatusCode(200));

        //when
        BeerOrder newBeerOrder = beerOrderManager.newBeerOrder(beerOrder);
        assertThat(newBeerOrder).isNotNull();

        assertOrderStatus(newBeerOrder.getId(), OrderStatus.ALLOCATED);
        beerOrderManager.cancelOrder(newBeerOrder.getId());
        assertOrderStatus(newBeerOrder.getId(), OrderStatus.CANCELED);
    }

    @Test
    void newThroughPartialAllocationToCancelled() throws JsonProcessingException {
        //given
        String beerUpc = "123654";
        String beerName = "my-beer";
        UUID beerId = UUID.randomUUID();
        BeerOrder beerOrder = getBeerOrder(beerId, beerUpc, VALIDATION_PASSED, PARTIAL_ALLOCATION);
        BeerDto beerDto = BeerDto.builder().id(beerId).beerName(beerName).quantityOnHand(15).price(new BigDecimal("12.34")).beerStyle(BeerStyle.IPA).build();

        mockServer.when(HttpRequest.request(BEER_UPC_PATH + beerUpc).withMethod("GET"))
                .respond(HttpResponse.response(objectMapper.writeValueAsString(beerDto)).withStatusCode(200));
        //when
        BeerOrder newBeerOrder = beerOrderManager.newBeerOrder(beerOrder);
        assertThat(newBeerOrder).isNotNull();

        assertOrderStatus(newBeerOrder.getId(), OrderStatus.PENDING_INVENTORY);
        beerOrderManager.cancelOrder(beerOrder.getId());

        assertOrderStatus(beerOrder.getId(), OrderStatus.CANCELED);
    }

    @org.springframework.boot.test.context.TestConfiguration
    static class Configuration {
        @Bean
        Queue validationRequestQueue() {
            return new ActiveMQQueue(BeerQueue.BEER_VALIDATION_REQUEST_QUEUE);
        }

        @Bean
        Queue allocationRequestQueue() {
            return new ActiveMQQueue(BeerQueue.ALLOCATE_ORDER_REQUEST_QUEUE);
        }

        @Bean
        Queue allocationFailedQueue() {
            return new ActiveMQQueue(BeerQueue.ALLOCATE_ORDER_FAILED_QUEUE);
        }
    }

    void assertOrderStatus(UUID orderId, OrderStatus status) {
        await().untilAsserted(() -> {
            repository.findById(orderId).ifPresentOrElse(order -> {
                assertThat(order.getOrderStatus()).isEqualTo(status);
            }, () -> fail(String.format("Order status should be %s, but was not found", status)));
        });
    }
}