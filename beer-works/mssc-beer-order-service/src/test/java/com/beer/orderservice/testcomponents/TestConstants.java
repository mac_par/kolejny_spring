package com.beer.orderservice.testcomponents;

public class TestConstants {
    public static final String VALIDATION_PASSED = "order-validation-passed";
    public static final String VALIDATION_FAILED = "order-validation-failed";
    public static final String ALLOCATION_PASSED = "order-allocation-passed";
    public static final String ALLOCATION_FAILED = "order-allocation-failed";
    public static final String PARTIAL_ALLOCATION = "order-partial-allocation";
}
