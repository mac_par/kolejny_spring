package com.beer.orderservice.service;

import com.beer.commons.model.domain.customer.BeerOrder;
import com.beer.commons.model.domain.customer.Customer;
import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.commons.model.dto.BeerOrderPagedList;
import com.beer.commons.model.dto.CustomersPagedList;
import com.beer.commons.model.dto.HttpStatusCode;
import com.beer.commons.model.exception.BreweryException;
import com.beer.commons.model.exception.PersistenceException;
import com.beer.commons.model.mappers.BeerOrderMapper;
import com.beer.commons.model.mappers.CustomerMapper;
import com.beer.orderservice.repository.BeerOrderRepository;
import com.beer.orderservice.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BeerOrderServiceBean implements BeerOrderService {
    private final BeerOrderRepository beerOrderRepository;
    private final CustomerRepository customerRepository;

    private final BeerOrderMapper beerOrderMapper;
    private final BeerOrderManager beerOrderManager;
    private final CustomerMapper customerMapper;

    @Autowired
    public BeerOrderServiceBean(BeerOrderRepository beerOrderRepository, CustomerRepository customerRepository,
                                @Qualifier("beerOrderMapperDecorator") BeerOrderMapper beerOrderMapper,
                                BeerOrderManager beerOrderManager, CustomerMapper customerMapper) {
        this.beerOrderRepository = beerOrderRepository;
        this.customerRepository = customerRepository;
        this.beerOrderMapper = beerOrderMapper;
        this.beerOrderManager = beerOrderManager;
        this.customerMapper = customerMapper;
    }

    @Override
    public BeerOrderPagedList listOrders(UUID customerId, Pageable pageable) {
        log.info("Getting customer {} orders list", customerId);
        return customerRepository.findById(customerId).map(customer -> getOrdersByCustomer(customer, pageable)).orElse(null);
    }

    @Override
    public BeerOrderDto placeOrder(UUID customerId, BeerOrderDto beerOrderDto) {
        log.info("Placing new order {} for customer {}", beerOrderDto, customerId);

        return customerRepository.findById(customerId).map(customer -> addBeerOrder(customer, beerOrderDto))
                .orElseThrow(() -> new BreweryException(HttpStatusCode.NOT_FOUND, String.format("Customer %s not found", customerId)));
    }

    @Override
    public BeerOrderDto getOrderById(UUID customerId, UUID orderId) {
        log.info("Retrieving order {} for customer {}", orderId, customerId);

        return beerOrderMapper.convert(getBeerOrder(customerId, orderId));
    }

    private BeerOrder getBeerOrder(UUID customerId, UUID orderId) {
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new BreweryException(HttpStatusCode.NOT_FOUND, String.format("Customer %s not found", customerId)));
        return customer.getBeerOrders().stream().filter(beerOrder -> beerOrder.getId().equals(orderId))
                .findAny().orElseThrow(() -> new BreweryException(HttpStatusCode.NOT_FOUND, String.format("Beer Order %s not found", orderId)));
    }

    @Override
    public void pickupOrder(UUID customerId, UUID orderId) {
        log.info("Picking up BeerOrder {} for Customer {}", orderId, customerId);
        beerOrderManager.processOrderPickUp(orderId);
    }

    private BeerOrderPagedList getOrdersByCustomer(Customer customer, Pageable pageable) {
        Page<BeerOrder> ordersPerCustomer = beerOrderRepository.findAllByCustomer(customer, pageable);
        return new BeerOrderPagedList(ordersPerCustomer.stream().map(beerOrderMapper::convert).collect(Collectors.toList()),
                PageRequest.of(pageable.getPageNumber(), pageable.getPageSize()), ordersPerCustomer.getSize());
    }

    private BeerOrderDto addBeerOrder(Customer customer, BeerOrderDto beerOrderDto) {
        if (beerOrderDto == null) {
            throw new PersistenceException("Beer Order must be provided");
        }

        BeerOrder beerOrder = beerOrderMapper.convert(beerOrderDto);
        beerOrder.setCustomer(customer);
        beerOrder.getBeerOrderLines().forEach(line -> line.setBeerOrder(beerOrder));
        BeerOrder beerOrderResult = beerOrderManager.newBeerOrder(beerOrder);

        log.info("New Beer Order {} was added", beerOrderResult.getId());

        return beerOrderMapper.convert(beerOrderResult);
    }

    @Override
    public CustomersPagedList listCustomers(Pageable pageable) {
        Page<Customer> results = customerRepository.findAll(pageable);
        return new CustomersPagedList(results.getContent().stream().map(customerMapper::convert).collect(Collectors.toList()),
                results.getPageable(), results.getTotalElements());
    }
}
