package com.beer.orderservice.statemachine.actions;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.domain.OrderStatus;
import com.beer.commons.model.domain.customer.BeerOrder;
import com.beer.commons.model.events.AllocateOrderRequest;
import com.beer.commons.model.events.BeerOrderEvent;
import com.beer.commons.model.mappers.BeerOrderMapper;
import com.beer.orderservice.repository.BeerOrderRepository;
import com.beer.orderservice.service.BeerOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class AllocateOrderAction implements Action<OrderStatus, BeerOrderEvent> {
    private final JmsTemplate jmsTemplate;
    private final BeerOrderRepository repository;
    private final BeerOrderMapper mapper;

    @Autowired
    public AllocateOrderAction(JmsTemplate jmsTemplate, BeerOrderRepository repository, @Qualifier("beerOrderMapperDecorator") BeerOrderMapper mapper) {
        this.jmsTemplate = jmsTemplate;
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public void execute(StateContext<OrderStatus, BeerOrderEvent> context) {
        String orderId = (String) context.getMessageHeader(BeerOrderService.ORDER_ID_HEADER);
        if (orderId != null) {
            BeerOrder beerOrder = repository.getOne(UUID.fromString(orderId));
            AllocateOrderRequest request = AllocateOrderRequest.builder().beerOrderDto(mapper.convert(beerOrder)).build();
            log.info("Sending Allocation request for {}", orderId);
            jmsTemplate.convertAndSend(BeerQueue.ALLOCATE_ORDER_REQUEST_QUEUE, request);
        }
    }
}
