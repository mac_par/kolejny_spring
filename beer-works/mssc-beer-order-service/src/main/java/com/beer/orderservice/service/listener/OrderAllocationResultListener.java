package com.beer.orderservice.service.listener;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.events.AllocateOrderResponse;
import com.beer.commons.model.events.BeerOrderEvent;
import com.beer.orderservice.service.BeerOrderManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class OrderAllocationResultListener {
    private final BeerOrderManager beerOrderManager;

    @Autowired
    public OrderAllocationResultListener(BeerOrderManager beerOrderManager) {
        this.beerOrderManager = beerOrderManager;
    }

    @JmsListener(destination = BeerQueue.ALLOCATE_ORDER_RESULT_QUEUE)
    void procesAllocationResult(AllocateOrderResponse response) {
        UUID orderId = response.getOrderId();
        log.info("Allocation result for beer order: {}, errors {}, pending {}", orderId, response.isAllocationError(), response.isPendingInventory());
        BeerOrderEvent event;
        if (response.isAllocationError()) {
            log.info("There was allocation error");
            event = BeerOrderEvent.ALLOCATION_FAILED;
        } else if (response.isPendingInventory()) {
            log.info("Allocation result: Pending");
            event = BeerOrderEvent.ALLOCATION_NO_INVENTORY;
        } else {
            log.info("Allocation passed");
            event = BeerOrderEvent.ALLOCATION_SUCCESS;
        }
        log.info("Sending {} event for order id: {}", event, orderId);
        beerOrderManager.processAllocationResult(orderId, event);
    }
}
