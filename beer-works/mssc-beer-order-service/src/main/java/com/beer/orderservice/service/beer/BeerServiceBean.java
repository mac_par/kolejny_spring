package com.beer.orderservice.service.beer;

import com.beer.commons.model.dto.BeerDto;
import com.beer.commons.model.dto.HttpStatusCode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.UUID;

@Service
@Slf4j
public class BeerServiceBean implements BeerService {
    private final ObjectMapper objectMapper;
    private final OkHttpClient httpClient;

    @Value("${order-service.beer-service.host}")
    private String host;
    @Value("${order-service.beer-service.endpoint.beer}")
    private String beerEndpoint;
    @Value("${order-service.beer-service.endpoint.beerUpc}")
    private String beerUpcEndpoint;

    @Autowired
    public BeerServiceBean(ObjectMapper objectMapper, OkHttpClient httpClient) {
        this.objectMapper = objectMapper;
        this.httpClient = httpClient;
    }

    @Override
    public Optional<BeerDto> getBeerByUpc(String upc) {
        log.info("Retrieve beer by upc {}", upc);
        if (StringUtils.isEmpty(upc)) {
            return Optional.empty();
        }

        log.info(getPath(beerUpcEndpoint, upc));
        Request request = new Request.Builder().get().url(getPath(beerUpcEndpoint, upc)).build();
        try (Response response = httpClient.newCall(request).execute()) {
            int responseCode = response.code();
            if (responseCode == HttpStatusCode.OK.getCode()) {
                ResponseBody responseBody = response.body();
                return getBodyFromResponse(responseBody);
            } else {
                return Optional.empty();
            }
        } catch (IOException ex) {
            log.error("Exception has occurred", ex);
            return Optional.empty();
        }
    }

    @Override
    public Optional<BeerDto> getBeerById(UUID beerId) {
        log.info("Retrieve beer by upc {}", beerId);
        if (Objects.isNull(beerId)) {
            return Optional.empty();
        }

        Request request = new Request.Builder().get().url(getPath(beerEndpoint, beerId.toString())).build();
        try (Response response = httpClient.newCall(request).execute()) {
            int responseCode = response.code();
            if (responseCode == HttpStatusCode.OK.getCode()) {
                ResponseBody responseBody = response.body();
                return getBodyFromResponse(responseBody);
            } else {
                return Optional.empty();
            }
        } catch (IOException ex) {
            log.error("Exception has occurred", ex);
            return Optional.empty();
        }
    }

    private String getPath(String... components) {
        StringJoiner joiner = new StringJoiner("/").add(host);
        for (String component : components) {
            joiner.add(component);
        }

        return joiner.toString();
    }

    private Optional<BeerDto> getBodyFromResponse(ResponseBody responseBody) throws IOException {
        if (responseBody != null) {
            String body = responseBody.string();
            if (StringUtils.isEmpty(body)) {
                return Optional.empty();
            } else {
                try {
                    return Optional.of(this.objectMapper.readValue(body, BeerDto.class));
                } catch (IOException ex) {
                    return Optional.empty();
                }
            }
        }

        return Optional.empty();
    }
}
