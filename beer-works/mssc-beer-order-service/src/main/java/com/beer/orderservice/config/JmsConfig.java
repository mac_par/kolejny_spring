package com.beer.orderservice.config;

import com.beer.commons.model.BeerQueue;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import java.nio.charset.StandardCharsets;

@Configuration
public class JmsConfig {
    @Value("${order-service.jms.broker.username}")
    private String brokerUsername;
    @Value("${order-service.jms.broker.password}")
    private String brokerPassword;
    @Value("${order-service.jms.broker.url}")
    private String brokerURL;

    @Bean
    ActiveMQConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory(brokerURL, brokerUsername, brokerPassword);
    }

    @Bean
    ActiveMQQueue orderServiceQueue() {
        return new ActiveMQQueue(BeerQueue.BEER_ORDER_SERVICE_QUEUE);
    }

    @Bean
    ActiveMQQueue beerOrderValidationResponseQueue() {
        return new ActiveMQQueue(BeerQueue.BEER_VALIDATION_RESPONSE_QUEUE);
    }

    @Bean
    MappingJackson2MessageConverter converter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        converter.setEncoding(StandardCharsets.UTF_8.displayName());
        return converter;
    }
}
