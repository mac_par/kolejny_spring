package com.beer.orderservice.service;

import com.beer.commons.model.domain.OrderStatus;
import com.beer.commons.model.domain.customer.BeerOrder;
import com.beer.commons.model.domain.customer.BeerOrderLine;
import com.beer.commons.model.events.BeerOrderEvent;
import com.beer.orderservice.repository.BeerOrderRepository;
import com.beer.orderservice.statemachine.interceptor.BeerOrderStateChangeInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class BeerOrderManagerBean implements BeerOrderManager {
    private final StateMachineFactory<OrderStatus, BeerOrderEvent> stateMachineFactory;
    private final BeerOrderRepository repository;
    private final BeerOrderStateChangeInterceptor interceptor;

    @Autowired
    public BeerOrderManagerBean(StateMachineFactory<OrderStatus, BeerOrderEvent> stateMachineFactory, BeerOrderRepository repository,
                                BeerOrderStateChangeInterceptor interceptor) {
        this.stateMachineFactory = stateMachineFactory;
        this.repository = repository;
        this.interceptor = interceptor;
    }

    @Transactional
    @Override
    public BeerOrder newBeerOrder(BeerOrder beerOrder) {
        beerOrder.setId(null);
        beerOrder.setOrderStatus(OrderStatus.NEW);

        BeerOrder savedBeerOrder = repository.saveAndFlush(beerOrder);
        sendBeerOrderEvent(savedBeerOrder, BeerOrderEvent.VALIDATE_ORDER);
        return savedBeerOrder;
    }

    private void sendBeerOrderEvent(BeerOrder beerOrder, BeerOrderEvent beerOrderEvent) {
        StateMachine<OrderStatus, BeerOrderEvent> stateMachine = build(beerOrder);
        Message<BeerOrderEvent> msg = MessageBuilder.withPayload(beerOrderEvent).setHeader(BeerOrderService.ORDER_ID_HEADER, beerOrder.getId().toString()).build();
        stateMachine.sendEvent(msg);
    }

    private StateMachine<OrderStatus, BeerOrderEvent> build(BeerOrder beerOrder) {
        StateMachine<OrderStatus, BeerOrderEvent> stateMachine = stateMachineFactory.getStateMachine(beerOrder.getId());
        stateMachine.stop();
        stateMachine.getStateMachineAccessor().doWithAllRegions(sma -> {
            sma.addStateMachineInterceptor(interceptor);
            sma.resetStateMachine(new DefaultStateMachineContext<>(beerOrder.getOrderStatus(), null, null, null));
        });
        stateMachine.start();

        return stateMachine;
    }

    @Transactional
    @Override
    public void processValidationResult(UUID orderId, BeerOrderEvent event) {
        log.info("Processing sending event {} for order {}", event, orderId);
        sendBeerOrderEvent(repository.getOne(orderId), event);
        log.info("Event {} was published for order {}", event, orderId);
        if (BeerOrderEvent.VALIDATION_PASSED == event) {
            BeerOrderEvent nextEvent = BeerOrderEvent.ALLOCATE_ORDER;
            log.info("Moving to orderValidation");
            sendBeerOrderEvent(repository.getOne(orderId), nextEvent);
        }
    }

    @Transactional
    @Override
    public void processAllocationResult(UUID orderId, BeerOrderEvent event) {
        BeerOrder beerOrder = repository.getOne(orderId);
        log.info("Allocation order {} resulted in {}", orderId, event);
        sendBeerOrderEvent(beerOrder, event);

        if (BeerOrderEvent.ALLOCATION_NO_INVENTORY == event) {
            updateAllocation(beerOrder);
        }
    }

    private void updateAllocation(BeerOrder beerOrder) {
        BeerOrder allocatedBeerOrder = repository.getOne(beerOrder.getId());
        allocatedBeerOrder.getBeerOrderLines().forEach(beerOrderLine -> {
            beerOrder.getBeerOrderLines().forEach(line -> {
                if (doesOrderLineMatch(beerOrderLine, line)) {
                    beerOrderLine.setQuantityAllocated(line.getOrderQuantity());
                }
            });
        });

        repository.saveAndFlush(allocatedBeerOrder);
    }

    @Transactional
    @Override
    public void processOrderPickUp(UUID orderId) {
        log.info("Processing collection order {}", orderId);
        repository.findById(orderId).ifPresentOrElse(order -> {
            log.info("Preparing order for pick up");
            sendBeerOrderEvent(order, BeerOrderEvent.BEER_ORDER_PICKED_UP);
        }, () -> log.error("Order Id {} was not found", orderId));
    }

    private boolean doesOrderLineMatch(BeerOrderLine orderLine, BeerOrderLine beerOrderLine) {
        return orderLine.getId().equals(beerOrderLine.getId());
    }

    @Override
    public void cancelOrder(UUID orderId) {
        log.info("Canceling Order {}", orderId);
        repository.findById(orderId).ifPresent(order -> {
            sendBeerOrderEvent(order, BeerOrderEvent.CANCEL_ORDER);
        });
    }
}
