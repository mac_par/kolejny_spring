package com.beer.orderservice.statemachine.actions;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.domain.OrderStatus;
import com.beer.commons.model.events.AllocationFailedEvent;
import com.beer.commons.model.events.BeerOrderEvent;
import com.beer.orderservice.service.BeerOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class AllocationOrderFailedAction implements Action<OrderStatus, BeerOrderEvent> {
    private final JmsTemplate jmsTemplate;

    @Autowired
    public AllocationOrderFailedAction(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void execute(StateContext<OrderStatus, BeerOrderEvent> context) {
        String orderIdStr = (String) context.getMessage().getHeaders().get(BeerOrderService.ORDER_ID_HEADER);
        if (orderIdStr != null) {
            log.info("Notification of failed order allocation for {}", orderIdStr);
            UUID orderId = UUID.fromString(orderIdStr);
            AllocationFailedEvent message = AllocationFailedEvent.builder().orderId(orderId).build();
            jmsTemplate.convertAndSend(BeerQueue.ALLOCATE_ORDER_FAILED_QUEUE, message);
        }
    }
}
