package com.beer.orderservice.statemachine.interceptor;

import com.beer.commons.model.domain.OrderStatus;
import com.beer.commons.model.domain.customer.BeerOrder;
import com.beer.commons.model.events.BeerOrderEvent;
import com.beer.orderservice.repository.BeerOrderRepository;
import com.beer.orderservice.service.BeerOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.StateMachineInterceptorAdapter;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
@Transactional
public class BeerOrderStateChangeInterceptor extends StateMachineInterceptorAdapter<OrderStatus, BeerOrderEvent> {
    private final BeerOrderRepository repository;

    @Autowired
    public BeerOrderStateChangeInterceptor(BeerOrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public void preStateChange(State<OrderStatus, BeerOrderEvent> state, Message<BeerOrderEvent> message, Transition<OrderStatus, BeerOrderEvent> transition,
                               StateMachine<OrderStatus, BeerOrderEvent> stateMachine) {
        Optional.ofNullable(message)
                .flatMap(msg -> Optional.ofNullable((String) msg.getHeaders().get(BeerOrderService.ORDER_ID_HEADER)))
                .ifPresent(orderId -> {
                    log.info("Getting beer order for id {}", orderId);
                    BeerOrder beerOrder = repository.getOne(UUID.fromString(orderId));
                    OrderStatus orderStatus = state.getId();
                    log.info("Changing Beer Order status into {}", orderStatus);
                    beerOrder.setOrderStatus(orderStatus);
                    repository.saveAndFlush(beerOrder);
                });
    }
}
