package com.beer.orderservice.service;

import com.beer.commons.model.domain.customer.Customer;
import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.commons.model.dto.BeerOrderLineDto;
import com.beer.orderservice.bootstrap.BeerOrderBootStrap;
import com.beer.orderservice.repository.BeerOrderRepository;
import com.beer.orderservice.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
@Slf4j
@Profile({"!test"})
public class TastingRoomService {
    private final CustomerRepository customerRepository;
    private final BeerOrderService beerOrderService;
    private final BeerOrderRepository beerOrderRepository;
    private final List<String> beerUpcs = new ArrayList<>();

    @Value("${orderservice.random.max}")
    private int maxOrderSize;

    @Autowired
    public TastingRoomService(CustomerRepository customerRepository, BeerOrderService beerOrderService, BeerOrderRepository beerOrderRepository) {
        this.customerRepository = customerRepository;
        this.beerOrderService = beerOrderService;
        this.beerOrderRepository = beerOrderRepository;

        beerUpcs.add(BeerOrderBootStrap.BEER_1_UPC);
        beerUpcs.add(BeerOrderBootStrap.BEER_2_UPC);
        beerUpcs.add(BeerOrderBootStrap.BEER_3_UPC);
    }

    @Transactional
    @Scheduled(fixedRate = 2000)
    public void placeTastingRoomOrder() {
        List<Customer> customerList = customerRepository.findAllByNameLike(BeerOrderBootStrap.TASTING_ROOM);
        if (customerList.size() == 1) {
            doPlaceOrder(customerList.get(0));
        } else {
            log.error("Too many customers or too few tasting rooms");
        }
    }

    private void doPlaceOrder(Customer customer) {
        String beerToOrder = getRandomBeerUpc();

        BeerOrderLineDto beerOrderLineDto = BeerOrderLineDto.builder()
                .upc(beerToOrder)
                .beerId(UUID.fromString("5a3b509e-f083-47c7-9a7b-0c044586b157"))
                .orderQuantity(new Random().nextInt(maxOrderSize))
                .build();

        BeerOrderDto beerOrderDto = BeerOrderDto.builder()
                .beerOrderLines(Collections.singletonList(beerOrderLineDto))
                .customerId(customer.getId())
                .customerRef(UUID.randomUUID().toString())
                .build();

        beerOrderService.placeOrder(customer.getId(), beerOrderDto);
    }

    private String getRandomBeerUpc() {
        return beerUpcs.get(new Random().nextInt(beerUpcs.size()));
    }
}
