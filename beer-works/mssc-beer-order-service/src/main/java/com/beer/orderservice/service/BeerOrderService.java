package com.beer.orderservice.service;

import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.commons.model.dto.BeerOrderPagedList;
import com.beer.commons.model.dto.CustomersPagedList;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface BeerOrderService {

    String ORDER_ID_HEADER = "BeerOrderId";

    BeerOrderPagedList listOrders(UUID customerId, Pageable pageable);

    CustomersPagedList listCustomers(Pageable pageable);

    BeerOrderDto placeOrder(UUID customerId, BeerOrderDto beerOrderDto);

    BeerOrderDto getOrderById(UUID customerId, UUID orderId);

    void pickupOrder(UUID customerId, UUID orderId);

}
