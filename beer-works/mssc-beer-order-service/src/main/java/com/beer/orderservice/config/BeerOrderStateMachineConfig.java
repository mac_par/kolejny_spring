package com.beer.orderservice.config;

import com.beer.commons.model.domain.OrderStatus;
import com.beer.commons.model.events.BeerOrderEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

@Configuration
@EnableStateMachineFactory
public class BeerOrderStateMachineConfig extends StateMachineConfigurerAdapter<OrderStatus, BeerOrderEvent> {
    private final Action<OrderStatus, BeerOrderEvent> validateBeerOrderAction;
    private final Action<OrderStatus, BeerOrderEvent> allocateOrderAction;
    private final Action<OrderStatus, BeerOrderEvent> validationFailureAction;
    private final Action<OrderStatus, BeerOrderEvent> allocationOrderFailedAction;
    private final Action<OrderStatus, BeerOrderEvent> deallocateOrderAction;

    @Autowired
    public BeerOrderStateMachineConfig(Action<OrderStatus, BeerOrderEvent> validateBeerOrderAction, Action<OrderStatus,
            BeerOrderEvent> allocateOrderAction, Action<OrderStatus, BeerOrderEvent> validationFailureAction,
                                       Action<OrderStatus, BeerOrderEvent> allocationOrderFailedAction,
                                       Action<OrderStatus, BeerOrderEvent> deallocateOrderAction) {
        this.validateBeerOrderAction = validateBeerOrderAction;
        this.allocateOrderAction = allocateOrderAction;
        this.validationFailureAction = validationFailureAction;
        this.allocationOrderFailedAction = allocationOrderFailedAction;
        this.deallocateOrderAction = deallocateOrderAction;
    }

    @Override
    public void configure(StateMachineConfigurationConfigurer<OrderStatus, BeerOrderEvent> config) throws Exception {
        super.configure(config);
    }

    @Override
    public void configure(StateMachineStateConfigurer<OrderStatus, BeerOrderEvent> states) throws Exception {
        states.withStates().initial(OrderStatus.NEW)
                .states(EnumSet.allOf(OrderStatus.class))
                .end(OrderStatus.DELIVERED)
                .end(OrderStatus.CANCELED)
                .end(OrderStatus.PICKED_UP)
                .end(OrderStatus.DELIVERY_EXCEPTION)
                .end(OrderStatus.ALLOCATION_EXCEPTION)
                .end(OrderStatus.VALIDATION_EXCEPTION);
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<OrderStatus, BeerOrderEvent> transitions) throws Exception {
        transitions.withExternal().source(OrderStatus.NEW).target(OrderStatus.VALIDATION_PENDING).event(BeerOrderEvent.VALIDATE_ORDER)
                .action(validateBeerOrderAction)
                .and()
                .withExternal().source(OrderStatus.VALIDATION_PENDING).target(OrderStatus.VALIDATED).event(BeerOrderEvent.VALIDATION_PASSED)
                .and()
                .withExternal().source(OrderStatus.VALIDATED).target(OrderStatus.CANCELED).event(BeerOrderEvent.CANCEL_ORDER)
                .action(deallocateOrderAction)
                .and()
                .withExternal().source(OrderStatus.VALIDATION_PENDING).target(OrderStatus.VALIDATION_EXCEPTION).event(BeerOrderEvent.VALIDATION_FAILED)
                .action(validationFailureAction)
                .and()
                .withExternal().source(OrderStatus.VALIDATED).target(OrderStatus.ALLOCATION_PENDING).event(BeerOrderEvent.ALLOCATE_ORDER)
                .action(allocateOrderAction)
                .and()
                .withExternal().source(OrderStatus.ALLOCATION_PENDING).target(OrderStatus.CANCELED).event(BeerOrderEvent.CANCEL_ORDER)
                .action(deallocateOrderAction)
                .and()
                .withExternal().source(OrderStatus.ALLOCATION_PENDING).target(OrderStatus.ALLOCATED).event(BeerOrderEvent.ALLOCATION_SUCCESS)
                .and()
                .withExternal().source(OrderStatus.ALLOCATED).target(OrderStatus.CANCELED).event(BeerOrderEvent.CANCEL_ORDER)
                .action(deallocateOrderAction)
                .and()
                .withExternal().source(OrderStatus.ALLOCATION_PENDING).target(OrderStatus.ALLOCATION_EXCEPTION).event(BeerOrderEvent.ALLOCATION_FAILED)
                .action(allocationOrderFailedAction)
                .and()
                .withExternal().source(OrderStatus.ALLOCATION_PENDING).target(OrderStatus.PENDING_INVENTORY).event(BeerOrderEvent.ALLOCATION_NO_INVENTORY)
                .and()
                .withExternal().source(OrderStatus.PENDING_INVENTORY).target(OrderStatus.CANCELED).event(BeerOrderEvent.CANCEL_ORDER)
                .action(deallocateOrderAction)
                .and()
                .withExternal().source(OrderStatus.ALLOCATED).target(OrderStatus.PICKED_UP).event(BeerOrderEvent.BEER_ORDER_PICKED_UP);
    }
}
