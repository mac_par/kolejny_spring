package com.beer.orderservice.service.listener;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.events.BeerOrderEvent;
import com.beer.commons.model.events.BeerOrderValidationResponseEvent;
import com.beer.orderservice.service.BeerOrderManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class BeerOrderValidationResponseListener {
    private final BeerOrderManager beerOrderManager;

    @Autowired
    public BeerOrderValidationResponseListener(BeerOrderManager beerOrderManager) {
        this.beerOrderManager = beerOrderManager;
    }

    @JmsListener(destination = BeerQueue.BEER_VALIDATION_RESPONSE_QUEUE)
    void validationResponse(@Payload BeerOrderValidationResponseEvent response) {
        log.info("Receiving validation response {}", response);
        UUID orderId = response.getOrderId();
        BeerOrderEvent event = BeerOrderEvent.VALIDATION_PASSED;
        if (!response.isValidationResult()) {
            log.info("Validation failed. Reason: {}", response.getErrors());
            event = BeerOrderEvent.VALIDATION_FAILED;
        }
        log.info("Beer Order Validation Event: {}, Order Id {}", event, orderId);
        beerOrderManager.processValidationResult(orderId, event);
    }
}
