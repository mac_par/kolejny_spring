package com.beer.orderservice.statemachine.actions;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.domain.OrderStatus;
import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.commons.model.events.BeerOrderEvent;
import com.beer.commons.model.events.DeallocateOrderEvent;
import com.beer.commons.model.mappers.BeerOrderMapper;
import com.beer.orderservice.repository.BeerOrderRepository;
import com.beer.orderservice.service.BeerOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class DeallocateOrderAction implements Action<OrderStatus, BeerOrderEvent> {
    private final JmsTemplate jmsTemplate;
    private final BeerOrderRepository repository;
    private final BeerOrderMapper mapper;

    @Autowired
    public DeallocateOrderAction(JmsTemplate jmsTemplate, BeerOrderRepository repository, @Qualifier("beerOrderMapper") BeerOrderMapper mapper) {
        this.jmsTemplate = jmsTemplate;
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public void execute(StateContext<OrderStatus, BeerOrderEvent> context) {
        String beerOrderStr = (String) context.getMessage().getHeaders().get(BeerOrderService.ORDER_ID_HEADER);
        if (beerOrderStr != null) {
            UUID orderId = UUID.fromString(beerOrderStr);
            repository.findById(orderId).ifPresent(beerOrder -> {
                BeerOrderDto beerOrderDto = mapper.convert(beerOrder);
                log.info("Sending deallocation message for order: {}", orderId);
                DeallocateOrderEvent event = DeallocateOrderEvent.builder().beerOrderDto(beerOrderDto).build();
                jmsTemplate.convertAndSend(BeerQueue.DEALLOCATE_ORDER_QUEUE, event);
            });
        }
    }
}
