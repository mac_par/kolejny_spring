package com.beer.orderservice.service.beer;

import com.beer.commons.model.dto.BeerDto;

import java.util.Optional;
import java.util.UUID;

public interface BeerService {

    Optional<BeerDto> getBeerByUpc(String upc);

    Optional<BeerDto> getBeerById(UUID beerId);

}
