package com.beer.orderservice.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "order-service.hikaricp")
class ConnectionPoolConfig {

    private int minIdle;
    private int maxPoolSize;
    private long idleTimeout;
    private long maxLifeTime;
    private long connectionTimeout;
    private String poolName;

}
