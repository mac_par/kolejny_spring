package com.beer.orderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class BeerOrderServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(BeerOrderServiceApplication.class, args);
    }
}
