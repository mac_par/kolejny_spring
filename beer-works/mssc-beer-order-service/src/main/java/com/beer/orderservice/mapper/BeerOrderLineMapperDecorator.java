package com.beer.orderservice.mapper;

import com.beer.commons.model.domain.customer.BeerOrderLine;
import com.beer.commons.model.dto.BeerDto;
import com.beer.commons.model.dto.BeerOrderLineDto;
import com.beer.commons.model.mappers.BeerOrderLineMapper;
import com.beer.orderservice.service.beer.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class BeerOrderLineMapperDecorator implements BeerOrderLineMapper {
    private final BeerOrderLineMapper beerOrderLineMapper;
    private final BeerService beerService;

    @Autowired
    public BeerOrderLineMapperDecorator(BeerOrderLineMapper beerOrderLineMapper, BeerService beerService) {
        this.beerOrderLineMapper = beerOrderLineMapper;
        this.beerService = beerService;
    }

    @Override
    public BeerOrderLine convert(BeerOrderLineDto orderLineDto) {
        return beerOrderLineMapper.convert(orderLineDto);
    }

    @Override
    public BeerOrderLineDto convert(BeerOrderLine orderLine) {
        BeerOrderLineDto line = beerOrderLineMapper.convert(orderLine);

        getBeerDtoByUpc(line.getUpc()).ifPresent(beer -> {
            line.setOrderQuantity(beer.getQuantityOnHand());
            line.setBeerId(beer.getId());
            line.setBeerName(beer.getBeerName());
            line.setPrice(beer.getPrice());
            line.setBeerStyle(beer.getBeerStyle());
        });
        return line;
    }

    Optional<BeerDto> getBeerDtoByUpc(String upc) {
        return beerService.getBeerByUpc(upc);
    }
}
