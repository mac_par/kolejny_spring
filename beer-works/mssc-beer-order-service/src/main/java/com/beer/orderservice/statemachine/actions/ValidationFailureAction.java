package com.beer.orderservice.statemachine.actions;

import com.beer.commons.model.domain.OrderStatus;
import com.beer.commons.model.events.BeerOrderEvent;
import com.beer.orderservice.service.BeerOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ValidationFailureAction implements Action<OrderStatus, BeerOrderEvent> {
    @Override
    public void execute(StateContext<OrderStatus, BeerOrderEvent> context) {
        String beerOrderId = (String) context.getMessage().getHeaders().get(BeerOrderService.ORDER_ID_HEADER);
        log.error("Compensating notification: Validation failed for Order id {}", beerOrderId);
    }
}
