package com.beer.orderservice.service;

import com.beer.commons.model.domain.customer.BeerOrder;
import com.beer.commons.model.events.BeerOrderEvent;

import java.util.UUID;

public interface BeerOrderManager {

    BeerOrder newBeerOrder(BeerOrder beerOrder);

    void processValidationResult(UUID orderId, BeerOrderEvent event);

    void processAllocationResult(UUID orderId, BeerOrderEvent event);

    void processOrderPickUp(UUID orderId);

    void cancelOrder(UUID orderId);
}
