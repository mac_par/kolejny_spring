package com.beer.orderservice.controller;

import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.commons.model.dto.BeerOrderPagedList;
import com.beer.commons.model.dto.CustomersPagedList;
import com.beer.orderservice.service.BeerOrderService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/customers")
public class BeerOrderController {

    @Value("${orderservice.page.number}")
    private int defaultPageNumber;

    @Value("${orderservice.page.size}")
    private int defaultPageSize;

    private final BeerOrderService beerOrderService;

    public BeerOrderController(BeerOrderService beerOrderService) {
        this.beerOrderService = beerOrderService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public CustomersPagedList listCustomers(@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
                                            @RequestParam(value = "pageSize", required = false) Integer pageSize) {
        pageNumber = checkPageNumber(pageNumber);
        pageSize = checkPageSize(pageSize);

        return beerOrderService.listCustomers(PageRequest.of(pageNumber, pageSize));
    }

    @GetMapping("{customerId}/orders")
    public BeerOrderPagedList listOrders(@PathVariable("customerId") UUID customerId,
                                         @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
                                         @RequestParam(value = "pageSize", required = false) Integer pageSize) {
        pageNumber = checkPageNumber(pageNumber);
        pageSize = checkPageSize(pageSize);

        return beerOrderService.listOrders(customerId, PageRequest.of(pageNumber, pageSize));
    }

    @PostMapping("{customerId}/orders")
    @ResponseStatus(HttpStatus.CREATED)
    public BeerOrderDto placeOrder(@PathVariable("customerId") UUID customerId, @RequestBody BeerOrderDto beerOrderDto) {
        return beerOrderService.placeOrder(customerId, beerOrderDto);
    }

    @GetMapping("{customerId}/orders/{orderId}")
    public BeerOrderDto getOrder(@PathVariable("customerId") UUID customerId, @PathVariable("orderId") UUID orderId) {
        return beerOrderService.getOrderById(customerId, orderId);
    }

    @PutMapping("{customerId}/orders/{orderId}/pickup")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void pickupOrder(@PathVariable("customerId") UUID customerId, @PathVariable("orderId") UUID orderId) {
        beerOrderService.pickupOrder(customerId, orderId);
    }

    private Integer checkPageNumber(Integer pageNumber) {
        return (pageNumber == null || pageNumber < 0) ? defaultPageNumber : pageNumber;
    }

    private Integer checkPageSize(Integer pageSize) {
        return (pageSize == null || pageSize < 1) ? defaultPageSize : pageSize;
    }
}
