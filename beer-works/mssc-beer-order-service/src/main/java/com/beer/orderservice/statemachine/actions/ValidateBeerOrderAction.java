package com.beer.orderservice.statemachine.actions;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.domain.OrderStatus;
import com.beer.commons.model.domain.customer.BeerOrder;
import com.beer.commons.model.events.BeerOrderEvent;
import com.beer.commons.model.events.BeerOrderValidationRequestEvent;
import com.beer.commons.model.mappers.BeerOrderMapper;
import com.beer.orderservice.repository.BeerOrderRepository;
import com.beer.orderservice.service.BeerOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Slf4j
public class ValidateBeerOrderAction implements Action<OrderStatus, BeerOrderEvent> {
    private final BeerOrderRepository repository;
    private final JmsTemplate jmsTemplate;
    private final BeerOrderMapper beerOrderMapper;

    @Autowired
    public ValidateBeerOrderAction(BeerOrderRepository repository, JmsTemplate jmsTemplate, BeerOrderMapper beerOrderMapper) {
        this.repository = repository;
        this.jmsTemplate = jmsTemplate;
        this.beerOrderMapper = beerOrderMapper;
    }

    @Override
    public void execute(StateContext<OrderStatus, BeerOrderEvent> context) {
        String beerOrderId = (String) context.getMessage().getHeaders().get(BeerOrderService.ORDER_ID_HEADER);
        if (beerOrderId != null) {
            log.info("Retrieving Beer Order for {}", beerOrderId);
            BeerOrder beerOrder = repository.getOne(UUID.fromString(beerOrderId));
            BeerOrderValidationRequestEvent validationEvent = BeerOrderValidationRequestEvent.builder()
                    .beerOrderDto(beerOrderMapper.convert(beerOrder))
                    .build();
            log.info("Sending Beer Order {} for validation", beerOrderId);

            jmsTemplate.convertAndSend(BeerQueue.BEER_VALIDATION_REQUEST_QUEUE, validationEvent);
        }
    }
}
