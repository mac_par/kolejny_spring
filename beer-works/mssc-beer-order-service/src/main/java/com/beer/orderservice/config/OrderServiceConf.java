package com.beer.orderservice.config;

import com.beer.commons.model.mappers.BeerOrderLineMapper;
import com.beer.commons.model.mappers.BeerOrderLineMapperBean;
import com.beer.commons.model.mappers.BeerOrderMapper;
import com.beer.commons.model.mappers.BeerOrderMapperBean;
import com.beer.commons.model.mappers.CustomerMapper;
import com.beer.commons.model.mappers.CustomerMapperBean;
import com.beer.commons.model.mappers.DatesMapper;
import com.beer.commons.model.mappers.DatesMapperBean;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrderServiceConf {

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper().registerModule(new JavaTimeModule()).enable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
                .enable(SerializationFeature.WRITE_DATES_WITH_ZONE_ID);
    }

    @Bean
    public OkHttpClient httpClient() {
        return new OkHttpClient.Builder().build();
    }

    @Bean
    BeerOrderMapper beerOrderMapperDecorator(DatesMapper datesMapper, BeerOrderLineMapper beerOrderLineMapperDecorator) {
        return new BeerOrderMapperBean(datesMapper, beerOrderLineMapperDecorator);
    }

    @Bean
    BeerOrderMapper beerOrderMapper(DatesMapper datesMapper, BeerOrderLineMapper beerOrderLineMapper) {
        return new BeerOrderMapperBean(datesMapper, beerOrderLineMapper);
    }

    @Bean
    DatesMapper datesMapper() {
        return new DatesMapperBean();
    }

    @Bean
    BeerOrderLineMapper beerOrderLineMapper(DatesMapper datesMapper) {
        return new BeerOrderLineMapperBean(datesMapper);
    }

    @Bean
    CustomerMapper customerMapper(DatesMapper datesMapper) {
        return new CustomerMapperBean(datesMapper);
    }
}
