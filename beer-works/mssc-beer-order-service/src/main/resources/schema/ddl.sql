drop table customers CASCADE CONSTRAINTS;
drop table beer_orders CASCADE CONSTRAINTS;
drop table beer_order_lines CASCADE CONSTRAINTS;

create table customers(
id varchar2(36),
name varchar2(60) not null,
api_key varchar2(36) not null,
version number(9) default 1,
created_date timestamp not null,
last_modified_date timestamp not null,
constraint customers_pk primary key(id)
);

create table beer_orders(
id varchar2(36),
customer_ref varchar2(60) not null,
customer_id varchar2(36) not null,
order_status varchar2(25) not null,
order_status_callback_url varchar2(50),
version number(9) default 1,
created_date timestamp not null,
last_modified_date timestamp not null,
constraint beer_orders_pk primary key(id)
);

alter table beer_orders add constraint beer_order_customer_fk foreign key (customer_id) references customers(id);

create table beer_order_lines(
id varchar2(36),
beer_order_id varchar2(36) not null,
beer_id varchar2(36) not null,
ordered_quantity number(9) default 0,
allocated_quantity number(9) default 0,
upc varchar2(25) not null,
version number(9) default 1,
created_date timestamp not null,
last_modified_date timestamp not null,
constraint beer_order_lines_pk primary key(id)
);

alter table beer_order_lines add constraint beer_order_fk foreign key (beer_order_id) references beer_orders(id);
alter table beer_order_lines add constraint beer_order_line_fk foreign key (beer_id) references beers(id);
