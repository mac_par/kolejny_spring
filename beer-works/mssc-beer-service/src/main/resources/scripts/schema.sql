create table beers(
id varchar2(36),
beer_name varchar2(255) not null,
beer_style varchar2(15) not null,
upc varchar2(25) unique,
quantity_on_hand number(9) not null,
price number(9,2) not null,
version number(9) default 1,
created_date timestamp not null,
last_modified_date timestamp not null,
constraint beers_pk primary key(id)
);