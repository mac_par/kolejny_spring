package com.beerservice.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@ConfigurationProperties(prefix = "beer-service.jms.broker")
@Component
public class JmsProperties {
    private String username;
    private String password;
    private String url;
}
