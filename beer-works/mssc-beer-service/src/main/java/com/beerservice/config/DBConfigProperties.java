package com.beerservice.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Properties;

@Setter
@NoArgsConstructor
@Component
@ConfigurationProperties(prefix = "beer-service.database")
public class DBConfigProperties {
    @Getter
    private String url;
    @Getter
    private String driver;
    @Getter
    private String username;
    @Getter
    private String password;
    private int poolSize;
    private String dialect;
    private boolean showSql;
    private int batchSize;
    private int fetchSize;
    private String hbm2dll;

    public void setProperties(Properties properties) {
        Objects.requireNonNull(properties);
        properties.put("hibernate.connection.pool_size", poolSize);
        properties.put("hibernate.dialect", dialect);
        properties.put("hibernate.jdbc.fetch_size", fetchSize);
        properties.put("hibernate.jdbc.batch_size", batchSize);
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", hbm2dll);
    }
}
