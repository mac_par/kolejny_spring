package com.beerservice.config;

import com.beer.commons.model.BeerQueue;
import lombok.Getter;
import lombok.Setter;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import javax.jms.Queue;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "beer-service.jms.broker")
public class JmsConfig {
    @Autowired
    private JmsProperties properties;

    @Bean
    Queue beerServiceQueue() {
        return new ActiveMQQueue(BeerQueue.BREW_BEER_QUEUE);
    }

    @Bean
    Queue beerOrderValidationQueue() {
        return new ActiveMQQueue(BeerQueue.BEER_VALIDATION_REQUEST_QUEUE);
    }

    @Bean
    ActiveMQConnectionFactory connectionFactory(JmsProperties properties) {
        return new ActiveMQConnectionFactory(properties.getUrl(), properties.getUsername(), properties.getPassword());
    }

    @Bean
    MappingJackson2MessageConverter messageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }
}
