package com.beerservice;

public class PathConstants {
    public static final String BEER_INVENTORY_PATH = "/api/v1/beer/{beerId}/inventory";
}
