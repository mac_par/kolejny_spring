package com.beerservice.mapper;

import com.beer.commons.model.domain.beer.Beer;
import com.beer.commons.model.dto.BeerDto;
import com.beer.commons.model.mappers.BeerMapper;
import com.beerservice.service.inventory.BeerInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
public class BeerMapperDecorator implements BeerMapper {
    private final BeerMapper beerMapper;
    private final BeerInventoryService service;

    @Autowired
    public BeerMapperDecorator(BeerMapper beerMapper, BeerInventoryService service) {
        this.beerMapper = beerMapper;
        this.service = service;
    }

    @Override
    public BeerDto convert(Beer beer) {
        BeerDto beerDto = beerMapper.convert(beer);
        beerDto.setQuantityOnHand(service.getOnHandInventory(beer.getId()));
        return beerDto;
    }

    @Override
    public Beer convert(BeerDto beer) {
        return beerMapper.convert(beer);
    }

    @Override
    public Beer update(Beer beer, BeerDto dto) {
        return beerMapper.update(beer, dto);
    }
}
