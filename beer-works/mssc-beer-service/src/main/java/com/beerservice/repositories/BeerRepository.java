package com.beerservice.repositories;

import com.beer.commons.model.domain.beer.Beer;
import com.beer.commons.model.domain.beer.BeerStyle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface BeerRepository extends JpaRepository<Beer, UUID> {
    @Query("select b from Beer b where b.beerName = :name and b.beerStyle = :style")
    Page<Beer> findByBeerNameAndStyle(@Param("name") String beerName, @Param("style") BeerStyle beerStyle, Pageable pageable);

    @Query("select b from Beer b where b.beerName = :name")
    Page<Beer> findByBeerName(@Param("name") String beerName, Pageable pageable);

    @Query("select b from Beer b where b.beerStyle = :style")
    Page<Beer> findByBeerStyle(@Param("style") BeerStyle beerStyle, Pageable pageable);

    @Query("select b from Beer b where b.upc = :upc")
    Optional<Beer> findByUpc(@Param("upc") String beerUpc);
}
