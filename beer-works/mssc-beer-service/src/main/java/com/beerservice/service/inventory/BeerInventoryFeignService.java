package com.beerservice.service.inventory;

import com.beer.commons.model.dto.BeerInventoryDto;
import com.beerservice.PathConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.UUID;

@FeignClient(name = "inventory-service")
public interface BeerInventoryFeignService {

    @RequestMapping(value = PathConstants.BEER_INVENTORY_PATH, method = RequestMethod.GET)
    ResponseEntity<List<BeerInventoryDto>> getOnHandInventory(@PathVariable("beerId") UUID beerId);
}
