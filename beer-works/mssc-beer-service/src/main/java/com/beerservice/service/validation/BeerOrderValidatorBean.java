package com.beerservice.service.validation;

import com.beer.commons.model.dto.BeerOrderLineDto;
import com.beer.commons.model.events.BeerOrderValidationRequestEvent;
import com.beer.commons.model.events.BeerOrderValidationResponseEvent;
import com.beerservice.repositories.BeerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class BeerOrderValidatorBean implements BeerOrderValidator {
    private final BeerRepository repository;

    @Autowired
    public BeerOrderValidatorBean(BeerRepository repository) {
        this.repository = repository;
    }

    @Override
    public BeerOrderValidationResponseEvent validate(BeerOrderValidationRequestEvent request) {
        List<String> validationErrors = request.getBeerOrderDto().getBeerOrderLines().stream().map(BeerOrderLineDto::getUpc)
                .map(upcString -> repository.findByUpc(upcString).isPresent() ? "" : String.format("UPC %s does not exist", upcString))
                .filter(str -> !str.isBlank()).collect(Collectors.toList());

        log.info("Validation results: {}", validationErrors);
        BeerOrderValidationResponseEvent response = BeerOrderValidationResponseEvent.builder()
                .orderId(request.getBeerOrderDto().getId())
                .validationResult(!validationErrors.isEmpty())
                .errors(validationErrors)
                .build();

        return response;
    }
}
