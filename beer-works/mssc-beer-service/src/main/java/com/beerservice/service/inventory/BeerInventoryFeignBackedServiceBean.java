package com.beerservice.service.inventory;

import com.beer.commons.model.dto.BeerInventoryDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Slf4j
@Profile("local-discovery")
@Service
public class BeerInventoryFeignBackedServiceBean implements BeerInventoryService {
    private final BeerInventoryFeignService inventoryFeignService;

    @Autowired
    public BeerInventoryFeignBackedServiceBean(BeerInventoryFeignService inventoryFeignService) {
        this.inventoryFeignService = inventoryFeignService;
    }

    @Override
    public int getOnHandInventory(UUID beerId) {
        log.debug("Calling Inventory Service - Beer Id: {}", beerId);

        ResponseEntity<List<BeerInventoryDto>> results = inventoryFeignService.getOnHandInventory(beerId);
        int sum = results.getBody().stream().mapToInt(BeerInventoryDto::getQuantityOnHand).sum();
        log.debug("{} on hand for beer Id: {}", sum, beerId);
        return sum;
    }
}
