package com.beerservice.service;

import com.beer.commons.model.BeerPagedList;
import com.beer.commons.model.domain.beer.Beer;
import com.beer.commons.model.domain.beer.BeerStyle;
import com.beer.commons.model.dto.BeerDto;
import com.beer.commons.model.dto.HttpStatusCode;
import com.beer.commons.model.exception.BreweryException;
import com.beer.commons.model.exception.PersistenceException;
import com.beer.commons.model.mappers.BeerMapper;
import com.beerservice.repositories.BeerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class BeerServiceBean implements BeerService {
    private final BeerMapper beerMapperPrimary;
    private final BeerMapper beerMapperSecondary;
    private BeerRepository repository;

    @Autowired
    public BeerServiceBean(BeerRepository repository,
                           @Qualifier("beerMapper") BeerMapper beerMapperPrimary, @Qualifier("beerMapperDecorator") BeerMapper beerMapperSecondary) {
        this.repository = repository;
        this.beerMapperSecondary = beerMapperSecondary;
        this.beerMapperPrimary = beerMapperPrimary;
    }

    @Override
    public BeerDto getById(UUID id, boolean showOnHand) {
        log.info("Retrieving Beer ");
        Function<Beer, BeerDto> convertion = beerMapperPrimary::convert;
        if (showOnHand) {
            convertion = beerMapperSecondary::convert;
        }
        return repository.findById(id).map(convertion)
                .orElseThrow(() -> new PersistenceException(String.format("Beer %s can not be found", id)));
    }

    @Override
    public BeerDto save(BeerDto beerDto) {
        log.info("Saving beer instance: {}", beerDto);
        Objects.requireNonNull(beerDto);
        Beer entity = beerMapperPrimary.convert(beerDto);
        return beerMapperPrimary.convert(repository.save(entity));
    }

    @Override
    public BeerDto update(UUID id, BeerDto beerDto) {
        log.info("Beer {} will be updated", id.toString());
        return repository.findById(id).map(beer -> beerMapperPrimary.update(beer, beerDto)).map(repository::save).map(beerMapperPrimary::convert)
                .orElseThrow(() -> new PersistenceException(String.format("Beer %s does not exist", id)));
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Removing beer item {}", id);
        repository.deleteById(id);
    }

    @Override
    public BeerPagedList listBeers(String beerName, BeerStyle beerStyle, PageRequest pageRequest, boolean showOnHand) {
        log.info("Retrieving beer list by: Beer name [{}], Beer style [{}] page {}, max results: {}", beerName, beerStyle,
                pageRequest.getOffset(), pageRequest.getPageSize());

        Page<Beer> beerPage;
        boolean hasName = !StringUtils.isEmpty(beerName);
        boolean hasStyle = beerStyle != null;
        if (hasName && hasStyle) {
            beerPage = repository.findByBeerNameAndStyle(beerName, beerStyle, pageRequest);
        } else if (hasName) {
            beerPage = repository.findByBeerName(beerName, pageRequest);
        } else if (hasStyle) {
            beerPage = repository.findByBeerStyle(beerStyle, pageRequest);
        } else {
            beerPage = repository.findAll(pageRequest);
        }

        Function<Beer, BeerDto> convertionFunction = beerMapperPrimary::convert;
        if (showOnHand) {
            convertionFunction = beerMapperSecondary::convert;
        }

        return conventToPagedList(beerPage, convertionFunction);
    }

    @Override
    public BeerDto getByUpc(String beerUpc) {
        log.info("Retrieving Beer by upc {}", beerUpc);
        if (StringUtils.isEmpty(beerUpc)) {
            throw new BreweryException(HttpStatusCode.BAD_REQUEST, "Upc can not be empty");
        }

        return beerMapperPrimary.convert(repository.findByUpc(beerUpc).orElseThrow(() -> new BreweryException(HttpStatusCode.BAD_REQUEST,
                String.format("Upc %s does not exists", beerUpc))));
    }

    private BeerPagedList conventToPagedList(Page<Beer> beerPage, Function<Beer, BeerDto> convertFn) {
        return new BeerPagedList(beerPage.stream().map(convertFn).collect(Collectors.toList()),
                PageRequest.of(beerPage.getNumber(), beerPage.getSize()),
                beerPage.getTotalElements());
    }
}
