package com.beerservice.service;

import com.beer.commons.model.BeerPagedList;
import com.beer.commons.model.domain.beer.BeerStyle;
import com.beer.commons.model.dto.BeerDto;
import org.springframework.data.domain.PageRequest;

import java.util.UUID;

public interface BeerService {

    BeerDto getById(UUID id, boolean showOnHand);

    BeerDto save(BeerDto beerDto);

    BeerDto update(UUID id, BeerDto beerDto);

    void deleteById(UUID id);

    BeerPagedList listBeers(String beerName, BeerStyle beerStyle, PageRequest pageRequest, boolean showOnHand);

    BeerDto getByUpc(String beerUpc);

}
