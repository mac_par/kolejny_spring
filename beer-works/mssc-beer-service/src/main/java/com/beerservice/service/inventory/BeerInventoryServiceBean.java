package com.beerservice.service.inventory;

import com.beer.commons.model.dto.BeerInventoryDto;
import com.beer.commons.model.dto.HttpStatusCode;
import com.beer.commons.model.exception.BreweryException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Profile("!local-discovery")
@Service
@Slf4j
public class BeerInventoryServiceBean implements BeerInventoryService {
    private static final TypeReference<List<BeerInventoryDto>> BEER_INVENTORY_TYPE_REFERENCE = new TypeReference<>() {
    };

    private final OkHttpClient httpClient;
    private final ObjectMapper objectMapper;

    @Value("${beer-service.service.beer-inventory.hostname}")
    private String hostname;
    @Value("${beer-service.service.beer-inventory.url}")
    private String inventoryServicePath;

    @Autowired
    public BeerInventoryServiceBean(OkHttpClient httpClient, ObjectMapper objectMapper) {
        this.httpClient = httpClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public int getOnHandInventory(UUID beerId) {
        Objects.requireNonNull(beerId);
        log.info("Retrieve on hand quantity for Beer {}", beerId);
        Request request = new Request.Builder().url(getPath(beerId)).get().build();
        try (Response response = httpClient.newCall(request).execute()) {
            if (response.code() == HttpStatusCode.OK.getCode()) {
                ResponseBody body = response.body();
                if (body != null) {
                    String responseBody = body.string();
                    if (StringUtils.isEmpty(responseBody)) {
                        throw new BreweryException(HttpStatusCode.INTERNAL_SERVER_ERROR, "No response body was received");
                    }
                    log.info("Response body: {}", responseBody);
                    List<BeerInventoryDto> list = objectMapper.readValue(responseBody, BEER_INVENTORY_TYPE_REFERENCE);
                    log.info(list.toString());
                    return list.stream().mapToInt(BeerInventoryDto::getQuantityOnHand).sum();
                } else {
                    throw new BreweryException(HttpStatusCode.INTERNAL_SERVER_ERROR, "No response body was received");
                }
            } else {
                int responseCode = response.code();
                throw new BreweryException(HttpStatusCode.getByCode(responseCode),
                        String.format("Response %d but was expected %d", responseCode, HttpStatusCode.OK.getCode()));
            }
        } catch (IOException e) {
            log.error("Exception on Get request", e);
            throw new BreweryException(HttpStatusCode.BAD_REQUEST, e.getMessage());
        }
    }

    private String getPath(UUID beerId) {
        return String.format("%s/%s", hostname, inventoryServicePath).replace("{}", beerId.toString());
    }
}
