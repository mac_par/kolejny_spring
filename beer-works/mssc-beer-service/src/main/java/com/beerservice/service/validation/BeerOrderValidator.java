package com.beerservice.service.validation;

import com.beer.commons.model.events.BeerOrderValidationRequestEvent;
import com.beer.commons.model.events.BeerOrderValidationResponseEvent;

public interface BeerOrderValidator {

    BeerOrderValidationResponseEvent validate(BeerOrderValidationRequestEvent request);

}
