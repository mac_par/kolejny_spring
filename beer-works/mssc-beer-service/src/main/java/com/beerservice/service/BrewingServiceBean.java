package com.beerservice.service;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.events.BrewBeerEvent;
import com.beer.commons.model.mappers.BeerMapper;
import com.beerservice.repositories.BeerRepository;
import com.beerservice.service.inventory.BeerInventoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Slf4j
@Service
public class BrewingServiceBean implements BrewingService {
    private final BeerRepository repository;
    private final BeerInventoryService inventoryService;
    private final JmsTemplate jmsTemplate;
    private final BeerMapper beerMapper;

    @Autowired
    public BrewingServiceBean(BeerRepository repository, BeerInventoryService inventoryService, JmsTemplate jmsTemplate, BeerMapper beerMapper) {
        this.repository = repository;
        this.inventoryService = inventoryService;
        this.jmsTemplate = jmsTemplate;
        this.beerMapper = beerMapper;
    }

    @Scheduled(fixedRate = 5000)
    @Override
    public void checkInventory() {
        repository.findAll().forEach(beer -> {
            int invOnHand = inventoryService.getOnHandInventory(beer.getId());
            log.info("Current amount on hand in inventory {}", invOnHand);
            if (beer.getQuantityOnHand() >= invOnHand) {
                BrewBeerEvent beerEvent = BrewBeerEvent.builder().beerId(beer.getId()).build();
                beerEvent.setAmountToBrew(ThreadLocalRandom.current().nextInt(1, 10));
                log.info("Request brewing {} additional items", beerEvent.getAmountToBrew());
                jmsTemplate.convertAndSend(BeerQueue.BREW_BEER_QUEUE, beerEvent);
            }
        });
    }
}
