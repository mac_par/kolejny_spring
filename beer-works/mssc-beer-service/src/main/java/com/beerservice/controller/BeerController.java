package com.beerservice.controller;

import com.beer.commons.model.BeerPagedList;
import com.beer.commons.model.domain.beer.BeerStyle;
import com.beer.commons.model.dto.BeerDto;
import com.beer.commons.model.dto.HttpStatusCode;
import com.beer.commons.model.exception.BreweryException;
import com.beer.commons.model.exception.BreweryExceptionHandler;
import com.beerservice.service.BeerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;
import java.util.UUID;
import javax.validation.Valid;

@Slf4j
@RequestMapping("/api/v1")
@RestController
@Import({BreweryExceptionHandler.class})
public class BeerController {
    private final BeerService beerService;
    @Value("${beer-service.page.number}")
    private int defaultPageNumber;
    @Value("${beer-service.page.size}")
    private int defaultPageSize;

    @Autowired
    public BeerController(BeerService beerService) {
        this.beerService = beerService;
    }

    @RequestMapping(path = "/beer", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<BeerPagedList> listBeer(@RequestParam(value = "pageNumber", required = false, defaultValue = "-1") int pageNumber,
                                                  @RequestParam(value = "pageSize", required = false, defaultValue = "-1") int pageSize,
                                                  @RequestParam(value = "showOnHand", required = false, defaultValue = "false") boolean showOnHand,
                                                  @RequestParam(required = false) String beerName,
                                                  @RequestParam(required = false) BeerStyle beerStyle) {
        pageNumber = getPageNumber(pageNumber);
        pageSize = getPageSize(pageSize);

        return ResponseEntity.ok(beerService.listBeers(beerName, beerStyle, PageRequest.of(pageNumber, pageSize), showOnHand));
    }

    @RequestMapping(path = "/beer/{beerId}", method = RequestMethod.GET)
    public ResponseEntity<BeerDto> getBeerById(@PathVariable("beerId") UUID id,
                                               @RequestParam(value = "showOnHand", required = false, defaultValue = "false") boolean showOnHand) {
        log.info("Retrieving Beer by {}", id);

        try {
            return ResponseEntity.ok(beerService.getById(id, showOnHand));
        } catch (RuntimeException ex) {
            throw new BreweryException(HttpStatusCode.NOT_FOUND, ex.getMessage());
        }
    }

    @RequestMapping(path = "/beerUpc/{upc}", method = RequestMethod.GET)
    public ResponseEntity<BeerDto> getBeerByUpc(@PathVariable("upc") String beerUpc,
                                                @RequestParam(value = "pageNumber", required = false, defaultValue = "-1") int pageNumber,
                                                @RequestParam(value = "pageSize", required = false, defaultValue = "-1") int pageSize) {
        log.info("Retrieving Beer by UPC: {}", beerUpc);
        return ResponseEntity.ok(beerService.getByUpc(beerUpc));
    }

    @RequestMapping(path = "/beer", method = RequestMethod.POST)
    public ResponseEntity<BeerDto> postBeer(@Valid @RequestBody BeerDto beerDto) {
        log.info("Beer was received: {}", beerDto);

        try {
            beerDto = beerService.save(beerDto);
        } catch (Exception ex) {
            throw new BreweryException(HttpStatusCode.BAD_REQUEST, ex.getMessage());
        }
        log.info("Beer {} was saved", beerDto.getId());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, "http://127.0.0.1:8080/api/v1/beer/" + beerDto.getId().toString());
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        return new ResponseEntity<>(beerDto, headers, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/beer/{beerId}", method = RequestMethod.PUT)
    public ResponseEntity<BeerDto> updateBeer(@PathVariable("beerId") UUID id, @Valid @RequestBody BeerDto beerDto) {
        log.info("Updating beer with id {}", id);
        Objects.requireNonNull(beerDto);
        try {
            beerDto = beerService.update(id, beerDto);
        } catch (Exception ex) {
            throw new BreweryException(HttpStatusCode.BAD_REQUEST, ex.getMessage());
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, "http://127.0.0.1:8080/api/v1/beer/" + beerDto.getId().toString());
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return new ResponseEntity<>(beerDto, headers, HttpStatus.OK);
    }

    @RequestMapping(path = "/beer/{beerId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteBeer(@PathVariable("beerId") UUID id) {
        log.info("Deleting beer {}", id);
        try {
            beerService.deleteById(id);
        } catch (Exception ex) {
            throw new BreweryException(HttpStatusCode.BAD_REQUEST, ex.getMessage());
        }
        return ResponseEntity.noContent().build();
    }

    private int getPageNumber(int pageNumber) {
        if (pageNumber < 0) {
            pageNumber = defaultPageNumber;
        }

        return pageNumber;
    }

    private int getPageSize(int pageSize) {
        if (pageSize < 1) {
            pageSize = defaultPageSize;
        }

        return pageSize;
    }
}
