package com.beerservice.listener;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.domain.beer.Beer;
import com.beer.commons.model.events.BrewBeerEvent;
import com.beer.commons.model.events.NewInventoryEvent;
import com.beer.commons.model.mappers.BeerMapper;
import com.beer.commons.model.mappers.BeerMapperBean;
import com.beerservice.repositories.BeerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@Import({BeerMapperBean.class})
public class BrewBeerListener {
    private final BeerRepository beerRepository;
    private final JmsTemplate jmsTemplate;
    private final BeerMapper beerMapper;

    @Autowired
    public BrewBeerListener(BeerRepository beerRepository, JmsTemplate jmsTemplate, BeerMapper beerMapper) {
        this.beerRepository = beerRepository;
        this.jmsTemplate = jmsTemplate;
        this.beerMapper = beerMapper;
    }

    @JmsListener(destination = BeerQueue.BREW_BEER_QUEUE)
    void brewBeer(@Payload BrewBeerEvent brewBeerEvent) {
        Optional<Beer> beer = beerRepository.findById(brewBeerEvent.getBeerId());
        beer.ifPresent(item -> {
            NewInventoryEvent newInventoryEvent = NewInventoryEvent.builder().beerDto(beerMapper.convert(item)).amountToProduce(brewBeerEvent.getAmountToBrew()).build();
            log.debug("Sending new inventory request for beer {} with amount of {}", brewBeerEvent.getBeerId(), brewBeerEvent.getAmountToBrew());
            jmsTemplate.convertAndSend(BeerQueue.BEER_INVENTORY_SERVICE_QUEUE, newInventoryEvent);
        });
    }
}
