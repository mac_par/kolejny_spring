package com.beerservice.listener;

import com.beer.commons.model.BeerQueue;
import com.beer.commons.model.events.BeerOrderValidationRequestEvent;
import com.beer.commons.model.events.BeerOrderValidationResponseEvent;
import com.beerservice.service.validation.BeerOrderValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ValidateBeerOrderListener {
    private final BeerOrderValidator beerOrderValidator;
    private final JmsTemplate jmsTemplate;

    public ValidateBeerOrderListener(BeerOrderValidator beerOrderValidator, JmsTemplate jmsTemplate) {
        this.beerOrderValidator = beerOrderValidator;
        this.jmsTemplate = jmsTemplate;
    }

    @JmsListener(destination = BeerQueue.BEER_VALIDATION_REQUEST_QUEUE)
    void validateOrder(@Payload BeerOrderValidationRequestEvent request) {
        log.info("Validation request received: {}", request);
        BeerOrderValidationResponseEvent response = beerOrderValidator.validate(request);

        log.info("Sending validation response to {} queue", BeerQueue.BEER_VALIDATION_RESPONSE_QUEUE);
        jmsTemplate.convertAndSend(BeerQueue.BEER_VALIDATION_RESPONSE_QUEUE, response);
    }
}
