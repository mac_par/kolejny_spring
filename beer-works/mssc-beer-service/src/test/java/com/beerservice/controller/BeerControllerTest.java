package com.beerservice.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.beer.commons.model.BreweryErrorResponse;
import com.beer.commons.model.domain.beer.BeerStyle;
import com.beer.commons.model.dto.BeerDto;
import com.beer.commons.model.dto.HttpStatusCode;
import com.beer.commons.model.exception.PersistenceException;
import com.beerservice.BeerUpcValues;
import com.beerservice.service.BeerService;
import com.beerservice.service.inventory.BeerInventoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.UUID;

@WebMvcTest(BeerController.class)
class BeerControllerTest {
    private static final String URL = "/api/v1/beer";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private BeerService beerServiceMock;

    @MockBean
    private BeerInventoryService beerInventoryServiceMock;

    @Test
    public void whenBeerExistsThenITsReturned() throws Exception {
        //given
        boolean showOnHand = false;
        UUID id = UUID.randomUUID();
        String name = "Piwko";
        BeerStyle style = BeerStyle.ALE;
        BigDecimal price = new BigDecimal("6.66");
        String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BeerDto expectation = BeerDto.builder().id(id).beerName(name).beerStyle(style).upc(upc).price(price).build();
        String expectedBeer = objectMapper.writeValueAsString(expectation);
        when(beerServiceMock.getById(eq(id), eq(showOnHand))).thenReturn(expectation);

        //when
        //then
        mockMvc.perform(get(URL + "/" + id.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedBeer));
        verify(beerServiceMock, atMostOnce()).getById(id, showOnHand);
        verify(beerInventoryServiceMock, never()).getOnHandInventory(id);
    }

    @Test
    public void whenBeerExistsThenITsReturned2() throws Exception {
        //given
        boolean showOnHand = true;
        UUID id = UUID.randomUUID();
        String name = "Piwko";
        BeerStyle style = BeerStyle.ALE;
        BigDecimal price = new BigDecimal("6.66");
        String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BeerDto expectation = BeerDto.builder().id(id).beerName(name).beerStyle(style).upc(upc).price(price).build();
        String expectedBeer = objectMapper.writeValueAsString(expectation);
        when(beerServiceMock.getById(eq(id), eq(showOnHand))).thenReturn(expectation);

        //when
        //then
        mockMvc.perform(get(URL + "/" + id.toString() + "?showOnHand=true"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedBeer));
        verify(beerServiceMock, atMostOnce()).getById(id, showOnHand);
        verify(beerInventoryServiceMock, atMostOnce()).getOnHandInventory(id);
    }

    @Test
    public void whenBeerDoesNotExistThenExceptionIsThrown() throws Exception {
        //given
        boolean showOnHand = false;
        final UUID id = UUID.randomUUID();
        PersistenceException exception = new PersistenceException("Sth bad has happened");
        String expectedResponse = objectMapper.writeValueAsString(new BreweryErrorResponse(HttpStatusCode.NOT_FOUND.getCode(), exception.getMessage()));
        when(beerServiceMock.getById(eq(id), eq(showOnHand))).thenThrow(exception);

        //when
        //then
        mockMvc.perform(get(URL + "/" + id.toString()))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedResponse));
        verify(beerServiceMock, atMostOnce()).getById(id, showOnHand);
        verify(beerInventoryServiceMock, never()).getOnHandInventory(id);
    }

    @Test
    public void whenBeerDoesNotExistThenExceptionIsThrown2() throws Exception {
        //given
        boolean showOnHand = true;
        final UUID id = UUID.randomUUID();
        PersistenceException exception = new PersistenceException("Sth bad has happened");
        String expectedResponse = objectMapper.writeValueAsString(new BreweryErrorResponse(HttpStatusCode.NOT_FOUND.getCode(), exception.getMessage()));
        when(beerServiceMock.getById(eq(id), eq(showOnHand))).thenThrow(exception);

        //when
        //then
        mockMvc.perform(get(URL + "/" + id.toString() + "?showOnHand=true"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedResponse));
        verify(beerServiceMock, atMostOnce()).getById(id, showOnHand);
        verify(beerInventoryServiceMock, never()).getOnHandInventory(id);
    }

    @Test
    public void postWillCreateABeer() throws Exception {
        //given
        UUID id = UUID.randomUUID();
        String name = "Piwko";
        BeerStyle style = BeerStyle.ALE;
        BigDecimal price = new BigDecimal("6.66");
        String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BeerDto resultBeer = BeerDto.builder().id(id).beerName(name).beerStyle(style).upc(upc).price(price).build();
        String expectedBeer = objectMapper.writeValueAsString(resultBeer);
        BeerDto beer = BeerDto.builder().beerName(name).beerStyle(style).upc(upc).price(price).build();

        String beerJson = objectMapper.writeValueAsString(beer);
        when(beerServiceMock.save(any())).thenAnswer(object -> {
            BeerDto argument = object.getArgument(0);
            argument.setId(id);
            return argument;
        });

        mockMvc.perform(post(URL).content(beerJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, "http://127.0.0.1:8080/api/v1/beer/" + id.toString()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedBeer));

        verify(beerServiceMock, atMostOnce()).update(id, resultBeer);
    }

    @Test
    public void whenPostHasFailedThenExceptionIsThrown() throws Exception {
        //given
        UUID id = UUID.randomUUID();
        String name = "Piwko";
        BeerStyle style = BeerStyle.ALE;
        BigDecimal price = new BigDecimal("6.66");
        String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BeerDto resultBeer = BeerDto.builder().id(id).beerName(name).beerStyle(style).upc(upc).price(price).build();
        PersistenceException exception = new PersistenceException("Sth bad has happened");
        String expectedBeer = objectMapper.writeValueAsString(new BreweryErrorResponse(HttpStatusCode.BAD_REQUEST.getCode(), exception.getMessage()));
        BeerDto beer = BeerDto.builder().beerName(name).beerStyle(style).upc(upc).price(price).build();

        String beerJson = objectMapper.writeValueAsString(beer);
        when(beerServiceMock.save(any())).thenThrow(exception);

        mockMvc.perform(post(URL).content(beerJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedBeer));

        verify(beerServiceMock, atMostOnce()).update(id, resultBeer);
    }

    @Test
    public void putUpdatesBeer() throws Exception {
        //given
        UUID id = UUID.randomUUID();
        String name = "Piwko";
        BigDecimal price = new BigDecimal("13.25");
        String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BeerStyle style = BeerStyle.GOSE;
        BeerDto resultBeer = BeerDto.builder().id(id).beerName(name).beerStyle(style).price(price).upc(upc).build();
        String expectedBeer = objectMapper.writeValueAsString(resultBeer);
        BeerDto beer = BeerDto.builder().beerName(name).beerStyle(style).price(price).upc(upc).build();

        String beerJson = objectMapper.writeValueAsString(beer);
        when(beerServiceMock.update(any(UUID.class), any(BeerDto.class))).thenAnswer(object -> {
            UUID uuid = object.getArgument(0);
            BeerDto beerDto = object.getArgument(1);
            beerDto.setId(uuid);
            return beerDto;
        });

        mockMvc.perform(put(URL + "/" + id.toString()).content(beerJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.LOCATION, "http://127.0.0.1:8080/api/v1/beer/" + id.toString()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedBeer));

        verify(beerServiceMock, atMostOnce()).update(id, resultBeer);
    }


    @Test
    public void whenPutHasFailedThenExceptionIsThrown() throws Exception {
        //given
        final UUID id = UUID.randomUUID();
        final String name = "Piwko";
        final BigDecimal price = new BigDecimal("13.25");
        final String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BeerStyle style = BeerStyle.GOSE;
        PersistenceException exception = new PersistenceException("Sth bad has happened");
        BeerDto beer = BeerDto.builder().beerName(name).beerStyle(style).price(price).upc(upc).build();
        String expectedBeer = objectMapper.writeValueAsString(new BreweryErrorResponse(HttpStatusCode.BAD_REQUEST.getCode(), exception.getMessage()));

        String beerJson = objectMapper.writeValueAsString(beer);
        when(beerServiceMock.update(any(UUID.class), any(BeerDto.class))).thenThrow(exception);

        mockMvc.perform(put(URL + "/" + id.toString()).content(beerJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedBeer));

        verify(beerServiceMock, atMostOnce()).update(id, beer);
    }

    @Test
    public void deleteRemovesBeer() throws Exception {
        //given
        final UUID id = UUID.randomUUID();

        mockMvc.perform(delete(URL + "/" + id.toString()))
                .andExpect(status().isNoContent());

        verify(beerServiceMock, atMostOnce()).deleteById(id);
    }

    @Test
    public void whenNameIsBlankThenValidationFails() throws Exception {
        //given
        final String name = "     ";
        BeerStyle style = BeerStyle.IPA;
        final BigDecimal price = new BigDecimal("6.78");
        final String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BeerDto beer = BeerDto.builder().beerName(name).beerStyle(style).price(price).upc(upc).build();
        String expectedError = "{\"code\":400,\"message\":\"Method Argument Violation\",\"errors\":[\"must not be blank\"]}";

        String beerJson = objectMapper.writeValueAsString(beer);
        mockMvc.perform(post(URL).content(beerJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedError));
    }

    @Test
    public void whenBeerStyleIsNullThenValidationFails() throws Exception {
        //given
        final String name = "jakies piwko";
        BeerStyle style = null;
        final BigDecimal price = new BigDecimal("6.78");
        final String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BeerDto beer = BeerDto.builder().beerName(name).beerStyle(style).upc(upc).price(price).build();
        String expectedError = "{\"code\":400,\"message\":\"Method Argument Violation\",\"errors\":[\"must not be null\"]}";

        String beerJson = objectMapper.writeValueAsString(beer);
        mockMvc.perform(post(URL).content(beerJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedError));
    }

    @Test
    public void whenUpcIsBlankThenValidationFails() throws Exception {
        //given
        final String name = "jakies piwko";
        BeerStyle style = BeerStyle.ALE;
        String upd = "     ";
        final BigDecimal price = new BigDecimal("6.78");
        String expectedError = "{\"code\":400,\"message\":\"Method Argument Violation\",\"errors\":[\"must not be blank\"]}";
        BeerDto beer = BeerDto.builder().beerName(name).beerStyle(style).upc(upd).price(price).build();

        String beerJson = objectMapper.writeValueAsString(beer);
        mockMvc.perform(post(URL).content(beerJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedError));
    }

    @Test
    public void whenBeerDoesNotExistsExceptionIsThrown() throws Exception {
        //given
        boolean showOnHand = false;
        final UUID uuid = UUID.randomUUID();
        when(beerServiceMock.getById(eq(uuid), eq(showOnHand))).thenThrow(new PersistenceException("It could not be found boss"));
        String expectedError = "{\"code\":404,\"message\":\"It could not be found boss\"}";

        mockMvc.perform(get(URL + "/" + uuid.toString()))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedError));

        verify(beerServiceMock, atLeastOnce()).getById(uuid, showOnHand);
    }

    @Test
    public void whenBeerCannotBeSavedThenExceptionIsThrown() throws Exception {
        //given
        final String name = "jakies piwko";
        BeerStyle style = BeerStyle.ALE;
        final String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        final BigDecimal price = new BigDecimal("6.78");
        BeerDto beer = BeerDto.builder().beerName(name).beerStyle(style).upc(upc).price(price).build();
        when(beerServiceMock.save(any(BeerDto.class))).thenThrow(new PersistenceException("It could not be saved boss"));
        String expectedError = "{\"code\":400,\"message\":\"It could not be saved boss\"}";

        mockMvc.perform(post(URL).content(objectMapper.writeValueAsString(beer)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedError));

        verify(beerServiceMock, atLeastOnce()).save(beer);
    }

    @Test
    public void whenBeerCannotBeUpdatedThenExceptionIsThrown() throws Exception {
        //given
        final UUID id = UUID.randomUUID();
        final String name = "jakies piwko";
        BeerStyle style = BeerStyle.ALE;
        final String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        final BigDecimal price = new BigDecimal("6.78");
        BeerDto beer = BeerDto.builder().beerName(name).beerStyle(style).upc(upc).price(price).build();
        when(beerServiceMock.update(any(UUID.class), any(BeerDto.class))).thenThrow(new PersistenceException("It could not be updated boss"));
        String expectedError = "{\"code\":400,\"message\":\"It could not be updated boss\"}";

        mockMvc.perform(put(URL + "/" + id.toString()).content(objectMapper.writeValueAsString(beer)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(expectedError));

        verify(beerServiceMock, atLeastOnce()).update(id, beer);
    }
}