package com.beerservice.service.inventory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.awaitility.Awaitility.await;

import com.beer.commons.model.dto.BeerInventoryDto;
import com.beer.commons.model.dto.HttpStatusCode;
import com.beer.commons.model.exception.BreweryException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.OffsetDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
class BeerInventoryServiceBeanTest {
    private static ClientAndServer mockServer;

    @Value("${beer-service.service.beer-inventory.hostname}")
    private String hostname;
    @Value("${beer-service.service.beer-inventory.url}")
    private String inventoryServicePath;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BeerInventoryService service;

    @BeforeAll
    static void setUpMock() {
        mockServer = new ClientAndServer("127.0.0.1", 9090, 9002);
    }

    @BeforeEach
    void reset() {
        mockServer.reset();
        await().timeout(10000, TimeUnit.MILLISECONDS).until(() -> mockServer.hasStarted());

    }

    @AfterAll
    static void stopServer() {
        mockServer.stop(false);
        await().timeout(10000, TimeUnit.MILLISECONDS).until(() -> !mockServer.isRunning());
    }

    @Test
    void whenSpecifiedBeerIsFoundThenCorrectSumIsReturned() throws JsonProcessingException {
        //given
        UUID beerId = UUID.randomUUID();
        String upc = "123456789";
        List<BeerInventoryDto> beerInventoryDtoList = getList(5, beerId, upc, 50);
        String jsonResponse = objectMapper.writeValueAsString(beerInventoryDtoList);
        int expectedAmount = beerInventoryDtoList.stream().mapToInt(BeerInventoryDto::getQuantityOnHand).sum();
        mockServer.when(HttpRequest.request().withMethod("GET").withPath(getPath(beerId)))
                .respond(HttpResponse.response().withBody(jsonResponse, MediaType.APPLICATION_JSON).withStatusCode(HttpStatusCode.OK.getCode()));

        //when
        int result = service.getOnHandInventory(beerId);

        //then
        assertThat(result).isEqualTo(expectedAmount);
    }

    @Test
    void whenSpecifiedBeerIsNotFoundThenExceptionIsThrown() {
        //given
        UUID beerId = UUID.randomUUID();
        mockServer.when(HttpRequest.request().withMethod("GET").withPath(getPath(beerId)))
                .respond(HttpResponse.response().withStatusCode(HttpStatusCode.NOT_FOUND.getCode()));

        //when
        //then
        assertThatThrownBy(() -> service.getOnHandInventory(beerId)).isInstanceOf(BreweryException.class)
                .hasMessage(String.format("Response %d but was expected %d", HttpStatusCode.NOT_FOUND.getCode(), HttpStatusCode.OK.getCode()));
    }


    @Test
    void whenBodyIsNotReceivedThenExceptionIsThrown() {
        //given
        UUID beerId = UUID.randomUUID();
        mockServer.when(HttpRequest.request().withMethod("GET").withPath(getPath(beerId)))
                .respond(HttpResponse.response().withStatusCode(HttpStatusCode.OK.getCode()));

        //when
        //then
        assertThatThrownBy(() -> service.getOnHandInventory(beerId)).isInstanceOf(BreweryException.class)
                .hasMessage("No response body was received");
    }

    List<BeerInventoryDto> getList(int items, UUID beerId, String upc, int maxOnHand) {
        List<BeerInventoryDto> beerInventoryDtoList = new LinkedList<>();
        Random random = new Random();

        for (int i = 0; i < items; i++) {
            BeerInventoryDto dto = BeerInventoryDto.builder()
                    .id(UUID.randomUUID())
                    .beerId(beerId)
                    .upc(upc)
                    .createdDate(OffsetDateTime.now())
                    .lastModifiedDate(OffsetDateTime.now())
                    .quantityOnHand(random.nextInt(maxOnHand))
                    .version(0L)
                    .build();
            beerInventoryDtoList.add(dto);
        }

        return beerInventoryDtoList;
    }

    String getPath(UUID beerId) {
        return String.format("/%s", inventoryServicePath).replace("{}", beerId.toString());
    }
}