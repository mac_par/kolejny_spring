package com.beerservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.beer.commons.model.domain.beer.BeerStyle;
import com.beer.commons.model.domain.beer.Beer;
import com.beer.commons.model.dto.BeerDto;
import com.beer.commons.model.exception.PersistenceException;
import com.beer.commons.model.mappers.BeerMapper;
import com.beer.commons.model.mappers.BeerMapperBean;
import com.beerservice.BeerUpcValues;
import com.beerservice.mapper.BeerMapperDecorator;
import com.beerservice.repositories.BeerRepository;
import com.beerservice.service.inventory.BeerInventoryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

class BeerServiceBeanTest {
    private BeerService service;
    private BeerRepository repository;
    private BeerInventoryService inventoryService;
    private BeerMapper beerMapperPrimary;
    private BeerMapper beerMapperSecondary;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {

        this.repository = mock(BeerRepository.class);
        this.inventoryService = mock(BeerInventoryService.class);
        this.beerMapperPrimary = new BeerMapperBean();
        this.beerMapperSecondary = new BeerMapperDecorator(this.beerMapperPrimary, this.inventoryService);
        this.service = new BeerServiceBean(repository, beerMapperPrimary, beerMapperSecondary);
    }

    @Test
    void whenBeerWithGivenIdExistsThenItsInstanceIsReturned() {
        //given
        boolean showOnHand = false;
        UUID id = UUID.randomUUID();
        String name = "Tomcio Paluch";
        BeerStyle style = BeerStyle.PALE_ALE;
        String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BigDecimal price = new BigDecimal("13.45");
        int onHand = 5;
        BeerDto expectedResponse = BeerDto.builder().id(id).beerName(name).beerStyle(style).price(price).upc(upc).quantityOnHand(onHand).build();
        when(repository.findById(eq(id))).thenReturn(Optional.of(beerMapperPrimary.convert(expectedResponse)));

        //when
        BeerDto result = service.getById(id, showOnHand);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResponse);
        verify(repository, times(1)).findById(id);
        verify(inventoryService, never()).getOnHandInventory(id);
    }

    @Test
    void whenBeerWithGivenIdExistsThenItsInstanceIsReturned2() {
        //given
        boolean showOnHand = true;
        UUID id = UUID.randomUUID();
        String name = "Tomcio Paluch";
        BeerStyle style = BeerStyle.PALE_ALE;
        String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BigDecimal price = new BigDecimal("13.45");
        int onHand = 5;
        BeerDto instance = BeerDto.builder().id(id).beerName(name).beerStyle(style).price(price).upc(upc).build();
        BeerDto expectedResult = BeerDto.builder().id(id).beerName(name).beerStyle(style).price(price).upc(upc).quantityOnHand(onHand).build();
        when(repository.findById(eq(id))).thenReturn(Optional.of(beerMapperPrimary.convert(instance)));
        when(inventoryService.getOnHandInventory(eq(id))).thenReturn(onHand);

        //when
        BeerDto result = service.getById(id, showOnHand);

        //then
        assertThat(result).isNotNull();
        assertThat(result).matches(item -> verifyObjects(result, expectedResult));
        verify(repository, times(1)).findById(id);
        verify(inventoryService, times(1)).getOnHandInventory(id);
    }

    @Test
    void whenGetHasFailedThenExceptionISThrown1() {
        //given
        boolean showOnHand = true;
        UUID id = UUID.randomUUID();
        when(repository.findById(eq(id))).thenReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> service.getById(id, showOnHand)).hasMessage(String.format("Beer %s can not be found", id)).isInstanceOf(PersistenceException.class);

        verify(repository, times(1)).findById(id);
        verify(inventoryService, never()).getOnHandInventory(id);
    }

    @Test
    void whenGetHasFailedThenExceptionISThrown2() {
        //given
        boolean showOnHand = false;
        UUID id = UUID.randomUUID();
        when(repository.findById(eq(id))).thenReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> service.getById(id, showOnHand)).hasMessage(String.format("Beer %s can not be found", id)).isInstanceOf(PersistenceException.class);

        verify(repository, times(1)).findById(id);
        verify(inventoryService, never()).getOnHandInventory(id);
    }

    @Test
    void whenBeerIsSavedThenNothingSpecialHappens() {
        //given
        UUID id = UUID.randomUUID();
        String name = "Tomcio Paluch";
        BeerStyle style = BeerStyle.PALE_ALE;
        String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BigDecimal price = new BigDecimal("13.45");
        int onHand = 5;
        BeerDto instance = BeerDto.builder().beerName(name).beerStyle(style).price(price).upc(upc).quantityOnHand(onHand).build();
        BeerDto expectedResponse = BeerDto.builder().id(id).beerName(name).beerStyle(style).price(price).upc(upc).quantityOnHand(onHand).build();
        when(repository.save(any(Beer.class))).thenAnswer(answer -> {
            Beer beer = answer.getArgument(0);
            beer.setId(id);
            return beer;
        });

        //when
        BeerDto result = service.save(instance);

        //then
        assertThat(result).isNotNull();
        assertThat(result).matches(pred -> verifyObjects(result, expectedResponse));
        verify(repository, times(1)).save(beerMapperPrimary.convert(result));
    }

    @Test
    void whenSaveHasFailedThenExceptionIsThrown() {
        //given
        UUID id = UUID.randomUUID();
        BeerDto instance = BeerDto.builder().build();
        when(repository.save(any(Beer.class))).thenThrow(RuntimeException.class);

        //when
        assertThatThrownBy(() -> service.save(instance)).isInstanceOf(RuntimeException.class);

        //then
        verify(repository, times(1)).save(beerMapperPrimary.convert(instance));
    }

    @Test
    void whenUpdateIsSuccessfulThenNoExceptionIsThrown() {
        //given
        UUID id = UUID.randomUUID();
        String name = "Tomcio Paluch";
        BeerStyle style = BeerStyle.PALE_ALE;
        String upc = BeerUpcValues.BEER_UPC_1_VALUE;
        BigDecimal price = new BigDecimal("13.45");
        int onHand = 5;
        BeerDto instance = BeerDto.builder().beerName(name).beerStyle(style).price(price).upc(upc).quantityOnHand(onHand).build();
        BeerDto expectedResponse = BeerDto.builder().id(id).beerName(name).beerStyle(style).price(price).upc(upc).quantityOnHand(onHand).build();
        when(repository.findById(eq(id))).thenAnswer(answer -> Optional.of(Beer.builder().id(answer.getArgument(0)).build()));
        when(repository.save(any(Beer.class))).thenAnswer(answer -> answer.getArgument(0));

        //when
        BeerDto result = service.update(id, instance);

        //then
        assertThat(result).isNotNull();
        assertThat(result).matches(pred -> verifyObjects(result, expectedResponse));
        verify(repository, times(1)).findById(id);
        verify(repository, times(1)).save(beerMapperPrimary.convert(result));
    }

    @Test
    void whenUpdateHasFailedThenExceptionIsThrown() {
        //given
        UUID id = UUID.randomUUID();
        BeerDto instance = BeerDto.builder().build();
        when(repository.findById(eq(id))).thenAnswer(answer -> Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> service.update(id, instance)).isInstanceOf(PersistenceException.class).hasMessage(String.format("Beer %s does not exist", id));
        verify(repository, times(1)).findById(id);
        verify(repository, never()).save(any(Beer.class));
    }

    @Test
    void whenBeerWithGivenIdISDeletedThenItsDeleted() {
        //given
        UUID id = UUID.randomUUID();

        //when
        service.deleteById(id);

        //then
        verify(repository, times(1)).deleteById(id);
    }

    private boolean verifyObjects(BeerDto result, BeerDto expectation) {
        assertThat(result.getId()).isEqualTo(expectation.getId());
        assertThat(result.getBeerName()).isEqualTo(expectation.getBeerName());
        assertThat(result.getBeerStyle()).isEqualTo(expectation.getBeerStyle());
        assertThat(result.getPrice()).isEqualTo(expectation.getPrice());
        assertThat(result.getQuantityOnHand()).isEqualTo(expectation.getQuantityOnHand());
        assertThat(result.getUpc()).isEqualTo(expectation.getUpc());
        return true;
    }
}