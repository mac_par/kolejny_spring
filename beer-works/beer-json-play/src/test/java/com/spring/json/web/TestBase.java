package com.spring.json.web;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.UUID;

class TestBase {
    BeerDto getBeerDto(UUID id, BeerStyle beerStyle, String price, BeerUpc upc, int onHand) {
        return BeerDto.builder().id(id).beerName("Beer").beerStyle(beerStyle).price(new BigDecimal(price)).quantityOnHand(onHand)
                .upc(upc).createdDate(OffsetDateTime.now()).lastModifiedDate(OffsetDateTime.now()).in(LocalDateTime.now()).out(LocalDateTime.now()).build();
    }
}
