package com.spring.json.web;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.UUID;

@JsonTest
class BeerDtoTest extends TestBase {
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldSerialize() throws JsonProcessingException {
        UUID id = UUID.randomUUID();
        BeerStyle style = BeerStyle.ALE;
        long upc = 125L;
        String price = "3.47";
        int onHand = 15;
        BeerDto beerDto = getBeerDto(id, style, price, BeerUpc.BEER_1_UPC, onHand);

        String json = objectMapper.writeValueAsString(beerDto);
        assertThat(json).isNotNull();
        BeerDto result = objectMapper.readValue(json, BeerDto.class);
        assertThat(result).isEqualTo(beerDto);
    }
}