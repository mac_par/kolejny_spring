package com.spring.json.web;

import static org.assertj.core.api.Assertions.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;

import java.time.OffsetDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@JsonTest
class BeerInventoryDtoTest extends TestBase {
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void serialization() throws JsonProcessingException {
        BeerInventoryDto dto = BeerInventoryDto.builder()
                .id(UUID.randomUUID())
                .beerId(UUID.randomUUID())
                .createdDate(OffsetDateTime.now())
                .lastModifiedDate(OffsetDateTime.now())
                .quantityOnHand(51)
                .upc("dfsdffsdfsf")
                .version(1L)
                .build();

        String json = objectMapper.writeValueAsString(dto);
        System.out.println(json);
        BeerInventoryDto copy = objectMapper.readValue(json, BeerInventoryDto.class);

        assertThat(copy).matches(item -> {
            assertThat(item.getId()).isEqualTo(dto.getId());
            assertThat(item.getBeerId()).isEqualTo(dto.getBeerId());
            assertThat(item.getQuantityOnHand()).isEqualTo(dto.getQuantityOnHand());
            assertThat(item.getUpc()).isEqualTo(dto.getUpc());
            return true;
        });
    }

    @Test
    void serializeList() throws JsonProcessingException {
        BeerInventoryDto dto = BeerInventoryDto.builder()
                .id(UUID.randomUUID())
                .beerId(UUID.randomUUID())
                .createdDate(OffsetDateTime.now())
                .lastModifiedDate(OffsetDateTime.now())
                .quantityOnHand(51)
                .upc("dfsdffsdfsf")
                .version(1L)
                .build();
        List<BeerInventoryDto> list = new LinkedList<>();
        list.add(dto);
        list.add(dto);
        String json = objectMapper.writeValueAsString(list);
        System.out.println(json);
        TypeReference<List<BeerInventoryDto>> beerTypeReference = new TypeReference<>() {
        };
        List<BeerInventoryDto> list2 = objectMapper.readValue(json, beerTypeReference);

        assertThat(list2).isNotNull();
        assertThat(list2).hasSize(2);
    }

}