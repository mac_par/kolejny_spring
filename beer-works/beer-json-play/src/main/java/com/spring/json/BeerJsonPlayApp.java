package com.spring.json;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeerJsonPlayApp {

    public static void main(String[] args) {
        SpringApplication.run(BeerJsonPlayApp.class, args);
    }

}
