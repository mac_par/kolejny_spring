package com.spring.json.web;

public enum BeerStyle {
    LAGER,
    PILSNER,
    STOUT,
    GOSE,
    PORTER,
    ALE,
    WHEAT,
    IPA,
    PALE_ALE,
    SAISON;
}
