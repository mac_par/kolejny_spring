package com.spring.json.web;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum BeerUpc {

    @JsonProperty(BeerUpcValues.BEER_UPC_1_VALUE)
    BEER_1_UPC,
    @JsonProperty(BeerUpcValues.BEER_UPC_2_VALUE)
    BEER_2_UPC,
    @JsonProperty(BeerUpcValues.BEER_UPC_3_VALUE)
    BEER_3_UPC;

}
