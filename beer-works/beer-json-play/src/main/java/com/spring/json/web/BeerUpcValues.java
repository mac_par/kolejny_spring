package com.spring.json.web;

class BeerUpcValues {
    static final String BEER_UPC_1_VALUE = "0631234200036";
    static final String BEER_UPC_2_VALUE = "0631234300019";
    static final String BEER_UPC_3_VALUE = "0083783375123";
}
