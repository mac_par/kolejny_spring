package com.beer.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeerGatewayApp {
    public static void main(String[] args) {
        SpringApplication.run(BeerGatewayApp.class, args);
    }
}
