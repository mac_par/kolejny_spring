package com.beer.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@Profile("!local-discovery")
@org.springframework.context.annotation.Configuration
public class GatewayConfiguration {
    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(route -> route.path("/api/v1/beer", "/api/v1/beer/*", "/api/v1/beerUpc/*").uri("http://127.0.0.1:8080").id("beer-service"))
                .route(route -> route.path("/api/v1/customers", "/api/v1/customers/**").uri("http://127.0.0.1:8081").id("beer-order-service"))
                .route(route -> route.path("/api/v1/beer/*/inventory", "/api/v1/beer/inventory").uri("http://127.0.0.1:8082").id("beer-inventory-service"))
                .route(route -> route.path("/api/v1/failover/{beerId}/inventory").uri("http://127.0.0.1:8083").id("inventory-failover"))
                .build();
    }
}
