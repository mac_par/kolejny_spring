package com.beer.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("local-discovery")
@Configuration
public class LoadBalancedRoutesConfiguration {
    @Bean
    RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(route -> route.path("/api/v1/beer", "/api/v1/beer/*", "/api/v1/beerUpc/*").uri("lb://beer-service").id("beer-service"))
                .route(route -> route.path("/api/v1/customers", "/api/v1/customers/**").uri("lb://beer-order-service").id("beer-order-service"))
                .route(route -> route.path("/api/v1/beer/*/inventory", "/api/v1/beer/inventory")
                        //TODO: nie działa ;/
//                        .filters(f -> f.circuitBreaker(c -> c.setName("inventoryCB").setFallbackUri("forward:/api/v1/failover/**").setRouteId("inventory-fail")))
                        .uri("lb://inventory-service").id("beer-inventory-service"))
                .route(route -> route.path("/api/v1/failover/{beerId}/inventory").uri("lb://inventory-failover").id("inventory-failover"))
                .build();
    }
}
