package com.beer.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("youtube")
@Configuration
public class GoogleConfig {

    @Bean
    public RouteLocator googleConfigRoute(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(route -> route.path("/youtube").uri("https://www.youtube.com/").id("youtube"))
                .build();
    }
}
