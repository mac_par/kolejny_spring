package com.beer.commons.model.mappers;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;


public class DatesMapperBean implements DatesMapper {

    @Override
    public Timestamp convert(OffsetDateTime date) {
        if (date == null) {
            return null;
        } else {
            return Timestamp.valueOf(date.toLocalDateTime());
        }
    }

    @Override
    public OffsetDateTime convert(Timestamp date) {
        if (date == null) {
            return null;
        } else {
            return OffsetDateTime.of(date.toLocalDateTime(), ZoneOffset.systemDefault().getRules().getOffset(date.toInstant()));
        }
    }
}
