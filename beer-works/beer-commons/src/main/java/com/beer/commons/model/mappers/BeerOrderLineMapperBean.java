package com.beer.commons.model.mappers;

import com.beer.commons.model.domain.customer.BeerOrderLine;
import com.beer.commons.model.dto.BeerOrderLineDto;
import org.springframework.beans.factory.annotation.Autowired;

public class BeerOrderLineMapperBean implements BeerOrderLineMapper {
    private final DatesMapper datesMapper;

    @Autowired
    public BeerOrderLineMapperBean(DatesMapper datesMapper) {
        this.datesMapper = datesMapper;
    }

    @Override
    public BeerOrderLine convert(BeerOrderLineDto orderLineDto) {
        if (orderLineDto == null) {
            return null;
        }

        BeerOrderLine orderLine = BeerOrderLine.builder()
                .id(orderLineDto.getId())
                .beerId(orderLineDto.getBeerId())
                .orderQuantity(orderLineDto.getOrderQuantity())
                .upc(orderLineDto.getUpc())
                .createdDate(datesMapper.convert(orderLineDto.getCreatedDate()))
                .lastModifiedDate(datesMapper.convert(orderLineDto.getLastModifiedDate()))
                .beerId(orderLineDto.getBeerId())
                .build();
        if (orderLineDto.getVersion() != null) {
            orderLine.setVersion(orderLineDto.getVersion());
        }
        return orderLine;
    }

    @Override
    public BeerOrderLineDto convert(BeerOrderLine orderLine) {
        if (orderLine == null) {
            return null;
        }

        BeerOrderLineDto orderLineDto = BeerOrderLineDto.builder()
                .id(orderLine.getId())
                .beerId(orderLine.getBeerId())
                .orderQuantity(orderLine.getOrderQuantity())
                .upc(orderLine.getUpc())
                .createdDate(datesMapper.convert(orderLine.getCreatedDate()))
                .lastModifiedDate(datesMapper.convert(orderLine.getLastModifiedDate()))
                .build();
        if (orderLine.getVersion() != null) {
            orderLineDto.setVersion(orderLine.getVersion());
        }
        return orderLineDto;
    }
}
