package com.beer.commons.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;

import java.util.Collection;

@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BreweryErrorResponse {
    private int code;
    private String message;
    private Collection<String> errors;

    public BreweryErrorResponse() {
    }

    public BreweryErrorResponse(int code, String message) {
        this.message = message;
        this.code = code;
    }

    public BreweryErrorResponse(int code, String message, Collection<String> errors) {
        this.message = message;
        this.code = code;
        this.errors = errors;
    }

    public int getCode() {
        return code;
    }

    public Collection<String> getErrors() {
        return errors;
    }
}
