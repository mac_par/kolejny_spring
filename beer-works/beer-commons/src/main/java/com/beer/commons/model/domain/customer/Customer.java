package com.beer.commons.model.domain.customer;

import com.beer.commons.model.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Getter
@Setter
//@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
@Table(name = "customers")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Customer extends BaseEntity {

    private static final long serialVersionUID = -2625828710370565286L;

    @Builder
    public Customer(UUID id, Long version, Timestamp createdDate, Timestamp lastModifiedDate, String name, UUID apiKey, Set<BeerOrder> beerOrders) {
        super(id, version, createdDate, lastModifiedDate);
        this.name = name;
        this.apiKey = apiKey;
        this.beerOrders = beerOrders;
    }

    private String name;
    @Type(type = "org.hibernate.type.UUIDCharType")
    @Column(length = 36, name = "api_key")
    private UUID apiKey;
    @OneToMany(mappedBy = "customer")
    private Set<BeerOrder> beerOrders;
}

