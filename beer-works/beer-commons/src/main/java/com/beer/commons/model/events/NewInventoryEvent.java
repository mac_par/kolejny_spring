package com.beer.commons.model.events;

import com.beer.commons.model.dto.BeerDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NewInventoryEvent implements java.io.Serializable {
    private static final long serialVersionUID = 7722172732941237638L;
    private int amountToProduce;
    private BeerDto beerDto;
}
