package com.beer.commons.model.dto;

import com.beer.commons.model.domain.OrderStatus;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BeerOrderDto extends BaseItem {

    private static final long serialVersionUID = 7002354376359132361L;

    @Builder
    public BeerOrderDto(UUID id, Long version, OffsetDateTime createdDate, OffsetDateTime lastModifiedDate, UUID customerId,
                        String customerRef, List<BeerOrderLineDto> beerOrderLines, OrderStatus orderStatus, String orderStatusCallbackUrl) {
        super(id, version, createdDate, lastModifiedDate);
        this.customerId = customerId;
        this.customerRef = customerRef;
        this.beerOrderLines = beerOrderLines;
        this.orderStatus = orderStatus;
        this.orderStatusCallbackUrl = orderStatusCallbackUrl;
    }

    private UUID customerId;
    private String customerRef;
    private List<BeerOrderLineDto> beerOrderLines;
    private OrderStatus orderStatus;
    private String orderStatusCallbackUrl;
}
