package com.beer.commons.model.mappers;

import com.beer.commons.model.domain.customer.BeerOrderLine;
import com.beer.commons.model.dto.BeerOrderLineDto;

public interface BeerOrderLineMapper {

    BeerOrderLine convert(BeerOrderLineDto orderLineDto);

    BeerOrderLineDto convert(BeerOrderLine orderLine);
}
