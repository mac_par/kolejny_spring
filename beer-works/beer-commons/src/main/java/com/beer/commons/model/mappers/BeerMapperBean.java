package com.beer.commons.model.mappers;

import com.beer.commons.model.domain.beer.Beer;
import com.beer.commons.model.dto.BeerDto;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

public class BeerMapperBean implements BeerMapper {
    @Override
    public Beer convert(BeerDto beer) {
        if (beer == null) {
            return null;
        }

        return Beer.builder()
                .id(beer.getId())
                .beerName(beer.getBeerName())
                .beerStyle(beer.getBeerStyle())
                .price(beer.getPrice())
                .quantityOnHand(beer.getQuantityOnHand())
                .upc(beer.getUpc())
                .version(beer.getVersion())
                .createdDate(convertToTimestamp(beer.getCreatedDate()))
                .lastModifiedDate(convertToTimestamp(beer.getLastModifiedDate()))
                .build();
    }

    private Timestamp convertToTimestamp(OffsetDateTime modified) {
        if (modified == null) {
            return null;
        }
        return Timestamp.valueOf(modified.atZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());
    }

    @Override
    public BeerDto convert(Beer beer) {
        if (beer == null) {
            return null;
        }

        return BeerDto.builder()
                .id(beer.getId())
                .beerName(beer.getBeerName())
                .beerStyle(beer.getBeerStyle())
                .price(beer.getPrice())
                .quantityOnHand(beer.getQuantityOnHand())
                .upc(beer.getUpc())
                .version(beer.getVersion())
                .createdDate(convertToOffsetDateTime(beer.getCreatedDate()))
                .lastModifiedDate(convertToOffsetDateTime(beer.getLastModifiedDate()))
                .build();
    }

    private OffsetDateTime convertToOffsetDateTime(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return OffsetDateTime.of(timestamp.toLocalDateTime(), ZoneOffset.UTC);
    }

    @Override
    public Beer update(Beer beer, BeerDto dto) {
        if (beer == null || dto == null) {
            return beer;
        }

        if (!Objects.equals(dto.getBeerName(), beer.getBeerName())) {
            beer.setBeerName(dto.getBeerName());
        }

        if (!Objects.equals(dto.getBeerStyle(), beer.getBeerStyle())) {
            beer.setBeerStyle(dto.getBeerStyle());
        }

        if (!Objects.equals(dto.getUpc(), beer.getUpc())) {
            beer.setUpc(dto.getUpc());
        }

        if (!Objects.equals(dto.getQuantityOnHand(), beer.getQuantityOnHand())) {
            beer.setQuantityOnHand(dto.getQuantityOnHand());
        }

        if (!Objects.equals(dto.getPrice(), beer.getPrice())) {
            beer.setPrice(dto.getPrice());
        }

        return beer;
    }
}
