package com.beer.commons.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BeerInventoryDto extends BaseItem {

    private static final long serialVersionUID = 5311277010146951342L;

    @Builder
    public BeerInventoryDto(UUID id, Long version, OffsetDateTime createdDate, OffsetDateTime lastModifiedDate, UUID beerId, int quantityOnHand, String upc) {
        super(id, version, createdDate, lastModifiedDate);
        this.beerId = beerId;
        this.quantityOnHand = quantityOnHand;
        this.upc = upc;
    }

    private UUID beerId;
    private int quantityOnHand;
    private String upc;
}
