package com.beer.commons.model.mappers;

import com.beer.commons.model.domain.inventory.BeerInventory;
import com.beer.commons.model.dto.BeerInventoryDto;
import org.springframework.beans.factory.annotation.Autowired;

public class BeerInventoryMapperBean implements BeerInventoryMapper {
    private final DatesMapper datesMapper;

    @Autowired
    public BeerInventoryMapperBean(DatesMapper datesMapper) {
        this.datesMapper = datesMapper;
    }

    @Override
    public BeerInventory convert(BeerInventoryDto dto) {
        if (dto == null) {
            return null;
        }

        BeerInventory inventory = BeerInventory.builder()
                .id(dto.getId())
                .beerId(dto.getBeerId())
                .quantityOnHand(dto.getQuantityOnHand())
                .upc(dto.getUpc())
                .createdDate(datesMapper.convert(dto.getCreatedDate()))
                .lastModifiedDate(datesMapper.convert(dto.getLastModifiedDate()))
                .build();

        if (dto.getVersion() != null) {
            inventory.setVersion(dto.getVersion());
        }

        return inventory;
    }

    @Override
    public BeerInventoryDto convert(BeerInventory inventory) {
        if (inventory == null) {
            return null;
        }

        BeerInventoryDto dto = BeerInventoryDto.builder()
                .id(inventory.getId())
                .beerId(inventory.getBeerId())
                .quantityOnHand(inventory.getQuantityOnHand())
                .upc(inventory.getUpc())
                .createdDate(datesMapper.convert(inventory.getCreatedDate()))
                .lastModifiedDate(datesMapper.convert(inventory.getLastModifiedDate()))
                .build();

        if (inventory.getVersion() != null) {
            dto.setVersion(inventory.getVersion());
        }

        return dto;
    }
}
