package com.beer.commons.model.events;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BrewBeerEvent implements java.io.Serializable {
    private static final long serialVersionUID = -1641484462978510021L;
    private int amountToBrew;
    private UUID beerId;
}
