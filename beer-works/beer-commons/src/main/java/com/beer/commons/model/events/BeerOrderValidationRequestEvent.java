package com.beer.commons.model.events;

import com.beer.commons.model.dto.BeerOrderDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class BeerOrderValidationRequestEvent implements java.io.Serializable {
    private static final long serialVersionUID = -5563708467425297088L;

    private BeerOrderDto beerOrderDto;

}
