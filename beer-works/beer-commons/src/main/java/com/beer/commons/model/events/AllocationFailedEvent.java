package com.beer.commons.model.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AllocationFailedEvent implements java.io.Serializable {
    private static final long serialVersionUID = 2192469330365504433L;

    private UUID orderId;
}
