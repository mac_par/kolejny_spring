package com.beer.commons.model.dto;

import com.beer.commons.model.domain.beer.BeerStyle;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BeerDto extends BaseItem {

    private static final long serialVersionUID = 630521203765095425L;

    @Builder
    public BeerDto(UUID id, @NotNull @Positive Long version, @NotNull OffsetDateTime createdDate, @NotNull OffsetDateTime lastModifiedDate,
                   @NotBlank String beerName, @NotNull BeerStyle beerStyle, @NotNull @NotBlank String upc, Integer quantityOnHand, @NotNull BigDecimal price) {
        super(id, version, createdDate, lastModifiedDate);
        this.beerName = beerName;
        this.beerStyle = beerStyle;
        this.upc = upc;
        this.quantityOnHand = quantityOnHand;
        this.price = price;
    }

    @NotBlank
    private String beerName;
    @NotNull
    private BeerStyle beerStyle;
    @NotNull
    @NotBlank
    private String upc;
    private Integer quantityOnHand;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @NotNull
    private BigDecimal price;

}
