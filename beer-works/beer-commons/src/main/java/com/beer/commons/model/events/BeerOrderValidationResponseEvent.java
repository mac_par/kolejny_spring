package com.beer.commons.model.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BeerOrderValidationResponseEvent implements java.io.Serializable {
    private static final long serialVersionUID = -6343977797212265295L;

    private UUID orderId;
    private boolean validationResult;
    private List<String> errors;

}
