package com.beer.commons.model.mappers;

import com.beer.commons.model.domain.customer.BeerOrder;
import com.beer.commons.model.dto.BeerOrderDto;

public interface BeerOrderMapper {

    BeerOrder convert(BeerOrderDto beerOrderDto);

    BeerOrderDto convert(BeerOrder beerOrder);
}
