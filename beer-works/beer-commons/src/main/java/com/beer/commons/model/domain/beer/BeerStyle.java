package com.beer.commons.model.domain.beer;

public enum BeerStyle {
    LAGER,
    PILSNER,
    STOUT,
    GOSE,
    PORTER,
    ALE,
    WHEAT,
    IPA,
    PALE_ALE,
    SAISON;
}
