package com.beer.commons.model.mappers;

import com.beer.commons.model.domain.beer.Beer;
import com.beer.commons.model.dto.BeerDto;

public interface BeerMapper {
    Beer convert(BeerDto beer);

    BeerDto convert(Beer beer);

    Beer update(Beer beer, BeerDto dto);
}
