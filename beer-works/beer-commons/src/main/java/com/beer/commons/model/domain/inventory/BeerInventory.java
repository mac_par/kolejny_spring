package com.beer.commons.model.domain.inventory;

import com.beer.commons.model.domain.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.sql.Timestamp;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "beer_inventory")
public class BeerInventory extends BaseEntity {

    private static final long serialVersionUID = -5027522783586990676L;

    @Builder
    public BeerInventory(UUID id, Long version, Timestamp createdDate, Timestamp lastModifiedDate, UUID beerId, String upc, int quantityOnHand) {
        super(id, version, createdDate, lastModifiedDate);
        this.beerId = beerId;
        this.upc = upc;
        this.quantityOnHand = quantityOnHand;
    }

    @Type(type = "org.hibernate.type.UUIDCharType")
    @Column(name = "beer_id")
    private UUID beerId;
    private String upc;
    @Column(name = "quantity_on_hand")
    private int quantityOnHand;
}
