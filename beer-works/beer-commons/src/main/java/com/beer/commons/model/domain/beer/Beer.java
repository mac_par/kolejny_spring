package com.beer.commons.model.domain.beer;

import com.beer.commons.model.domain.BaseEntity;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "beers")
@EqualsAndHashCode(callSuper = true)
public class Beer extends BaseEntity {

    private static final long serialVersionUID = 3105492816111027780L;

    @Builder
    public Beer(UUID id, Long version, Timestamp createdDate, Timestamp lastModifiedDate, String beerName,
                BeerStyle beerStyle, String upc, Integer quantityOnHand, BigDecimal price) {
        super(id, version, createdDate, lastModifiedDate);
        this.beerName = beerName;
        this.beerStyle = beerStyle;
        this.upc = upc;
        this.quantityOnHand = quantityOnHand;
        this.price = price;
    }

    @Column(name = "beer_name")
    private String beerName;
    @Enumerated(EnumType.STRING)
    @Column(name = "beer_style")
    private BeerStyle beerStyle;
    @Column(unique = true)
    private String upc;
    @Column(name = "quantity_on_hand")
    private Integer quantityOnHand;
    private BigDecimal price;

}
