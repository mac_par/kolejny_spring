package com.beer.commons.model.events;

import com.beer.commons.model.dto.BeerOrderDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AllocateOrderRequest implements java.io.Serializable {
    private static final long serialVersionUID = 9063321208864546946L;
    private BeerOrderDto beerOrderDto;
}
