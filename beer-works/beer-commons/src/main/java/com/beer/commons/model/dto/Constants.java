package com.beer.commons.model.dto;

public class Constants {
    public static final String OFFSETDATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";
}
