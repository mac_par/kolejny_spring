package com.beer.commons.model;

public class BeerQueue {
    public static final String BREW_BEER_QUEUE = "brew-beer-queue";
    public static final String BEER_ORDER_SERVICE_QUEUE = "beer-order-service-queue";
    public static final String BEER_INVENTORY_SERVICE_QUEUE = "beer-inventory-queue";
    public static final String BEER_VALIDATION_REQUEST_QUEUE = "beer-validation-queue";
    public static final String BEER_VALIDATION_RESPONSE_QUEUE = "validate-order-result";
    public static final String ALLOCATE_ORDER_REQUEST_QUEUE = "allocate-order-result";
    public static final String ALLOCATE_ORDER_RESULT_QUEUE = "allocation-order-result";
    public static final String ALLOCATE_ORDER_FAILED_QUEUE = "allocation-order-failed-queue";
    public static final String DEALLOCATE_ORDER_QUEUE = "cancel-order-queue";
}
