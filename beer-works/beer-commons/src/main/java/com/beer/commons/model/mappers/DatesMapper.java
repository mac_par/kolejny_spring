package com.beer.commons.model.mappers;

import java.sql.Timestamp;
import java.time.OffsetDateTime;

public interface DatesMapper {
    Timestamp convert(OffsetDateTime date);

    OffsetDateTime convert(Timestamp date);
}
