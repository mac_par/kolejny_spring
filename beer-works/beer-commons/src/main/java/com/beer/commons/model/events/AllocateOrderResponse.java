package com.beer.commons.model.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AllocateOrderResponse implements java.io.Serializable {
    private static final long serialVersionUID = 9063321208864546946L;
    private UUID orderId;
    private boolean allocationError;
    private boolean pendingInventory;
}
