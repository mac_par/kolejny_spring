package com.beer.commons.model.exception;

public class PersistenceException extends RuntimeException {
    public PersistenceException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return String.format("%s:%s", PersistenceException.class.getName(), getMessage());
    }
}
