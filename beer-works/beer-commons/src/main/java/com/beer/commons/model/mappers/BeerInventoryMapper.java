package com.beer.commons.model.mappers;

import com.beer.commons.model.domain.inventory.BeerInventory;
import com.beer.commons.model.dto.BeerInventoryDto;

public interface BeerInventoryMapper {

    BeerInventory convert(BeerInventoryDto dto);

    BeerInventoryDto convert(BeerInventory inventory);

}
