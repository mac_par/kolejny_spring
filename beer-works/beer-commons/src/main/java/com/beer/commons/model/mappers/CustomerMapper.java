package com.beer.commons.model.mappers;

import com.beer.commons.model.domain.customer.Customer;
import com.beer.commons.model.dto.CustomerDto;

public interface CustomerMapper {
    Customer convert(CustomerDto customer);

    CustomerDto convert(Customer customer);

    Customer update(Customer customer, CustomerDto dto);
}
