package com.beer.commons.model.exception;

import com.beer.commons.model.BreweryErrorResponse;
import com.beer.commons.model.dto.HttpStatusCode;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class BreweryExceptionHandler {

    @ExceptionHandler({ConstraintViolationException.class})
    ResponseEntity<BreweryErrorResponse> respond(ConstraintViolationException exception) {
        List<String> errors = exception.getConstraintViolations().stream().map(BreweryExceptionHandler::getViolationDetails).collect(Collectors.toList());
        BreweryErrorResponse error = new BreweryErrorResponse(HttpStatusCode.BAD_REQUEST.getCode(), "Constraints Violation", errors);

        return getBreweryErrorResponseResponseEntity(error);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    ResponseEntity<BreweryErrorResponse> respondMethodValidation(MethodArgumentNotValidException exception) {
        List<String> errors = exception.getBindingResult().getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
        BreweryErrorResponse error = new BreweryErrorResponse(HttpStatusCode.BAD_REQUEST.getCode(), "Method Argument Violation", errors);

        return getBreweryErrorResponseResponseEntity(error);
    }

    @ExceptionHandler({BreweryException.class})
    ResponseEntity<BreweryErrorResponse> processBreweryException(BreweryException exception) {
        BreweryErrorResponse error = new BreweryErrorResponse(exception.getStatus().getCode(), exception.getMessage());

        return getBreweryErrorResponseResponseEntity(error);
    }

    private ResponseEntity<BreweryErrorResponse> getBreweryErrorResponseResponseEntity(BreweryErrorResponse error) {
        HttpHeaders httpHeaders = getHttpHeaders();
        return new ResponseEntity<>(error, httpHeaders, HttpStatus.valueOf(error.getCode()));
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return httpHeaders;
    }

    static private <T> String getViolationDetails(ConstraintViolation<T> violation) {
        return violation.getPropertyPath() + ":" + violation.getMessage();
    }
}
