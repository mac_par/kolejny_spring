package com.beer.commons.model.mappers;

import com.beer.commons.model.domain.customer.BeerOrder;
import com.beer.commons.model.domain.customer.BeerOrderLine;
import com.beer.commons.model.dto.BeerOrderDto;
import com.beer.commons.model.dto.BeerOrderLineDto;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BeerOrderMapperBean implements BeerOrderMapper {
    private final DatesMapper datesMapper;
    private final BeerOrderLineMapper beerOrderLineMapper;

    @Autowired
    public BeerOrderMapperBean(DatesMapper datesMapper, BeerOrderLineMapper beerOrderLineMapper) {
        this.datesMapper = datesMapper;
        this.beerOrderLineMapper = beerOrderLineMapper;
    }

    @Override
    public BeerOrder convert(BeerOrderDto beerOrderDto) {
        if (beerOrderDto == null) {
            return null;
        }

        BeerOrder order = BeerOrder.builder()
                .id(beerOrderDto.getId())
                .beerOrderLines(toBeerOrderLineSet(beerOrderDto.getBeerOrderLines()))
                .customerRef(beerOrderDto.getCustomerRef())
                .orderStatus(beerOrderDto.getOrderStatus())
                .orderStatusCallbackUrl(beerOrderDto.getOrderStatusCallbackUrl())
                .createdDate(datesMapper.convert(beerOrderDto.getCreatedDate()))
                .lastModifiedDate(datesMapper.convert(beerOrderDto.getLastModifiedDate()))
                .build();
        if (beerOrderDto.getVersion() != null) {
            order.setVersion(beerOrderDto.getVersion());
        }

        return order;
    }

    @Override
    public BeerOrderDto convert(BeerOrder beerOrder) {
        if (beerOrder == null) {
            return null;
        }

        BeerOrderDto order = BeerOrderDto.builder()
                .id(beerOrder.getId())
                .beerOrderLines(toBeerOrderLineDtoList(beerOrder.getBeerOrderLines()))
                .customerId(beerOrder.getCustomer().getId())
                .customerRef(beerOrder.getCustomerRef())
                .orderStatus(beerOrder.getOrderStatus())
                .orderStatusCallbackUrl(beerOrder.getOrderStatusCallbackUrl())
                .createdDate(datesMapper.convert(beerOrder.getCreatedDate()))
                .lastModifiedDate(datesMapper.convert(beerOrder.getLastModifiedDate()))
                .build();
        if (beerOrder.getVersion() != null) {
            order.setVersion(beerOrder.getVersion());
        }

        return order;
    }

    private Set<BeerOrderLine> toBeerOrderLineSet(List<BeerOrderLineDto> list) {
        if (list == null) {
            return null;
        }

        return list.stream().map(beerOrderLineMapper::convert).collect(Collectors.toSet());
    }

    private List<BeerOrderLineDto> toBeerOrderLineDtoList(Set<BeerOrderLine> set) {
        if (set == null) {
            return null;
        }

        return set.stream().map(beerOrderLineMapper::convert).collect(Collectors.toList());
    }
}
