package com.beer.commons.model.dto;

import com.beer.commons.model.exception.BreweryException;

public enum HttpStatusCode {
    OK(200, "OK"),
    CREATED(201, "CREATED"),
    ACCEPTED(202, "ACCEPTED"),
    NO_CONTENT(204, "NO CONTENT"),
    MOVED_PERMANENTLY(301, "MOVED PERMANENTLY"),
    NOT_MODIFIED(304, "NOT MODIFIED"),
    BAD_REQUEST(400, "BAD REQUEST"),
    UNAUTHORIZED(401, "UNAUTHORIZED"),
    FORBIDDEN(403, "FORBIDDEN"),
    NOT_FOUND(404, "NOT FOUND"),
    INTERNAL_SERVER_ERROR(500, "INTERNAL SERVER ERROR"),
    NOT_IMPLEMENTED(501, "NOT IMPLEMENTED");

    HttpStatusCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private final int code;
    private final String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static HttpStatusCode getByCode(int code) {
        for(HttpStatusCode instance: HttpStatusCode.values()) {
            if (Integer.compare(code, instance.code) == 0) {
                return instance;
            }
        }

        throw new BreweryException(HttpStatusCode.NOT_IMPLEMENTED, String.format("Requested code [%d] was not implemented", code));
    }
}
