package com.beer.commons.model.exception;

import com.beer.commons.model.dto.HttpStatusCode;

public class BreweryException extends RuntimeException {
    private final HttpStatusCode status;

    public BreweryException(HttpStatusCode status, String message) {
        super(message);
        this.status = status;
    }

    public BreweryException(HttpStatusCode status, String message, Throwable cause) {
        super(message, cause);
        this.status = status;
    }

    public HttpStatusCode getStatus() {
        return status;
    }

    @Override
    public String toString() {
        if (getCause() == null) {
            return String.format("BreweryException[%d]: %s", status.getCode(), super.getMessage());
        } else {
            return String.format("BreweryException[%d]: %s - cause: %s", status.getCode(), super.getMessage(), getCause().getMessage());
        }
    }
}
