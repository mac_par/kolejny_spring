package com.beer.commons.model.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class CustomerDto extends BaseItem {

    private static final long serialVersionUID = -8396806033588107267L;

    @Builder
    public CustomerDto(UUID id, Long version, OffsetDateTime createdDate, OffsetDateTime lastModifiedDate, String name) {
        super(id, version, createdDate, lastModifiedDate);
        this.name = name;
    }

    @Size(min = 3, max = 100)
    @NotBlank
    private String name;

}
