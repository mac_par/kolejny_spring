package com.beer.commons.model.domain.customer;

import com.beer.commons.model.domain.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.sql.Timestamp;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "beer_order_lines")
public class BeerOrderLine extends BaseEntity {

    private static final long serialVersionUID = 8647445239306808554L;

    @Builder
    public BeerOrderLine(UUID id, Long version, Timestamp createdDate, Timestamp lastModifiedDate, BeerOrder beerOrder,
                         UUID beerId, int orderQuantity, int quantityAllocated, String upc) {
        super(id, version, createdDate, lastModifiedDate);
        this.beerOrder = beerOrder;
        this.beerId = beerId;
        this.orderQuantity = orderQuantity;
        this.quantityAllocated = quantityAllocated;
        this.upc = upc;
    }

    @JoinColumn(name = "beer_order_id")
    @ManyToOne
    private BeerOrder beerOrder;
    @Type(type = "org.hibernate.type.UUIDCharType")
    @Column(length = 36, name = "beer_id")
    private UUID beerId;
    private String upc;
    @Column(name = "ordered_quantity")
    private int orderQuantity;
    @Column(name = "allocated_quantity")
    private int quantityAllocated;

    protected boolean canEqual(final Object other) {
        return other instanceof BeerOrderLine;
    }

}
