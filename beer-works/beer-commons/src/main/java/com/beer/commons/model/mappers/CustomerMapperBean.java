package com.beer.commons.model.mappers;

import com.beer.commons.model.domain.customer.Customer;
import com.beer.commons.model.dto.CustomerDto;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

public class CustomerMapperBean implements CustomerMapper {
    private final DatesMapper mapper;

    @Autowired
    public CustomerMapperBean(DatesMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public Customer convert(CustomerDto customer) {
        if (customer == null) {
            return null;
        }
        return Customer.builder()
                .id(customer.getId())
                .name(customer.getName())
                .createdDate(mapper.convert(customer.getCreatedDate()))
                .lastModifiedDate(mapper.convert(customer.getLastModifiedDate()))
                .version(customer.getVersion())
                .build();
    }

    @Override
    public CustomerDto convert(Customer customer) {
        if (customer == null) {
            return null;
        }
        return CustomerDto.builder()
                .id(customer.getId())
                .name(customer.getName())
                .createdDate(mapper.convert(customer.getCreatedDate()))
                .lastModifiedDate(mapper.convert(customer.getLastModifiedDate()))
                .version(customer.getVersion())
                .build();
    }

    @Override
    public Customer update(Customer customer, CustomerDto dto) {
        if (customer == null || dto == null) {
            return customer;
        }

        if (!Objects.equals(customer.getName(), dto.getName())) {
            customer.setName(dto.getName());
        }
        return customer;
    }
}
