package com.beer.commons.model.dto;

import com.beer.commons.model.domain.beer.BeerStyle;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BeerOrderLineDto extends BaseItem {

    private static final long serialVersionUID = 1461397054050223248L;

    @Builder
    public BeerOrderLineDto(UUID id, Long version, OffsetDateTime createdDate, OffsetDateTime lastModifiedDate,
                            String upc, String beerName, UUID beerId, int orderQuantity, BigDecimal price, BeerStyle beerStyle) {
        super(id, version, createdDate, lastModifiedDate);
        this.upc = upc;
        this.beerName = beerName;
        this.beerId = beerId;
        this.orderQuantity = orderQuantity;
        this.price = price;
        this.beerStyle = beerStyle;
    }

    private String upc;
    private String beerName;
    private UUID beerId;
    private int orderQuantity;
    private int quantityAllocated;
    private BigDecimal price;
    private BeerStyle beerStyle;
}
