package com.customer.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.awaitility.Awaitility.await;

import com.beer.commons.model.dto.CustomerDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class CustomerClientTest {

    private static ClientAndServer mockServer;
    @Value("${brewery.apiHost}")
    private String apiHost;
    @Value("${brewery.beerUrl}")
    private String customerApi;

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CustomerClient customerClient;

    @BeforeAll
    public static void prepare() {
        mockServer = new ClientAndServer("127.0.0.1", 9001, 9000);
    }

    @BeforeEach
    void setUp() {
        mockServer.reset();
        await().timeout(10000, TimeUnit.MILLISECONDS).until(() -> mockServer.hasStarted());
    }

    @AfterAll
    static void tearAll() {
        mockServer.stop(false);
        await().timeout(10000, TimeUnit.MILLISECONDS).until(() -> !mockServer.isRunning());
    }

    @Test
    void whenGetRequestIsCorrectThenCustomerInstanceIsReturned() throws IOException {
        //given
        UUID id = UUID.randomUUID();
        CustomerDto expectedClient = CustomerDto.builder().id(id).name("Marco Polo").build();
        mockServer.when(HttpRequest.request().withMethod("GET").withPath("/" + customerApi + "/" + id.toString()))
                .respond(HttpResponse.response().withStatusCode(200).withBody(objectMapper.writeValueAsString(expectedClient), MediaType.APPLICATION_JSON));

        //when
        CustomerDto response = customerClient.getCustomer(id);

        //then
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(expectedClient);
    }

    @Test
    void whenOnGetRequestCustomerDoesNotExistThenExceptionIsThrown() throws IOException {
        //given
        UUID id = UUID.randomUUID();
        mockServer.when(HttpRequest.request().withMethod("GET").withPath("/" + customerApi + "/" + id.toString()))
                .respond(HttpResponse.response().withStatusCode(404));

        //when
        //then
        assertThatThrownBy(() -> customerClient.getCustomer(id), String.format("Customer %s does not exist", id));
    }


    @Test
    void whenPostRequestIsSuccessfulThenCustomerInstanceIsReturned() throws IOException {
        //given
        UUID id = UUID.randomUUID();
        CustomerDto client = CustomerDto.builder().name("Marco Polo").build();
        CustomerDto expectedClient = CustomerDto.builder().id(id).name("Marco Polo").build();
        mockServer.when(HttpRequest.request().withMethod("POST").withPath("/" + customerApi).withBody(objectMapper.writeValueAsString(client)))
                .respond(HttpResponse.response().withStatusCode(201).withBody(objectMapper.writeValueAsString(expectedClient), MediaType.APPLICATION_JSON));

        //when
        CustomerDto response = customerClient.create(client);

        //then
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(expectedClient);
    }

    @Test
    void whenPostRequestFailedThenExceptionIsThrown() throws IOException {
        //given
        UUID id = UUID.randomUUID();
        CustomerDto client = CustomerDto.builder().name("Marco Polo").build();
        mockServer.when(HttpRequest.request().withMethod("POST").withPath("/" + customerApi + "/" + id.toString()).withBody(objectMapper.writeValueAsString(client)))
                .respond(HttpResponse.response().withStatusCode(400));

        //when
        //then
        assertThatThrownBy(() -> customerClient.create(client), "No response from service");
    }

    @Test
    void whenPutRequestIsSuccessfulThenCustomerInstanceIsUpdated() throws IOException {
        //given
        UUID id = UUID.randomUUID();
        CustomerDto client = CustomerDto.builder().name("Marco Polo").build();
        CustomerDto expectedClient = CustomerDto.builder().id(id).name("Marco Polo").build();
        mockServer.when(HttpRequest.request().withMethod("PUT").withPath("/" + customerApi + "/" + id.toString()).withBody(objectMapper.writeValueAsString(client)))
                .respond(HttpResponse.response().withStatusCode(200).withBody(objectMapper.writeValueAsString(expectedClient), MediaType.APPLICATION_JSON));

        //when
        CustomerDto response = customerClient.update(id, client);

        //then
        assertThat(response).isNotNull();
        assertThat(response).isEqualTo(expectedClient);
    }

    @Test
    void whenPutRequestFailedThenExceptionIsThrown() throws IOException {
        //given
        UUID id = UUID.randomUUID();
        CustomerDto client = CustomerDto.builder().name("Marco Polo").build();
        mockServer.when(HttpRequest.request().withMethod("PUT").withPath("/" + customerApi + "/" + id.toString()).withBody(objectMapper.writeValueAsString(client)))
                .respond(HttpResponse.response().withStatusCode(400));

        //when
        //then
        assertThatThrownBy(() -> customerClient.update(id, client), String.format("Customer %s could not be updated", id.toString()));
    }

    @Test
    void whenDeleteRequestIsSuccessfulThenCustomerIsremoved() throws IOException {
        //given
        UUID id = UUID.randomUUID();
        mockServer.when(HttpRequest.request().withMethod("DELETE").withPath("/" + customerApi + "/" + id.toString()))
                .respond(HttpResponse.response().withStatusCode(204));

        //when
        //then
        customerClient.delete(id);
    }

    @Test
    void whenDeleteRequestFailedThenExceptionIsThrown() throws IOException {
        //given
        UUID id = UUID.randomUUID();
        mockServer.when(HttpRequest.request().withMethod("DELETE").withPath("/" + customerApi + "/" + id.toString()))
                .respond(HttpResponse.response().withStatusCode(400));

        //when
        //then
        assertThatThrownBy(() -> customerClient.delete(id), String.format("Customer %s could not be deleted", id));
    }
}