package com.customer.client;

import com.beer.commons.model.dto.CustomerDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.UUID;

@Slf4j
@Component
public class CustomerClient {
    @Value("${brewery.apiHost}")
    private String apiHost;
    @Value("${brewery.beerUrl}")
    private String customerApi;

    private final ObjectMapper objectMapper;
    private final OkHttpClient httpClient;

    @Autowired
    public CustomerClient(ObjectMapper objectMapper, OkHttpClient httpClient) {
        this.objectMapper = objectMapper;
        this.httpClient = httpClient;
    }

    public CustomerDto getCustomer(UUID id) throws IOException {
        log.info("Retrieve Customer {}", id);
        Objects.requireNonNull(id);

        Request request = new Request.Builder().get().url(getUrl(id.toString())).build();
        try (Response response = httpClient.newCall(request).execute()) {
            if (response.code() == 200) {
                ResponseBody body = response.body();
                Objects.requireNonNull(body, "Response Body is required");
                return objectMapper.readValue(body.string(), CustomerDto.class);
            } else {
                throw new RuntimeException(String.format("Customer %s does not exist", id));
            }
        }
    }

    public CustomerDto create(CustomerDto customer) throws IOException {
        log.info("Creating a customer");
        Objects.requireNonNull(customer, "Customer instance is required");
        RequestBody requestBody = RequestBody.create(objectMapper.writeValueAsString(customer), MediaType.parse("application/json"));
        Request request = new Request.Builder().post(requestBody).url(getRootUrl()).build();
        try (Response response = httpClient.newCall(request).execute()) {
            if (response.code() == 201) {
                ResponseBody body = response.body();
                Objects.requireNonNull(body, "No response from service");
                return objectMapper.readValue(body.string(), CustomerDto.class);
            } else {
                throw new RuntimeException("Customer could not be created");
            }
        }
    }

    public CustomerDto update(UUID id, CustomerDto customerDto) throws IOException {
        log.info("Updating customer {}", id);
        RequestBody requestBody = RequestBody.create(objectMapper.writeValueAsBytes(customerDto), MediaType.parse("application/json"));
        Request request = new Request.Builder().put(requestBody).url(getUrl(id.toString())).build();
        try (Response response = httpClient.newCall(request).execute()) {
            if (response.code() == 200) {
                ResponseBody body = response.body();
                Objects.requireNonNull(body, "Response body is required");
                return objectMapper.readValue(body.string(), CustomerDto.class);
            } else {
                throw new RuntimeException(String.format("Customer %s could not be updated", id.toString()));
            }
        }
    }

    public void delete(UUID id) throws IOException {
        log.info("Deleting customer {}", id);
        Request request = new Request.Builder().delete().url(getUrl(id.toString())).build();
        try (Response response = httpClient.newCall(request).execute()) {
            if (response.code() != 204) {
                throw new RuntimeException(String.format("Customer %s could not be deleted", id));
            }
        }
    }

    private String getRootUrl() {
        StringJoiner joiner = new StringJoiner("/");
        joiner.add(apiHost).add(customerApi);

        return joiner.toString();
    }

    private String getUrl(String... components) {
        StringJoiner joiner = new StringJoiner("/");
        joiner.add(getRootUrl());
        for (String component : components) {
            joiner.add(component);
        }

        return joiner.toString();
    }
}
