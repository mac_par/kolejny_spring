package com.brewery.web.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.beer.commons.model.BreweryErrorResponse;
import com.beer.commons.model.dto.CustomerDto;
import com.beer.commons.model.dto.HttpStatusCode;
import com.beer.commons.model.exception.PersistenceException;
import com.beer.commons.model.mappers.CustomerMapper;
import com.brewery.web.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {
    private static final String URL = "/api/v1/customer";
    private static final String LOCATION_URL = "http://127.0.0.1:8080/api/v1/customer/";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CustomerService customerServiceMock;

    @Autowired
    private CustomerMapper customerMapper;

    @Test
    public void whenExistingCustomerIsRequestedThenITsReturned() throws Exception {
        UUID id = UUID.randomUUID();
        final String name = "Marco Polo";
        CustomerDto resultCustomer = CustomerDto.builder().id(id).name(name).build();
        final String expectedCustomer = objectMapper.writeValueAsString(resultCustomer);

        when(customerServiceMock.getById(eq(id))).thenReturn(resultCustomer);

        mockMvc.perform(get(URL + "/" + id.toString()))
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string(expectedCustomer));

        verify(customerServiceMock, atMostOnce()).getById(id);
    }

    @Test
    public void whenCustomerDoesNotExistThenExceptionIsThrown() throws Exception {
        UUID id = UUID.randomUUID();
        final String name = "Marco Polo";
        PersistenceException errorResponse = new PersistenceException("Exception has happened");
        String errorMessage = objectMapper.writeValueAsString(new BreweryErrorResponse(HttpStatusCode.NOT_FOUND.getCode(), errorResponse.getMessage()));
        when(customerServiceMock.getById(eq(id))).thenThrow(errorResponse);

        mockMvc.perform(get(URL + "/" + id.toString()))
                .andExpect(status().isNotFound())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string(errorMessage));

        verify(customerServiceMock, atMostOnce()).getById(id);
    }

    @Test
    public void postCreatesCustomer() throws Exception {
        UUID id = UUID.randomUUID();
        final String name = "Marco Polo";
        CustomerDto resultCustomer = CustomerDto.builder().id(id).name(name).build();
        final String expectedCustomer = objectMapper.writeValueAsString(resultCustomer);
        String requestContent = objectMapper.writeValueAsString(CustomerDto.builder().name(name).build());

        when(customerServiceMock.save(any())).thenAnswer(answer -> {
            CustomerDto customer = answer.getArgument(0);
            customer.setId(id);

            return customer;
        });

        mockMvc.perform(post(URL).contentType(MediaType.APPLICATION_JSON).content(requestContent))
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, LOCATION_URL + id.toString()))
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string(expectedCustomer));

        verify(customerServiceMock, atMostOnce()).update(id, resultCustomer);
    }

    @Test
    public void whenNameIsBlankThenErrorIsReturned() throws Exception {
        UUID id = UUID.randomUUID();
        final String name = "   ";
        CustomerDto resultCustomer = CustomerDto.builder().id(id).name(name).build();
        final String expectedResponse = "{\"code\":400,\"message\":\"Method Argument Violation\",\"errors\":[\"must not be blank\"]}";
        String requestContent = objectMapper.writeValueAsString(CustomerDto.builder().name(name).build());

        when(customerServiceMock.save(any())).thenAnswer(answer -> {
            CustomerDto customer = answer.getArgument(0);
            customer.setId(id);

            return customer;
        });

        mockMvc.perform(post(URL).contentType(MediaType.APPLICATION_JSON).content(requestContent))
                .andExpect(status().isBadRequest())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string(expectedResponse));

        verify(customerServiceMock, atMostOnce()).update(id, resultCustomer);
    }

    @Test
    public void whenNameIsShorterThan3CharactersThenErrorIsReturned() throws Exception {
        UUID id = UUID.randomUUID();
        final String name = "bu";
        CustomerDto resultCustomer = CustomerDto.builder().id(id).name(name).build();
        final String expectedResponse = "{\"code\":400,\"message\":\"Method Argument Violation\",\"errors\":[\"size must be between 3 and 100\"]}";
        String requestContent = objectMapper.writeValueAsString(CustomerDto.builder().name(name).build());

        when(customerServiceMock.save(any())).thenAnswer(answer -> {
            CustomerDto customer = answer.getArgument(0);
            customer.setId(id);

            return customer;
        });

        mockMvc.perform(post(URL).contentType(MediaType.APPLICATION_JSON).content(requestContent))
                .andExpect(status().isBadRequest())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string(expectedResponse));

        verify(customerServiceMock, atMostOnce()).update(id, resultCustomer);
    }

    @Test
    public void postFailsThenExceptionIsThrown() throws Exception {
        UUID id = UUID.randomUUID();
        final String name = "Marco Polo";
        PersistenceException errorResponse = new PersistenceException("Exception has happened");
        String errorMessage = objectMapper.writeValueAsString(new BreweryErrorResponse(HttpStatusCode.BAD_REQUEST.getCode(), errorResponse.getMessage()));
        CustomerDto requestObject = CustomerDto.builder().name(name).build();
        String requestContent = objectMapper.writeValueAsString(requestObject);

        when(customerServiceMock.save(any())).thenThrow(errorResponse);

        mockMvc.perform(post(URL).contentType(MediaType.APPLICATION_JSON).content(requestContent))
                .andExpect(status().isBadRequest())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string(errorMessage));

        verify(customerServiceMock, atMostOnce()).update(id, requestObject);
    }

    @Test
    public void putUpdatesCustomer() throws Exception {
        UUID id = UUID.randomUUID();
        final String name = "Marco Polo";
        CustomerDto resultCustomer = CustomerDto.builder().id(id).name(name).build();
        final String expectedCustomer = objectMapper.writeValueAsString(resultCustomer);
        String requestContent = objectMapper.writeValueAsString(CustomerDto.builder().name(name).build());

        when(customerServiceMock.update(any(), any())).thenAnswer(answer -> {
            CustomerDto customer = answer.getArgument(1);
            customer.setId(answer.getArgument(0));

            return customer;
        });

        mockMvc.perform(put(URL + "/" + id.toString()).contentType(MediaType.APPLICATION_JSON).content(requestContent))
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.LOCATION, LOCATION_URL + id.toString()))
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string(expectedCustomer));

        verify(customerServiceMock, atMostOnce()).update(id, resultCustomer);
    }

    @Test
    public void whenPutFailsThenExceptionIsThrown() throws Exception {
        UUID id = UUID.randomUUID();
        final String name = "Marco Polo";
        CustomerDto resultCustomer = CustomerDto.builder().id(id).name(name).build();
        PersistenceException errorResponse = new PersistenceException("Exception has happened");
        String errorMessage = objectMapper.writeValueAsString(new BreweryErrorResponse(HttpStatusCode.NOT_FOUND.getCode(), errorResponse.getMessage()));
        String requestContent = objectMapper.writeValueAsString(CustomerDto.builder().name(name).build());

        when(customerServiceMock.update(any(), any())).thenThrow(errorResponse);

        mockMvc.perform(put(URL + "/" + id.toString()).contentType(MediaType.APPLICATION_JSON).content(requestContent))
                .andExpect(status().isNotFound())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string(errorMessage));

        verify(customerServiceMock, atMostOnce()).update(id, resultCustomer);
    }

    @Test
    public void deleteRemovesCustomer() throws Exception {
        UUID id = UUID.randomUUID();

        mockMvc.perform(delete(URL + "/" + id.toString()))
                .andExpect(status().isNoContent());

        verify(customerServiceMock, atMostOnce()).deleteById(id);
    }

}