package com.brewery.web.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.beer.commons.model.domain.customer.Customer;
import com.beer.commons.model.exception.PersistenceException;
import com.beer.commons.model.mappers.CustomerMapper;
import com.beer.commons.model.mappers.CustomerMapperBean;
import com.beer.commons.model.mappers.DatesMapperBean;
import com.beer.commons.model.dto.CustomerDto;
import com.brewery.web.repository.CustomerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

class CustomerServiceBeanTest {

    private CustomerService service;
    private CustomerRepository repository;
    private final CustomerMapper customerMapper = new CustomerMapperBean(new DatesMapperBean());
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        this.repository = mock(CustomerRepository.class);
        this.service = new CustomerServiceBean(repository, customerMapper);
    }

    @Test
    void whenCustomerWithGivenIdExistsThenItsInstanceIsReturned() {
        //given
        UUID id = UUID.randomUUID();
        String name = "Tomcio Paluch";
        CustomerDto expectedResponse = CustomerDto.builder().id(id).name(name).build();
        when(repository.findById(eq(id))).thenReturn(Optional.of(customerMapper.convert(expectedResponse)));

        //when
        CustomerDto result = service.getById(id);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(expectedResponse);
        verify(repository, times(1)).findById(id);
    }

    @Test
    void whenGetHasFailedThenExceptionISThrown() {
        //given
        UUID id = UUID.randomUUID();
        when(repository.findById(eq(id))).thenReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> service.getById(id)).hasMessage(String.format("Customer identified by %s does not exist", id)).isInstanceOf(PersistenceException.class);

        verify(repository, times(1)).findById(id);
    }

    @Test
    void whenCustomerIsSavedThenNothingSpecialHappens() {
        //given
        UUID id = UUID.randomUUID();
        String name = "Tomcio Paluch";
        CustomerDto instance = CustomerDto.builder().name(name).build();
        when(repository.save(any(Customer.class))).thenAnswer(answer -> {
            Customer customer = answer.getArgument(0);
            customer.setId(id);
            return customer;
        });

        //when
        CustomerDto result = service.save(instance);

        //then
        assertThat(result).isNotNull();
        assertThat(result).matches(pred -> {
            assertThat(pred.getId()).isEqualTo(id);
            assertThat(pred.getName()).isEqualTo(name);
            return true;
        });
        verify(repository, times(1)).save(customerMapper.convert(result));
    }

    @Test
    void whenSaveHasFailedThenExceptionIsThrown() {
        //given
        UUID id = UUID.randomUUID();
        String name = "Tomcio Paluch";
        CustomerDto instance = CustomerDto.builder().id(id).name(name).build();
        when(repository.save(any(Customer.class))).thenThrow(RuntimeException.class);

        //when
        assertThatThrownBy(() -> service.save(instance)).isInstanceOf(RuntimeException.class);

        //then
        verify(repository, times(1)).save(customerMapper.convert(instance));
    }

    @Test
    void whenUpdateIsSuccessfulThenNoExceptionIsThrown() {
        //given
        UUID id = UUID.randomUUID();
        String name = "Tomcio Paluch";
        CustomerDto instance = CustomerDto.builder().name(name).build();
        when(repository.findById(eq(id))).thenAnswer(answer -> Optional.of(Customer.builder().id(answer.getArgument(0)).build()));
        when(repository.save(any(Customer.class))).thenAnswer(answer -> answer.getArgument(0));

        //when
        CustomerDto result = service.update(id, instance);

        //then
        assertThat(result).isNotNull();
        assertThat(result).matches(pred -> {
            assertThat(pred.getId()).isEqualTo(id);
            assertThat(pred.getName()).isEqualTo(name);
            return true;
        });
        verify(repository, times(1)).findById(id);
        verify(repository, times(1)).save(customerMapper.convert(result));
    }

    @Test
    void whenUpdateHasFailedThenExceptionIsThrown() {
        //given
        UUID id = UUID.randomUUID();
        String name = "Tomcio Paluch";
        CustomerDto instance = CustomerDto.builder().name(name).build();
        when(repository.findById(eq(id))).thenAnswer(answer -> Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> service.update(id, instance)).isInstanceOf(PersistenceException.class).hasMessage(String.format("Customer %s does not exist", id));
        verify(repository, times(1)).findById(id);
        verify(repository, never()).save(any(Customer.class));
    }

    @Test
    void whenCustomerWithGivenIdISDeletedThenItsDeleted() {
        //given
        UUID id = UUID.randomUUID();

        //when
        service.deleteById(id);

        //then
        verify(repository, times(1)).deleteById(id);
    }
}