package com.brewery.web.service;

import com.beer.commons.model.dto.CustomerDto;

import java.util.UUID;

public interface CustomerService {

    CustomerDto getById(UUID id);

    CustomerDto save(CustomerDto customer);

    CustomerDto update(UUID id, CustomerDto customer);

    void deleteById(UUID id);
}
