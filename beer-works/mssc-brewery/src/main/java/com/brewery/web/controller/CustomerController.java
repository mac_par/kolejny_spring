package com.brewery.web.controller;

import com.beer.commons.model.dto.CustomerDto;
import com.beer.commons.model.dto.HttpStatusCode;
import com.beer.commons.model.exception.BreweryException;
import com.beer.commons.model.exception.BreweryExceptionHandler;
import com.beer.commons.model.exception.PersistenceException;
import com.beer.commons.model.mappers.CustomerMapperBean;
import com.beer.commons.model.mappers.DatesMapperBean;
import com.brewery.web.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import javax.validation.Valid;

@Slf4j
@Import({BreweryExceptionHandler.class, CustomerMapperBean.class, DatesMapperBean.class})
@RequestMapping("/api/v1/customer")
@RestController
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping(path = "/{customerID}", method = RequestMethod.GET)
    public ResponseEntity<CustomerDto> getCustomeById(@PathVariable("customerID") UUID customerId) {
        log.info("Retrieving customer by {}", customerId);
        try {
            return ResponseEntity.ok(customerService.getById(customerId));
        } catch (PersistenceException ex) {
            throw new BreweryException(HttpStatusCode.NOT_FOUND, ex.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CustomerDto> postCustomer(@Valid @RequestBody CustomerDto customer) {
        log.info("Processing new customer {}", customer);
        try {
            customer = customerService.save(customer);
        } catch (PersistenceException ex) {
            throw new BreweryException(HttpStatusCode.BAD_REQUEST, ex.getMessage());
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, "http://127.0.0.1:8080/api/v1/customer/" + customer.getId().toString());
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return new ResponseEntity<>(customer, headers, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{customerId}", method = RequestMethod.PUT)
    public ResponseEntity<CustomerDto> updateCustomer(@PathVariable("customerId") UUID id, @Valid @RequestBody CustomerDto customer) {
        log.info("Updating customer {}", id);
        try {
            customer = customerService.update(id, customer);
        } catch (PersistenceException ex) {
            throw new BreweryException(HttpStatusCode.NOT_FOUND, ex.getMessage());
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, "http://127.0.0.1:8080/api/v1/customer/" + customer.getId().toString());
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        return new ResponseEntity<>(customer, headers, HttpStatus.OK);
    }

    @RequestMapping(path = "/{customerId}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCustomer(@PathVariable("customerId") UUID id) {
        log.info("Deleting Customer {}", id);
        customerService.deleteById(id);
    }
}
