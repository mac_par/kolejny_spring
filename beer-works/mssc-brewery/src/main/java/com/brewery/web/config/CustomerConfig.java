package com.brewery.web.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {"com.brewery.web.repository"})
@EntityScan({"com.beer.commons.model.domain"})
@ComponentScan(basePackages = {"com.beer.commons"})
public class CustomerConfig {
}
