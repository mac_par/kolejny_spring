package com.brewery.web.service;

import com.beer.commons.model.exception.PersistenceException;
import com.beer.commons.model.mappers.CustomerMapper;
import com.beer.commons.model.dto.CustomerDto;
import com.brewery.web.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class CustomerServiceBean implements CustomerService {
    private final CustomerRepository repository;
    private final CustomerMapper customerMapper;

    @Autowired
    public CustomerServiceBean(CustomerRepository repository, CustomerMapper customerMapper) {
        this.repository = repository;
        this.customerMapper = customerMapper;
    }

    @Override
    public CustomerDto getById(UUID id) {
        log.info("Retrieving client by id {}", id);
        return repository.findById(id).map(customerMapper::convert)
                .orElseThrow(() -> new PersistenceException(String.format("Customer identified by %s does not exist", id)));
    }

    @Override
    public CustomerDto save(CustomerDto customer) {
        log.info("Saving customer: {}", customer);
        return customerMapper.convert(repository.save(customerMapper.convert(customer)));
    }

    @Override
    public CustomerDto update(UUID id, CustomerDto customer) {
        log.info("Updating customer: {}", customer);
        return repository.findById(id)
                .map(item -> customerMapper.update(item, customer))
                .map(repository::save)
                .map(customerMapper::convert)
                .orElseThrow(() -> new PersistenceException(String.format("Customer %s does not exist", id)));
    }

    @Override
    public void deleteById(UUID id) {
        log.info("Removing Customer: {}", id);
        repository.deleteById(id);
    }
}
