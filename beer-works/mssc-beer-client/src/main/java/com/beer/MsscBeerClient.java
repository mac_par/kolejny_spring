package com.beer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsscBeerClient {
    public static void main(String[] args) {
        SpringApplication.run(MsscBeerClient.class, args);
    }
}
