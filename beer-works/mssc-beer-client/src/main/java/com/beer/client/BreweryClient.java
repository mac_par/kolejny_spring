package com.beer.client;

import com.beer.commons.model.dto.BeerDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.UUID;

@Slf4j
@Component
public class BreweryClient {

    @Value("${brewery.apiHost}")
    private String apiHost;
    @Value("${brewery.beerUrl}")
    private String beerApi;

    private final OkHttpClient httpClient;
    private final ObjectMapper objectMapper;

    @Autowired
    public BreweryClient(OkHttpClient httpClient, ObjectMapper objectMapper) {
        this.httpClient = httpClient;
        this.objectMapper = objectMapper;
    }

    public BeerDto getById(UUID id) throws IOException {
        log.info("Retrieving beer identified by {}", id);
        Request request = new Request.Builder().get().url(getUrl(id.toString())).build();
        try (Response response = httpClient.newCall(request).execute()) {
            if (response.code() == 200) {
                ResponseBody responseBody = response.body();
                Objects.requireNonNull(responseBody, "Response body was not provided");
                return objectMapper.readValue(responseBody.string(), BeerDto.class);
            } else {
                throw new RuntimeException(String.format("Beer %s does not exist", id));
            }
        }
    }

    public URI save(BeerDto beer) throws IOException {
        log.info("Saving Beer2 {}", beer);
        RequestBody requestBody = RequestBody.create(objectMapper.writeValueAsString(beer), MediaType.parse("application/json"));
        Request request = new Request.Builder().url(getRootUrl()).post(requestBody).build();
        try (Response response = httpClient.newCall(request).execute()) {
            if (response.code() == 201) {
                String locationHeader = response.header("Location");
                Objects.requireNonNull(locationHeader, "Location header is required");
                return URI.create(locationHeader);
            } else {
                throw new RuntimeException("Beer could not be updated");
            }
        }
    }

    public BeerDto update(UUID id, BeerDto beerDto) throws IOException {
        Objects.requireNonNull(beerDto);
        log.info("Updating beer {}", id);
        RequestBody requestBody = RequestBody.create(objectMapper.writeValueAsBytes(beerDto), MediaType.parse("application/json"));
        Request request = new Request.Builder().put(requestBody).url(getUrl(id.toString())).build();
        try (Response response = httpClient.newCall(request).execute()) {
            if (response.code() == 200) {
                ResponseBody body = response.body();
                Objects.requireNonNull(body, "Response body is required");
                return objectMapper.readValue(body.string(), BeerDto.class);
            } else {
                throw new RuntimeException(String.format("Beer %s could not be updated", id));
            }
        }
    }

    public boolean delete(UUID id) throws IOException {
        Objects.requireNonNull(id);
        log.info("Removing beer {}", id);
        Request request = new Request.Builder().delete().url(getUrl(id.toString())).build();
        try (Response response = httpClient.newCall(request).execute()) {
            return response.code() == 204;
        }
    }

    private String getUrl(String... components) {
        StringJoiner joiner = new StringJoiner("/");
        joiner.add(getRootUrl());
        for (String component : components) {
            joiner.add(component);
        }

        return joiner.toString();
    }

    private String getRootUrl() {
        StringJoiner joiner = new StringJoiner("/");
        joiner.add(apiHost).add(beerApi);

        return joiner.toString();
    }
}