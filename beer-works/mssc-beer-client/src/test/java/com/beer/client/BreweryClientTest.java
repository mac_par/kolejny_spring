package com.beer.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import com.beer.commons.model.dto.BeerDto;
import com.beer.commons.model.domain.beer.BeerStyle;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;
import org.mockserver.model.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class BreweryClientTest {
    @Value("${brewery.apiHost}")
    private String apiHost;
    @Value("${brewery.beerUrl}")
    private String beerApi;

    private ObjectMapper objectMapper = new ObjectMapper();

    private static ClientAndServer mockServer;

    @Autowired
    private BreweryClient breweryClient;

    @BeforeAll
    static void prepare() {
        mockServer = new ClientAndServer("127.0.0.1", 9001, 9000);
    }

    @AfterEach
    void setUp() {
        mockServer.reset();
        Awaitility.await().timeout(10000, TimeUnit.MILLISECONDS).until(() -> mockServer.hasStarted());
    }

    @AfterAll
    static void tearDown() {
        mockServer.stop(false);
        Awaitility.await().timeout(10000, TimeUnit.MILLISECONDS).until(() -> !mockServer.isRunning());
    }

    @Disabled
    @Test
    void getShouldReturnBeerDto() throws IOException {
        //given
        UUID id = UUID.randomUUID();
        BeerDto expectedBeer = BeerDto.builder().id(id).beerName("Jakies piwko").beerStyle(BeerStyle.PALE_ALE).build();

        //when
        BeerDto beerDto = breweryClient.getById(id);
        mockServer.when(request().withMethod("GET").withPath(beerApi + "/" + id.toString()))
                .respond(response().withStatusCode(200).withBody(objectMapper.writeValueAsString(expectedBeer), MediaType.APPLICATION_JSON));
        //then

        assertThat(beerDto).isNotNull();
        assertThat(beerDto.getId()).isEqualTo(id);
        assertThat(beerDto).isEqualTo(expectedBeer);
    }

    @Test
    void whenBeerIsSentThenIsSaved() throws IOException {
        //given
        String beerName = "Jakies piwo";
        BeerStyle style = BeerStyle.PALE_ALE;
        UUID id = UUID.randomUUID();
        BeerDto beer = BeerDto.builder().beerName(beerName).beerStyle(style).build();
        BeerDto expectedBeer = BeerDto.builder().id(id).beerName(beerName).beerStyle(style).build();
        URI expectedLink = URI.create(apiHost + "/" + beerApi + "/" + id.toString());

        mockServer.when(request().withMethod("POST").withPath("/" + beerApi).withBody(objectMapper.writeValueAsString(beer), StandardCharsets.UTF_8))
                .respond(response().withStatusCode(201).withBody(objectMapper.writeValueAsString(expectedBeer), MediaType.APPLICATION_JSON).withHeader(new Header("Location", expectedLink.toString())));

        //when
        URI uri = breweryClient.save(beer);

        //then
        assertThat(uri).isNotNull();
        assertThat(uri).isEqualTo(expectedLink);
    }

    @Test
    void whenPostRequestFailedThenExceptionIsThrown() throws IOException {
        //given
        String beerName = "Jakies piwo";
        BeerStyle style = BeerStyle.PALE_ALE;
        BeerDto beer = BeerDto.builder().beerName(beerName).beerStyle(style).build();

        mockServer.when(request().withMethod("POST").withPath("/" + beerApi).withBody(objectMapper.writeValueAsString(beer), StandardCharsets.UTF_8))
                .respond(response().withStatusCode(500));

        //when
        //then
        assertThatThrownBy(() -> breweryClient.save(beer), "Beer could not be updated");
    }

    @Test
    void whenBeerUpdateIsValidThenBeerIsUpdated() throws IOException {
        //given
        String beerName = "Jakies piwo";
        BeerStyle style = BeerStyle.PALE_ALE;
        UUID id = UUID.randomUUID();
        BeerDto beer = BeerDto.builder().beerName(beerName).beerStyle(style).build();
        BeerDto expectedBeer = BeerDto.builder().id(id).beerName(beerName).beerStyle(style).build();
        URI expectedLink = URI.create(apiHost + "/" + beerApi + "/" + id.toString());

        mockServer.when(request().withMethod("PUT").withPath("/" + beerApi + "/" + id.toString())
                .withBody(objectMapper.writeValueAsString(beer), StandardCharsets.UTF_8))
                .respond(response().withStatusCode(200).withBody(objectMapper.writeValueAsString(expectedBeer), MediaType.APPLICATION_JSON)
                        .withHeader(new Header("Location", expectedLink.toString())));

        //when
        BeerDto updatedBeer = breweryClient.update(id, beer);

        //then
        assertThat(updatedBeer).isNotNull();
        assertThat(updatedBeer.getId()).isNotNull();
        assertThat(updatedBeer).isEqualTo(expectedBeer);
    }

    @Test
    void whenBeerUpdateIsVIncorrectThenExceptionIsThrown() throws IOException {
        //given
        String beerName = "Jakies piwo";
        BeerStyle style = BeerStyle.PALE_ALE;
        UUID id = UUID.randomUUID();
        BeerDto beer = BeerDto.builder().beerName(beerName).beerStyle(style).build();

        mockServer.when(request().withMethod("PUT").withPath("/" + beerApi + "/" + id.toString())
                .withBody(objectMapper.writeValueAsString(beer), StandardCharsets.UTF_8))
                .respond(response().withStatusCode(500));

        //when
        //then
        assertThatThrownBy(() -> breweryClient.update(id, beer), String.format("Beer %s could not be updated", id));
    }

    @Test
    void whenDeleteRequestWasPerformedThenMethodReturnsTrue() throws IOException {
        //given
        UUID id = UUID.randomUUID();

        mockServer.when(request().withMethod("DELETE").withPath("/" + beerApi + "/" + id.toString()))
                .respond(response().withStatusCode(204));

        //when
        boolean result = breweryClient.delete(id);

        //then
        assertThat(result).isTrue();
    }

    @Test
    void whenDeleteRequestWasIncorrectThenReturnFalse() throws IOException {
        //given
        UUID id = UUID.randomUUID();

        mockServer.when(request().withMethod("DELETE").withPath("/" + beerApi + "/" + id.toString()))
                .respond(response().withStatusCode(500));

        //when
        boolean result = breweryClient.delete(id);

        //then
        assertThat(result).isFalse();
    }
}