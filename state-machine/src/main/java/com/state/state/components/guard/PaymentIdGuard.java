package com.state.state.components.guard;

import static com.state.services.PaymentServiceBean.PAYMENT_ID_HEADER;

import com.state.domain.PaymentEvent;
import com.state.domain.PaymentState;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;
import org.springframework.stereotype.Component;

@Component
public class PaymentIdGuard implements Guard<PaymentState, PaymentEvent> {
    @Override
    public boolean evaluate(StateContext<PaymentState, PaymentEvent> stateContext) {
        return stateContext.getMessageHeader(PAYMENT_ID_HEADER) != null;
    }
}
