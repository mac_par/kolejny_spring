package com.state.services;

import com.state.domain.Payment;
import com.state.domain.PaymentEvent;
import com.state.domain.PaymentState;
import org.springframework.statemachine.StateMachine;

public interface PaymentService {
    Payment newPayment(Payment payment);

    StateMachine<PaymentState, PaymentEvent> preAuth(Long payment);

    StateMachine<PaymentState, PaymentEvent> authorizePayment(Long payment);

    StateMachine<PaymentState, PaymentEvent> declineAuthorization(Long payment);
}
