package com.state.services;

import static org.junit.Assert.assertEquals;

import com.state.domain.Payment;
import com.state.domain.PaymentEvent;
import com.state.domain.PaymentState;
import com.state.repository.PaymentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;

import java.math.BigDecimal;

@SpringBootTest
class PaymentServiceBeanTest {
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private PaymentRepository repository;
    private Payment payment;

    @BeforeEach
    void setUp() {
        payment = Payment.builder().amount(new BigDecimal("12.99")).build();
    }

    @Test
    void preAuth() {
        Payment savedPayment = paymentService.newPayment(payment);
        StateMachine<PaymentState, PaymentEvent> statePaymentEventStateMachine = paymentService.preAuth(savedPayment.getId());
        Payment preAuthPayment = repository.getOne(savedPayment.getId());

        assertEquals(PaymentState.NEW, statePaymentEventStateMachine.getState().getId());
    }

    @RepeatedTest(10)
    void auth() {
        Payment savedPayment = paymentService.newPayment(payment);
        StateMachine<PaymentState, PaymentEvent> state = paymentService.preAuth(savedPayment.getId());
        if (state.getState().getId() == PaymentState.PRE_AUTH) {
            System.out.println("Payment is PreAuthorized");
            StateMachine<PaymentState, PaymentEvent> authSM = paymentService.authorizePayment(savedPayment.getId());
            System.out.println("Auth Result: " + authSM.getState().getId());
        } else {
            System.out.println("Payment failed pre-authorization");
        }
    }
}