package com.udemy.boot.repository;

import com.udemy.boot.model.StatusUpdate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusUpdateRepository extends JpaRepository<StatusUpdate, Long> {
    @Query(value = "select * from (select * from status_update u order by date_modified desc, date_added desc) where rownum < 2", nativeQuery = true)
    StatusUpdate findRecent();

    @Query(value = "select count(*) from status_update where trim(text_content) = trim(?1)", nativeQuery = true)
    int exists(String string);
}
