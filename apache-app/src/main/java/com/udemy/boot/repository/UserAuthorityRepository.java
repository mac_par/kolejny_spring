package com.udemy.boot.repository;

import com.udemy.boot.model.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAuthorityRepository extends JpaRepository<UserAuthority, Long> {
    @Query("select a from UserAuthority a where a.authority = :name")
    UserAuthority getAuthorityByName(String name);

    @Query("select a from UserAuthority a where a.authority in (:names)")
    List<UserAuthority> getAuthorityByNames(List<String> names);
}
