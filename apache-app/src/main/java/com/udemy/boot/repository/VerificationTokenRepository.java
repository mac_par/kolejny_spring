package com.udemy.boot.repository;

import com.udemy.boot.model.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
    Optional<VerificationToken> findByToken(String token);

    @Query(value = "delete from verification_tokens t where t.user_id = ?1", nativeQuery = true)
    void removeForUser(long userId);
}
