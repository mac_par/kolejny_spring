package com.udemy.boot.repository;

import com.udemy.boot.model.Profile;
import com.udemy.boot.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
    Optional<Profile> findByUser(User user);
    @Query(value = "select * from profiles_data p where p.user_id = ?1", nativeQuery = true)
    Optional<Profile> findByUserId(Long userId);
}
