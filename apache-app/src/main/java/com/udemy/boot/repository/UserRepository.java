package com.udemy.boot.repository;

import com.udemy.boot.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query(value = "select u from User u left join fetch u.authorities where u.username = ?1")
    Optional<User> findUserByUsername(String username);
}
