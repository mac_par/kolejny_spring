package com.udemy.boot.service;

import com.udemy.boot.model.StatusUpdate;
import com.udemy.boot.repository.StatusUpdateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional(readOnly = true)
public class UpdateStatusServiceBean implements com.udemy.boot.service.StatusUpdateService {
    private static final int PAGE_SIZE = 5;
    private final StatusUpdateRepository repository;

    @Autowired
    public UpdateStatusServiceBean(StatusUpdateRepository repository) {
        this.repository = repository;
    }

    @Transactional
    @Override
    public StatusUpdate save(StatusUpdate entity) {
        log.info("Saving {}", entity);
        return repository.save(entity);
    }

    @Override
    public StatusUpdate getRecent() {
        log.info("Retrieving recent");
        return repository.findRecent();
    }

    @Override
    public boolean checkContentUniquess(String value) {
        log.info("Checking '{}' for uniqueness on database", value);

        return repository.exists(value) == 0;
    }

    @Override
    public Page<StatusUpdate> page(int pageNbr) {
        Sort sortingRule = Sort.by("added").descending();

        PageRequest pageRequest = PageRequest.of(convertToPage(pageNbr), PAGE_SIZE, sortingRule);
        return repository.findAll(pageRequest);
    }

    private int convertToPage(int page) {
        return page > 0 ? page -1 : 0;
    }

    @Override
    public boolean contains(Long id) {
        if (id == null) {
            return false;
        }

        return repository.findById(id).isPresent();
    }

    @Override
    public void delete(long id) {
        log.info("Deleting item {}",id);
        repository.deleteById(id);
    }

    @Override
    public StatusUpdate get(Long id) {
        log.info("Updating UpdateStatus identified by {}", id);
        return repository.findById(id).orElse(null);
    }

    @Override
    public void update(StatusUpdate entity) {
        log.info("Updating UpdateStatus {}", entity.getId());
        repository.save(entity);
    }
}
