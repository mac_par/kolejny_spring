package com.udemy.boot.service;

import com.udemy.boot.model.TokenType;
import com.udemy.boot.model.User;

public interface TokenGenerator {
    String generateToken(User user, TokenType tokenType);
}
