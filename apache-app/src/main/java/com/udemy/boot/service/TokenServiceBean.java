package com.udemy.boot.service;

import com.udemy.boot.model.TokenType;
import com.udemy.boot.model.User;
import com.udemy.boot.model.VerificationToken;
import com.udemy.boot.repository.VerificationTokenRepository;
import com.udemy.boot.service.handling.SiteException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TokenServiceBean implements TokenService {
    private final VerificationTokenRepository tokenRepository;
    private final TokenGenerator tokenGenerator;

    @Autowired
    public TokenServiceBean(VerificationTokenRepository tokenRepository, TokenGenerator tokenGenerator) {
        this.tokenRepository = tokenRepository;
        this.tokenGenerator = tokenGenerator;
    }

    @Override
    public String generateRegistrationToken(User user) {
        return generateToken(user, TokenType.REGISTRATION);
    }

    @Override
    public String generatePasswordResetToken(User user) {
        return generateToken(user, TokenType.PASSWORD_RESET);
    }

    @Override
    public VerificationToken findByToken(String token) {
        log.info("Retrieving Verification token by token {}", token);
        return tokenRepository.findByToken(token).orElseThrow(() -> new SiteException("Provided token was not identified."));
    }

    @Override
    public void remove(long tokenId) {
        log.info("Removing token identified by id {}", tokenId);
        tokenRepository.deleteById(tokenId);
    }

    @Override
    public void removeByUser(long userId) {
        log.info("Removing all tokens assign to user identified by id: {}", userId);
        tokenRepository.removeForUser(userId);
    }

    private String generateToken(User user, TokenType tokenType) {
        log.info("Generating {} token for user {}", tokenType, user.getUsername());
        final String tokenString = tokenGenerator.generateToken(user, tokenType);
        VerificationToken token = new VerificationToken(tokenString, user, tokenType);

        tokenRepository.save(token);
        log.info("Token '{}' successfully generated", tokenString);
        return tokenString;
    }
}
