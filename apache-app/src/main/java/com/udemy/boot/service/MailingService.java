package com.udemy.boot.service;

import com.udemy.boot.model.User;

public interface MailingService {
    void sendConfirmationMail(User user, String token);
    void sendResetPasswordMail(User user, String token);
}
