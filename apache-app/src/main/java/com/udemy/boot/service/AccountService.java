package com.udemy.boot.service;

import com.udemy.boot.model.User;
import com.udemy.boot.model.UserAuthority;
import com.udemy.boot.model.UserDao;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface AccountService extends UserDetailsService {
    User save(User user);

    void persistNewPassword(User user);

    User findUserById(Long id);

    User getUserByUsername(String email);

    UserAuthority getAuthorityByName(String name);

    List<UserAuthority> getAuthorityByNames(List<String> names);

    List<UserAuthority> getAllAuthorities();

    User createUser(UserDao dao);

    User convertToUser(UserDao dao);

    Page<User> page(int pageNbr);

    Boolean hasAdminRole(String username);

    void remove(long id);

    void encodePassword(User user);

    boolean userExists(String username);
}
