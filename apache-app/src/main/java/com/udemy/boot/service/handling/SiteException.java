package com.udemy.boot.service.handling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.StringJoiner;

@ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
public class SiteException extends RuntimeException {
    public SiteException(String message) {
        super(message);
    }

    public SiteException(String message, Throwable cause) {
        super(message, cause);
    }

    public String getDetails() {
        StringJoiner joiner = new StringJoiner("\n");
        if (this.getStackTrace().length > 0) {
            for (StackTraceElement element : this.getStackTrace()) {
                joiner.add(element.toString());
            }
        }

        return joiner.toString();
    }
}
