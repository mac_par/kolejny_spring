package com.udemy.boot.service;

import com.udemy.boot.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.InternetAddress;
import java.util.Date;

@Service
@Slf4j
public class MailingServiceBean implements MailingService {

    private final TemplateEngine templateEngine;
    private final JavaMailSender mailSender;

    @Value("${mail.replyEmail}")
    private String replyEmail;
    @Value("${mail.enable}")
    private boolean enable;
    @Value("${mail.verify.title}")
    private String verifyEmailTitle;
    @Value("${mail.verify.template}")
    private String verifyEmailTemplate;
    @Value("${mail.reset.title}")
    private String passwordResetTitle;
    @Value("${mail.reset.template}")
    private String passwordResetTemplate;
    @Value("${site.root.url}")
    private String siteRootURL;

    @Autowired
    public MailingServiceBean(JavaMailSender mailSender, TemplateEngine templateEngine) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    @Async
    @Override
    public void sendConfirmationMail(User user, String token) {
        final String emailAddress = user.getUsername();
        log.info("Sending e-mail confirmation to {}", emailAddress);

        Context context = getTemplateContext(user, token, "Email Verification");

        MimeMessagePreparator mimeMessagePreparator = generateMimeMessagePreparator(verifyEmailTemplate, verifyEmailTitle,
                emailAddress, context);
        sendEmail(mimeMessagePreparator);
        log.info("Email was sent to {}", emailAddress);
    }

    @Async
    @Override
    public void sendResetPasswordMail(User user, String token) {
        final String emailAddress = user.getUsername();
        log.info("Sending password reset e-mail to {}", emailAddress);

        Context context = getTemplateContext(user, token, "Account Password Reset");

        MimeMessagePreparator mimeMessagePreparator = generateMimeMessagePreparator(passwordResetTemplate, passwordResetTitle,
                emailAddress, context);
        sendEmail(mimeMessagePreparator);
        log.info("Email was sent to {}", emailAddress);
    }

    private void sendEmail(MimeMessagePreparator preparator) {
        if (enable) {
            mailSender.send(preparator);
        }
    }

    private Context getTemplateContext(User user, String token, String title) {
        Context context = new Context();
        context.setVariable("page_title", title);
        context.setVariable("name", user.getUsername());
        context.setVariable("token", token);
        context.setVariable("rootUrl", siteRootURL);
        return context;
    }

    private MimeMessagePreparator generateMimeMessagePreparator(String template, String emailTitle, String emailAddress, Context context) {
        return mimeMessage -> {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
            mimeMessageHelper.setFrom(new InternetAddress(replyEmail));
            mimeMessageHelper.setTo(emailAddress);
            mimeMessageHelper.setSubject(emailTitle);
            mimeMessageHelper.setSentDate(new Date());
            final String content = templateEngine.process(template, context);
            mimeMessageHelper.setText(content, true);
        };
    }
}
