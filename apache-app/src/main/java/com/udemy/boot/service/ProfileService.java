package com.udemy.boot.service;

import com.udemy.boot.model.Profile;
import com.udemy.boot.model.User;

public interface ProfileService {
    Profile getProfileByUser(User user);

    Profile save(Profile profile);

    Profile create(User user, String description);

    void removeByUserId(Long userId);

    Profile getById(Long id);
}
