package com.udemy.boot.service.validation;

import com.udemy.boot.model.UserDao;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class PasswordsMatchValidation implements ConstraintValidator<PasswordsMatch, UserDao> {
    @Override
    public void initialize(PasswordsMatch constraintAnnotation) {

    }

    @Override
    public boolean isValid(UserDao dao, ConstraintValidatorContext context) {
        if (Objects.isNull(dao.getRepeatPassword())) {
            return false;
        }


        return !Objects.isNull(dao.getPassword()) && dao.getPassword().equals(dao.getRepeatPassword());
    }
}
