package com.udemy.boot.service;

import com.udemy.boot.model.Profile;
import com.udemy.boot.model.User;
import com.udemy.boot.repository.ProfileRepository;
import com.udemy.boot.service.handling.SiteException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
public class ProfileServiceBean implements ProfileService {
    private static final String INITIAL_DESCRIPTION = "";
    private final ProfileRepository repository;

    @Autowired
    public ProfileServiceBean(ProfileRepository repository) {
        this.repository = repository;
    }

    @Override
    public Profile getProfileByUser(User user) {
        log.info("Retrieve Profile by user {}", user);

        return repository.findByUser(user).orElseGet(()->{
            log.info("Profile for user {} does not exist. Creating initial profile.", user.getUsername());
            return create(user, null);
        });
    }

    @Override
    public Profile save(Profile profile) {
        Objects.requireNonNull(profile);
        log.info("Saving profile {}", profile);
        return repository.save(profile);
    }

    @Override
    public Profile create(User user, String description) {
        Objects.requireNonNull(user, "User instance is required");
        log.info("Creating Profile for user {}", user.getUsername());
        Profile profile = repository.findByUser(user).orElseGet(() -> new Profile(user, description));
        profile.setAbout(description);
        return repository.save(profile);
    }

    @Override
    public void removeByUserId(Long userId) {
        log.info("Removing profile for user Id {}", userId);
        repository.findByUserId(userId).ifPresent(repository::delete);
    }

    @Override
    public Profile getById(Long id) {
        log.info("Retrieve Profile by id {}", id);

        return repository.findById(id).orElseThrow(()-> {
            log.error("Profile for {} does not exist", id);
            throw new SiteException("Provided profile does not exist");
        });
    }
}
