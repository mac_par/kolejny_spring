package com.udemy.boot.service;

import com.udemy.boot.model.User;
import com.udemy.boot.model.UserAuthority;
import com.udemy.boot.model.UserDao;
import com.udemy.boot.repository.UserAuthorityRepository;
import com.udemy.boot.repository.UserRepository;
import com.udemy.boot.service.handling.SiteException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AccountServiceBean implements AccountService {
    private static final String USER_ACCOUNT = "user";
    private static final String ADMIN_ACCOUNT = "admin";
    private static final int PAGE_SIZE = 5;

    private final UserRepository userRepository;
    private final UserAuthorityRepository authorityRepository;
    private final PasswordEncoder passwordEncoder;


    @Autowired
    public AccountServiceBean(UserRepository userRepository, UserAuthorityRepository authorityRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Retrieving user by login [{}]", username);
        return userRepository.findUserByUsername(username).orElseThrow(() -> {
            String msg = String.format("User '%s' does not exist", username);
            log.error(msg);
            return new UsernameNotFoundException(msg);
        });
    }

    @Override
    public List<UserAuthority> getAuthorityByNames(List<String> names) {
        log.info("Retrieving authorities by names: [{}]", names);
        return authorityRepository.getAuthorityByNames(names);
    }

    @Override
    public User save(User user) {
        log.info("Saving/Updating user {}", user.getUsername());
        return userRepository.save(user);
    }

    @Override
    public UserAuthority getAuthorityByName(String name) {
        log.info("Retrieving authority by name: {}", name);
        return authorityRepository.getAuthorityByName(name);
    }

    @Override
    public List<UserAuthority> getAllAuthorities() {
        log.info("Retrieving all User Authorities");
        return authorityRepository.findAll();
    }

    @Override
    public User createUser(UserDao dao) {
        log.info("An attempt to create user account instance");
        if (Objects.isNull(dao)) {
            log.error("No data was provided to create user instance");
            throw new SiteException("Initial data is required to create an user account instance.");
        }

        User user = new User(dao.getUsername(), passwordEncoder.encode(dao.getPassword()));
        List<String> userAuthorities = new ArrayList<>(List.of(USER_ACCOUNT));

        if (dao.isAdmin()) {
            userAuthorities.add(ADMIN_ACCOUNT);
        }

        List<UserAuthority> authorities = getAuthorityByNames(userAuthorities);
        log.info("User authorities: {}", authorities);
        user.addAuthorities(authorities);
        user.setEnabled(false);

        return user;
    }

    @Override
    public Page<User> page(int pageNbr) {
        Sort sortingRule = Sort.by("lastModified").descending();

        PageRequest pageRequest = PageRequest.of(convertToPage(pageNbr), PAGE_SIZE, sortingRule);
        return userRepository.findAll(pageRequest);
    }

    private int convertToPage(int page) {
        return page > 0 ? page - 1 : 0;
    }

    @Override
    public Boolean hasAdminRole(String username) {
        log.info("Checking whether user '{}' has admin role", username);
        return this.userRepository.findUserByUsername(username)
                .map(user -> user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .filter(collection -> collection.contains(ADMIN_ACCOUNT)).isPresent();
    }

    @Override
    public void remove(long id) {
        log.info("Removing account identified by id {}", id);
        userRepository.deleteById(id);
    }

    @Override
    public User findUserById(Long id) {
        log.info("Looking for user identified by id {}", id);
        return userRepository.findById(id)
                .orElseThrow(() -> new SiteException(String.format("Account identified by id {} does not exist", id)));
    }

    @Override
    public void encodePassword(User user) {
        Objects.requireNonNull(user);
        log.info("Encoding user's password");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.modifyCredentials();
    }

    @Override
    public User getUserByUsername(String name) {
        log.info("Retrieving user by username {}", name);
        return userRepository.findUserByUsername(name)
                .orElseThrow(() -> new SiteException(String.format("User %s does not exist", name)));
    }

    @Override
    public User convertToUser(UserDao dao) {
        Objects.requireNonNull(dao);
        log.info("convertToUser: username: {}, authorities: {}, enabled: {}, locked: {}", dao.getUsername(), dao.getAuthorities(), dao.isEnabled(), dao.isLocked());
        User user = getUserByUsername(dao.getUsername());
        user.setEnabled(dao.isEnabled());
        user.setLocked(dao.isLocked());

        if (!user.getPassword().equals(dao.getPassword())) {
            user.setPassword(dao.getPassword());
            encodePassword(user);
        }

        return user;
    }

    @Override
    public boolean userExists(String username) {
        return this.userRepository.findUserByUsername(username).isPresent();
    }

    @Override
    public void persistNewPassword(User user) {
        log.info("Persisting new password for user {}", user.getUsername());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }
}
