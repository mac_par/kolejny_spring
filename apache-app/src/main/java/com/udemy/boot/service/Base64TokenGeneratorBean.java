package com.udemy.boot.service;

import com.udemy.boot.model.TokenType;
import com.udemy.boot.model.User;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Objects;

@Service
public class Base64TokenGeneratorBean implements TokenGenerator {
    @Override
    public String generateToken(User user, TokenType tokenType) {
        if (Objects.isNull(user)) {
            return "";
        }
        final String plainText = String.format("%s:%s:%s", user.getUsername(),user.getLastModified(), tokenType);

        return Base64.getEncoder().encodeToString(plainText.getBytes());
    }
}
