package com.udemy.boot.service;

import com.udemy.boot.model.User;
import com.udemy.boot.model.VerificationToken;

public interface TokenService {
    String generateRegistrationToken(User user);

    VerificationToken findByToken(String token);

    void remove(long tokenId);

    void removeByUser(long userId);

    String generatePasswordResetToken(User user);
}
