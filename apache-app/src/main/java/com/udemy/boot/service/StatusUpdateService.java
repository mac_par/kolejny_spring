package com.udemy.boot.service;

import com.udemy.boot.model.StatusUpdate;
import org.springframework.data.domain.Page;

public interface StatusUpdateService {
    StatusUpdate save(StatusUpdate entity);

    StatusUpdate getRecent();

    boolean checkContentUniquess(String value);

    Page<StatusUpdate> page(int pageNbr);

    boolean contains(Long id);

    void delete(long id);

    StatusUpdate get(Long id);

    void update(StatusUpdate update);
}
