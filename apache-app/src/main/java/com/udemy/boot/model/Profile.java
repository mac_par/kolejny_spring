package com.udemy.boot.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "profiles_data")
public class Profile implements java.io.Serializable {
    private static final long serialVersionUID = 4970385403910767767L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profile_seq")
    @SequenceGenerator(name = "profile_seq", allocationSize = 1, sequenceName = "profiles_data_seq")
    @Column(name = "id")
    private Long id;

    @NotNull
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Size(max = 256, message = "{profile.about.max}")
    @Column(name = "profile_desc")
    private String about;

    public Profile() {
    }

    public Profile(User user, String about) {
        this.user = user;
        this.about = about;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, about);
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.isNull(obj)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Profile)) {
            return false;
        }

        Profile profile = (Profile) obj;

        if (id != null ? !id.equals(profile.id) : profile.id != null) {
            return false;
        }

        if (user != null ? !user.equals(profile.user) : profile.user != null) {
            return false;
        }

        return Objects.equals(about, profile.about);
    }
}
