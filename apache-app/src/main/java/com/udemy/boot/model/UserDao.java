package com.udemy.boot.model;

import com.udemy.boot.service.validation.PasswordsMatch;
import com.udemy.boot.service.validation.ValidateMatchingPassword;
import com.udemy.boot.service.validation.ValidatePassword;
import org.springframework.security.core.GrantedAuthority;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@PasswordsMatch(groups = {ValidateMatchingPassword.class})
public class UserDao implements java.io.Serializable {
    private static final long serialVersionUID = -7314758586544839092L;
    @NotNull
    @Size(min = 4, max = 21, message = "{email.size.invalid}")
    @Email(message = "{email.message.invalid}")
    private String username;
    @NotNull
    @Size(min = 8, max = 20, message = "{password.size.invalid}", groups = ValidatePassword.class)
    private String password;
    private String repeatPassword;
    private boolean admin;
    private final List<String> authorities;
    private boolean enabled;
    private boolean locked;

    public UserDao() {
        this.authorities = new LinkedList<>();
    }

    public UserDao(User user) {
        Objects.requireNonNull(user);
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.enabled = user.isEnabled();
        this.locked = user.isLocked();

        this.authorities = user.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public boolean passwordMatchCheck() {
        return !Objects.isNull(password) && Objects.equals(password, repeatPassword);
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}
