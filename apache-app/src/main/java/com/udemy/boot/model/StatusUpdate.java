package com.udemy.boot.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "status_update")
public class StatusUpdate implements java.io.Serializable {
    private static final long serialVersionUID = -6182676495615011596L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "status_text_generator")
    @SequenceGenerator(allocationSize = 1, name = "status_text_generator", sequenceName = "status_update_seq")
    @Column(name = "status_id")
    private Long id;

    @NotNull(message = "{status.text.null}")
    @Size(min = 5, max = 256, message = "{status.text.length}")
    @Column(name = "text_content")
    private String text;

    @Column(name = "date_added", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date added;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_modified")
    private Date dateModified;

    public StatusUpdate() {
    }

    public StatusUpdate(String text) {
        this.text = text;
    }

    public StatusUpdate(String text, Date date) {
        this.text = text;
        this.added = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getAdded() {
        return added;
    }

    public void setAdded(Date added) {
        this.added = added;
    }

    public Date getDateModified() {
        return dateModified;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, added);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (!(obj instanceof StatusUpdate)) {
            return false;
        }

        StatusUpdate su = (StatusUpdate) obj;

        if (id != null ? !id.equals(su.id) : su.id != null) {
            return false;
        }

        if (text != null ? !text.equals(su.text) : su.text != null) {
            return false;
        }

        return added != null ? added.equals(su.added) : su.added == null;
    }

    @PrePersist
    protected void setDate() {
        if (this.added == null) {
            this.added = new Date();
        }
        this.dateModified = added;
    }

    @PreUpdate
    protected void updateModification() {
        this.dateModified = new Date();
    }
}
