package com.udemy.boot.model;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "user_authorities")
public class UserAuthority implements GrantedAuthority {
    private static final long serialVersionUID = 7571933864386425029L;

    @Id
    @Column(name = "auth_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_authority_seq")
    @SequenceGenerator(name = "user_authority_seq", allocationSize = 1, sequenceName = "user_authorities_sequence")
    private Long id;
    @NotNull
    @Column(name = "authority_name",unique = true, length = 60)
    private String authority;
    @Column(name = "date_created")
    private LocalDateTime created;
    @Column(name = "date_modified")
    private LocalDateTime modified;

    public UserAuthority() {
    }

    public UserAuthority(@NotNull String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(authority);
    }

    @PrePersist
    private void populateDate() {
        this.created = LocalDateTime.now();
        this.modified = this.created;
    }

    @PreUpdate
    private void updateModificationTime() {
        this.modified = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (!(obj instanceof UserAuthority)) {
            return false;
        }

        UserAuthority userAuthority = (UserAuthority) obj;

        if (id != null ? !id.equals(userAuthority.id) : userAuthority.id != null) {
            return false;
        }

        return Objects.equals(this.authority, userAuthority.authority);
    }

    @Override
    public String toString() {
        return this.authority;
    }
}
