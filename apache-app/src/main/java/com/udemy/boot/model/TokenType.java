package com.udemy.boot.model;

public enum TokenType {
    REGISTRATION,
    PASSWORD_RESET
}
