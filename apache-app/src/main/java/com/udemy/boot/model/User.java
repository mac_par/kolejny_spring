package com.udemy.boot.model;

import com.udemy.boot.converter.BooleanToStringConverter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.*;

@Entity
@Table(name = "users_data")
public class User implements UserDetails {
    private static final long serialVersionUID = 1998348047230747501L;
    private static final int ACCOUNT_MAX_EXPIRATION_MONTHS = 12;
    private static final int CREDENTIALS_MAX_EXPIRATION_MONTHS = 3;
    private static final int ZERO = 0;
    private static final Period ACCOUNT_VALIDITY_PERIOD = Period.ofMonths(ACCOUNT_MAX_EXPIRATION_MONTHS);
    private static final Period CREDENTIALS_VALIDITY_PERIOD = Period.ofMonths(CREDENTIALS_MAX_EXPIRATION_MONTHS);


    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_data_seq")
    @SequenceGenerator(name = "user_data_seq", allocationSize = 1, sequenceName = "users_data_sequence")
    private Long id;

    @NotBlank(message = "{email.message.invalid}")
    @Size(min = 4, max = 21, message = "{email.size.invalids}")
    @Email(message = "{email.message.invalid}")
    @Column(name = "user_name")
    private String username;

    @NotNull
    @Size(min = 8, max = 255, message = "{password.size.invalid}")
    @Column(name = "user_password")
    private String password;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH})
    @JoinTable(joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "authority_id"),
            name = "user_authority_join")
    private final Set<UserAuthority> authorities;

    @Column(name = "last_modified_date")
    private LocalDateTime lastModified;

    @Column(name = "credentials_modification_date")
    private LocalDateTime credentialsModificationDate;

    @Convert(converter = BooleanToStringConverter.class)
    @Column(name = "account_enabled")
    private boolean enabled;

    @Convert(converter = BooleanToStringConverter.class)
    @Column(name = "account_locked")
    private boolean locked;

    public User() {
        this.authorities = new HashSet<>();
    }

    public User(String username, String password, Set<UserAuthority> authorities) {
        this.username = username;
        this.password = password;
        Objects.requireNonNull(authorities);
        this.authorities = authorities;
        this.enabled = true;
    }

    public User(String username, String password) {
        this();
        this.username = username;
        this.password = password;
        this.enabled = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.unmodifiableSet(authorities);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return beforeDeadline(lastModified, ACCOUNT_VALIDITY_PERIOD);
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return beforeDeadline(credentialsModificationDate, CREDENTIALS_VALIDITY_PERIOD);
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public LocalDateTime getCredentialsModificationDate() {
        return credentialsModificationDate;
    }

    public void setCredentialsModificationDate(LocalDateTime credentialsModificationDate) {
        this.credentialsModificationDate = credentialsModificationDate;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public void addAuthority(UserAuthority authority) {
        this.authorities.add(authority);
    }

    public void addAuthorities(Collection<UserAuthority> authorities) {
        if (authorities != null && !authorities.isEmpty()) {
            this.authorities.clear();
            this.authorities.addAll(authorities);
        }
    }

    public void removeAuthority(UserAuthority authority) {
        this.authorities.remove(authority);
    }

    static boolean beforeDeadline(LocalDateTime time, Period period) {
        LocalDateTime deadline = time.plus(period);
        return LocalDateTime.now().isBefore(deadline);
    }

    @PrePersist
    @PreUpdate
    void updateModificationDate() {
        this.lastModified = LocalDateTime.now().withHour(ZERO).withMinute(ZERO).withSecond(ZERO);
        if (this.credentialsModificationDate == null) {
            modifyCredentials();
        }
    }

    public void modifyCredentials() {
        this.credentialsModificationDate = LocalDateTime.now().withHour(ZERO).withMinute(ZERO).withSecond(ZERO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, authorities);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof User)) {
            return false;
        }

        User user = (User) obj;

        return Objects.equals(id, user.id)
                && Objects.equals(username, user.username)
                && Objects.equals(password, user.password)
                && Objects.equals(authorities, user.authorities);
    }
}
