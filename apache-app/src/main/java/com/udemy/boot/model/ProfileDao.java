package com.udemy.boot.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Objects;

public class ProfileDao implements java.io.Serializable {
    private static final long serialVersionUID = -3402366565448886706L;

    private Long id;
    @NotEmpty
    @Size(max = 256, message = "{profile.about.max}")
    private String description;
    private String username;

    public ProfileDao() {
    }

    public ProfileDao(Long id, String description, String username) {
        this.id = id;
        this.description = description;
        this.username = username;
    }

    public ProfileDao(Profile profile) {
        Objects.requireNonNull(profile);
        this.id = profile.getId();
        this.description = profile.getAbout();
        this.username = profile.getUser().getUsername();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
