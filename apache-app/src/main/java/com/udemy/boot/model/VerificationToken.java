package com.udemy.boot.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "verification_tokens")
public class VerificationToken implements java.io.Serializable {
    private static final long serialVersionUID = 7187160282701484834L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "token_seq")
    @SequenceGenerator(name = "token_seq", allocationSize = 1, sequenceName = "verification_token_sequence")
    @Column(name = "token_id")
    private Long id;

    @Size(min = 4, max = 255)
    @Column(name = "token", nullable = false)
    private String token;

    @NotNull
    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Enumerated(EnumType.STRING)
    @Column(name = "token_type", length = 60)
    private TokenType type;

    @Column(name = "expiration_date")
    private LocalDateTime expiry;

    public VerificationToken() {
    }

    public VerificationToken(String token, User user, TokenType type) {
        this.token = token;
        this.user = user;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TokenType getType() {
        return type;
    }

    public void setType(TokenType type) {
        this.type = type;
    }

    public LocalDateTime getExpiry() {
        return expiry;
    }

    public void setExpiry(LocalDateTime expiry) {
        this.expiry = expiry;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, token, user, type, expiry);
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.isNull(obj)) {
            return false;
        }

        if (!(obj instanceof VerificationToken)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        VerificationToken vt = (VerificationToken) obj;

        if (!Objects.equals(id, vt.id)) {
            return false;
        }

        if (!Objects.equals(token, vt.token)) {
            return false;
        }

        if (!Objects.equals(user, vt.user)) {
            return false;
        }

        if (!Objects.equals(type, vt.type)) {
            return false;
        }

        return Objects.equals(expiry, vt.expiry);
    }

    @PrePersist
    private void updateExpiryDate() {
        expiry = LocalDateTime.now().plusDays(1L);
    }

    public boolean isValid() {
        return LocalDateTime.now().isBefore(expiry);
    }
}
