package com.udemy.boot.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailConfig {
    @Value("${mail.smtp.port}")
    private int portNumber;
    @Value("${mail.encoding}")
    private String encoding;


    @Bean
    public JavaMailSender mailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setPort(portNumber);
        mailSender.setDefaultEncoding(encoding);

        return mailSender;
    }
}
