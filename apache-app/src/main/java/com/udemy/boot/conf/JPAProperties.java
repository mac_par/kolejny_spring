package com.udemy.boot.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
@ConfigurationProperties(prefix = "apache.data.hibernate")
public class JPAProperties {
    private int minSize = 2;
    private int maxSize = 10;
    private int timeout = 1600;
    private int maxStatements = 5;
    private String dialect = "org.hibernate.dialect.PostgreSQLDialect";
    private boolean showSql;

    public int getMinSize() {
        return minSize;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getMaxStatements() {
        return maxStatements;
    }

    public void setMaxStatements(int maxStatements) {
        this.maxStatements = maxStatements;
    }

    public String getDialect() {
        return dialect;
    }

    public void setDialect(String dialect) {
        this.dialect = dialect;
    }

    public boolean isShowSql() {
        return showSql;
    }

    public void setShowSql(boolean showSql) {
        this.showSql = showSql;
    }

    public void setProperties(Properties properties) {
        properties.put("hibernate.c3p0.min_size" , minSize);
        properties.put("hibernate.c3p0.max_size" , maxSize);
        properties.put("hibernate.c3p0.timeout" , timeout);
        properties.put("hibernate.dialect" , dialect);
        properties.put("hibernate.show_sql" , showSql);
        properties.put("hibernate.hbm2ddl.auto" , "validate");
    }
}
