package com.udemy.boot.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableJpaRepositories(basePackages = "com.udemy.boot.repository")
@EnableTransactionManagement
public class DBConfig {
    private static final Logger log = LoggerFactory.getLogger(DBConfig.class);

    @Autowired
    private DBDetails details;

    @Bean
    public DataSource dataSource() {
        try {
            return DataSourceBuilder.create()
                    .driverClassName(details.getDriver())
                    .url(details.getUrl())
                    .username(details.getUser())
                    .password(details.getPasswd())
                    .build();
        } catch(Exception ex) {
            log.error("Jebło", ex);
            throw ex;
        }
    }

    @Bean
    public EntityManagerFactory entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean emfBean = new LocalContainerEntityManagerFactoryBean();
        emfBean.setDataSource(dataSource);
        emfBean.setPackagesToScan("com.udemy.boot.model");
        emfBean.setJpaProperties(jpaProperties());
        emfBean.setJpaVendorAdapter(vendorAdapter());
        emfBean.afterPropertiesSet();

        return emfBean.getNativeEntityManagerFactory();
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

    @Bean
    public Properties jpaProperties() {
        Properties properties = new Properties();
        details.getJPAProperties().setProperties(properties);
        return properties;
    }

    @Bean
    public JpaVendorAdapter vendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }
}
