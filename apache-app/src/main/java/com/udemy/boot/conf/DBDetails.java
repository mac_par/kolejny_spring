package com.udemy.boot.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "apache.data")
public class DBDetails {
    private String user;
    private String passwd;
    private String url;
    private String driver;

    private final JPAProperties JPAProperties;

    public DBDetails(JPAProperties JPAProperties) {
        this.JPAProperties = JPAProperties;
    }

    public String getUser() {
        return user;
    }

    public String getPasswd() {
        return passwd;
    }

    public String getUrl() {
        return url;
    }

    public String getDriver() {
        return driver;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public com.udemy.boot.conf.JPAProperties getJPAProperties() {
        return JPAProperties;
    }
}
