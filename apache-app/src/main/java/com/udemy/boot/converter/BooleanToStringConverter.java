package com.udemy.boot.converter;

import javax.persistence.AttributeConverter;

public class BooleanToStringConverter implements AttributeConverter<Boolean, Character> {
    private static final Character YES = 'Y';
    private static final Character NO = 'N';

    @Override
    public Character convertToDatabaseColumn(Boolean aBoolean) {
        if (aBoolean == null) {
            return NO;
        }

        return aBoolean ? YES : NO;
    }

    @Override
    public Boolean convertToEntityAttribute(Character s) {
        if (s == null) {
            return false;
        }
        return s.equals(YES);
    }
}
