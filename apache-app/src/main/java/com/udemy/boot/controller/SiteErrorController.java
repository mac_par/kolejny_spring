package com.udemy.boot.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Slf4j
@Controller
public class SiteErrorController implements ErrorController {

    static void populateModelDefaults(ModelAndView mav) {
        mav.getModel().put("status_h1", "Error Page");
        mav.getModel().put("page_title", "Shit has hit the fan!!! Deal with it mate!");
        mav.setViewName("error-page");
    }

    @RequestMapping("/forbidden")
    public ModelAndView handleError(ModelAndView mav) {
        log.info("Forbidden access has occurred");
        populateModelDefaults(mav);
        mav.getModel().put("error.details", "You are not allowed to enter this site");
        mav.getModel().put("error.message", "Forbidden access");
        mav.getModel().put("status_h1", "You are not allowed here mate!");
        mav.getModel().put("page_title", "Forbidden access attempt");

        return mav;
    }

    @RequestMapping("/error")
    public ModelAndView processError(HttpServletRequest request, ModelAndView mav) {
        log.info("An Error has occured");
        Object statusCodeObject = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (statusCodeObject != null) {
            int status = Integer.parseInt(statusCodeObject.toString());

            log.info("Error Status code - {}", status);
            if (HttpStatus.NOT_FOUND.value() == status) {
                mav.setViewName("error-page");
                mav.getModel().put("error.details", "Element not found");
                mav.getModel().put("error.message", "404 Not Found");
                mav.getModel().put("status_h1", "Sth was not found");
                mav.getModel().put("page_title", "Error 404  occurred");

            }
        } else {
            mav.setViewName("error-page");
        }

        return mav;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
