package com.udemy.boot.controller;

import com.udemy.boot.model.StatusUpdate;
import com.udemy.boot.service.StatusUpdateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Slf4j
public class PageController {
    private final StatusUpdateService service;

    @Autowired
    public PageController(StatusUpdateService service) {
        this.service = service;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home(ModelAndView mav) {
        log.info("Entering home page");
        StatusUpdate statusUpdate = service.getRecent();
        mav.getModel().put("page_title", "Welcome to Homepage");
        mav.getModel().put("recent_status", statusUpdate);
        mav.setViewName("home");
        return mav;
    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String about(Model model) {
        log.info("Entering about page");
        model.addAttribute("page_title", "About Page");
        model.addAttribute("about_h1", "About page data");
        return "about";
    }
}
