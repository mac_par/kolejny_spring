package com.udemy.boot.controller;

import com.udemy.boot.model.Profile;
import com.udemy.boot.model.ProfileDao;
import com.udemy.boot.model.User;
import com.udemy.boot.service.AccountService;
import com.udemy.boot.service.ProfileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class ProfileController {
    private final ProfileService profileService;
    private final AccountService accountService;

    @Autowired
    public ProfileController(ProfileService profileService, AccountService accountService) {
        this.profileService = profileService;
        this.accountService = accountService;
    }

    @RequestMapping(value = "profile", method = RequestMethod.GET)
    public ModelAndView showProfile(ModelAndView mav) {
        log.info("Preparing user profile data");

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = accountService.getUserByUsername(authentication.getName());
        log.info("User {}", user);
        Profile userProfile = profileService.getProfileByUser(user);
        mav.getModel().put("profile", userProfile);
        mav.getModel().put("page_title", "Profile Page");
        mav.setViewName("profile_site");
        return mav;
    }

    @RequestMapping(value = "/editProfile", method = RequestMethod.GET)
    public ModelAndView prepareEditProfileForm(ModelAndView mav, @RequestParam("profileId") Long profileId) {
        Profile profile = profileService.getById(profileId);
        ProfileDao profileDao = new ProfileDao(profile);
        mav.getModel().put("profile", profileDao);
        mav.getModel().put("page_title", "Profile edit");
        mav.getModel().put("status_h1", "Edit Profile");

        mav.setViewName("edit-profile");
        return mav;
    }

    @RequestMapping(value = "/editProfile", method = RequestMethod.POST)
    public ModelAndView prepareEditProfileForm(ModelAndView mav, @Valid @ModelAttribute ProfileDao profile, BindingResult result) {
        log.info("Processing profile edition");

        if (result.hasErrors()) {
            final String errors = result.getAllErrors().stream().map(ObjectError::getObjectName).collect(Collectors.joining(", "));
            mav.getModel().put("errors", errors);
            mav.getModel().put("profile", profile);
            mav.getModel().put("page_title", "Profile edit");
            mav.getModel().put("status_h1", "Edit Profile");

            mav.setViewName("edit-profile");
        } else {
            Profile newProfile = profileService.getById(profile.getId());
            newProfile.setAbout(profile.getDescription());
            profileService.save(newProfile);
            mav.setViewName("redirect:/profile");
        }

        return mav;
    }
}
