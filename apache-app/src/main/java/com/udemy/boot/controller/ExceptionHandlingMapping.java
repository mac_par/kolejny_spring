package com.udemy.boot.controller;

import com.udemy.boot.service.handling.SiteException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionHandlingMapping {
    private static final String MAIN_MESSAGE = "Viewer Error: ";

    @ExceptionHandler({RuntimeException.class})
    public ModelAndView handleRuntimeException(RuntimeException inputException, ModelAndView mav) {
        SiteException exception = new SiteException(MAIN_MESSAGE + inputException.getMessage(), inputException);
        SiteErrorController.populateModelDefaults(mav);
        mav.getModel().put("error", exception);

        return mav;
    }

    @ExceptionHandler({SiteException.class})
    public ModelAndView handleRuntimeException(SiteException exception, ModelAndView mav) {
        mav.getModel().put("error", exception);
        SiteErrorController.populateModelDefaults(mav);
        return mav;
    }
}
