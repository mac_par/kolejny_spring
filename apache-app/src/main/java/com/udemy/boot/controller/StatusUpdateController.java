package com.udemy.boot.controller;

import com.udemy.boot.model.StatusUpdate;
import com.udemy.boot.service.StatusUpdateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class StatusUpdateController {
    private final StatusUpdateService service;

    @Autowired
    public StatusUpdateController(StatusUpdateService service) {
        this.service = service;
    }

    @RequestMapping(value = "addStatus", method = RequestMethod.GET)
    public ModelAndView addStatus(ModelAndView mav, @ModelAttribute("entry") StatusUpdate entry) {
        log.info("Entering addStatus page");
        mav.setViewName("addStatus");
        mav.getModel().put("page_title", "Add Status Page");
        mav.getModel().put("status_h1", "Adding Status, or Not?");
        mav.getModel().put("entry", entry);
        return mav;
    }

    @RequestMapping(value = "addStatus", method = RequestMethod.POST)
    public ModelAndView processStatusUpdate(ModelAndView mav, @Valid StatusUpdate entry, BindingResult result) {
        log.info("Updating");

        if (result.hasErrors()) {
            log.error("There are {} errors upon post operation", result.getErrorCount());
            mav.getModel().put("errorMsg", getErrorMessage(result));
            mav.getModel().put("status_h1", "Coś jebło!");
            mav.getModel().put("entry", entry);
            mav.getModel().put("page_title", "Add Status Page");
            mav.setViewName("addStatus");
        } else {
            persistAndPopulate(mav, entry);
            mav.setViewName("redirect:/viewStatus");
        }
        return mav;
    }

    private void persistAndPopulate(ModelAndView mav, StatusUpdate entry) {
        if (!service.checkContentUniquess(entry.getText())) {
            mav.getModel().put("errorMsg", String.format("'%s' is not unique.", entry.getText()));
            mav.getModel().put("status_h1", "Coś jebło!");
            mav.getModel().put("entry", entry);
        } else {
            mav.getModel().put("status_h1", "Adding Status, or Not?");
            service.save(entry);
            mav.getModel().put("entry", new StatusUpdate());
        }
    }

    @ModelAttribute("recent")
    public StatusUpdate getRecent() {
        return service.getRecent();
    }

    private String getErrorMessage(BindingResult result) {
        return result.getAllErrors().stream()
                .map(ObjectError::getDefaultMessage).collect(Collectors.joining("\n"));
    }

    @RequestMapping(value = "viewStatus", method = RequestMethod.GET)
    public ModelAndView viewStatus(ModelAndView mav, @RequestParam(value = "page", defaultValue = "1") int page) {
        log.info("Viewing status updates for page #{}", page);

        mav.getModel().put("page_title", "View Status Update - Page " + page);
        mav.getModel().put("status_h1", "Viewing paged results");
        mav.setViewName("status-view");
        Page<StatusUpdate> pages = service.page(page);

        mav.getModel().put("results", pages);

        return mav;
    }

    @RequestMapping(value = "deleteStatus", method = RequestMethod.GET)
    public ModelAndView deleteUpdate(ModelAndView mav, @RequestParam Long id) {
        log.info("Deleting status id {}", id);

        if (!service.contains(id)) {
            log.error("StatusUpdate for {} does not exist", id);
        } else {
            service.delete(id);
            log.info("StatusUpdate identified by {} was deleted", id);
        }
        mav.setViewName("redirect:/viewStatus");
        return mav;
    }

    @RequestMapping(value = "editStatus", method = RequestMethod.GET)
    public ModelAndView prepareUpdate(ModelAndView mav, @RequestParam Long id) {
        log.info("Preparing for update of StatusUpdate, that can be identified by {}", id);
        if (!service.contains(id)) {
            log.error("Provided id {} is incorrect", id);
            mav.setViewName("redirect:/viewStatus");
        } else {
            mav.setViewName("edit-status");
            mav.getModel().put("status_h1", "Edit choosed Status Update instance");
            mav.getModel().put("page_title", "Edit Status Update Page");
            mav.getModel().put("entry", service.get(id));
        }

        return mav;
    }

    @RequestMapping(value = "editStatus", method = RequestMethod.POST)
    public ModelAndView prepareUpdate(ModelAndView mav, @Valid StatusUpdate entity, BindingResult result) {
        log.info("Performing StatusUpdate entity identified by {}", entity.getId());
        if (result.hasErrors()) {
            mav.setViewName("edit-status");
            mav.getModel().put("status_h1", "There were errors while processing update. Please review it.");
            mav.getModel().put("page_title", "Edit Status Update Page");
            mav.getModel().put("errors", getErrorMessage(result));
            mav.getModel().put("entry", entity);
        } else {
            service.update(entity);
            mav.setViewName("redirect:/viewStatus");
        }

        return mav;
    }
}
