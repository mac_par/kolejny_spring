package com.udemy.boot.controller;

import com.udemy.boot.model.TokenType;
import com.udemy.boot.model.User;
import com.udemy.boot.model.UserDao;
import com.udemy.boot.model.VerificationToken;
import com.udemy.boot.service.AccountService;
import com.udemy.boot.service.MailingService;
import com.udemy.boot.service.ProfileService;
import com.udemy.boot.service.TokenService;
import com.udemy.boot.service.handling.SiteException;
import com.udemy.boot.service.validation.ValidateMatchingPassword;
import com.udemy.boot.service.validation.ValidatePassword;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class AccountController {
    private static final String USER_DAO_KEY = "user";

    private final AccountService accountService;
    private final MailingService mailingService;
    private final TokenService tokenService;
    private final ProfileService profileService;

    @Autowired
    public AccountController(AccountService accountService, MailingService mailingService, TokenService tokenService, ProfileService profileService) {
        this.accountService = accountService;
        this.mailingService = mailingService;
        this.tokenService = tokenService;
        this.profileService = profileService;
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView login(ModelAndView mav) {
        log.info("Display Login form");
        mav.getModel().put("status_h1", "Login");
        mav.getModel().put("page_title", "Login Page");
        mav.setViewName("login-page");
        return mav;
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ModelAndView loginForward(ModelAndView mav) {
        log.info("Forwarding successful login");
        mav.setViewName("redirect:/");
        return mav;
    }

    @RequestMapping(value = "register", method = RequestMethod.GET)
    public ModelAndView registerForward(ModelAndView mav) {
        log.info("Prepare register account form");

        mav.getModel().put("page_data", "");
        setPageModel(mav);
        mav.getModel().put(USER_DAO_KEY, new UserDao());
        mav.setViewName("register-page");
        return mav;
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public ModelAndView processRegisterRequest(Principal principal, ModelAndView mav, @Validated({ValidatePassword.class}) UserDao userDao, BindingResult results) {
        log.info("Process add account request");
        setPageModel(mav);
        if (results.hasErrors()) {
            mav.getModel().put("errorMsg", getErrorMessage(results));
            mav.getModel().put(USER_DAO_KEY, userDao);
            mav.setViewName("register-page");
        } else if (accountService.userExists(userDao.getUsername())) {
            log.info("Provided username already exists");
            mav.getModel().put("errorMsg", "Username must be unique");
            mav.getModel().put(USER_DAO_KEY, userDao);
            mav.setViewName("register-page");
        } else {
            prepareAccount(userDao);
            mav.getModel().put(USER_DAO_KEY, new UserDao());

            mav.setViewName(principal != null ? "register-page" : "redirect:/verifyEmail");
        }

        return mav;
    }

    void prepareAccount(UserDao userDao) {
        User user = convertDaoToUserEntity(userDao);
        log.info("Persisting user account");
        accountService.save(user);
        profileService.create(user, null);
        final String token = tokenService.generateRegistrationToken(user);
        mailingService.sendConfirmationMail(user, token);
    }

    private String getErrorMessage(BindingResult result) {
        return result.getAllErrors().stream()
                .map(ObjectError::getDefaultMessage).collect(Collectors.joining("\n"));
    }

    private void setPageModel(ModelAndView mav) {
        mav.getModel().put("status_h1", "Register a new user");
        mav.getModel().put("page_title", "Registration Page");
    }

    private User convertDaoToUserEntity(UserDao userDao) {
        if (Objects.isNull(userDao)) {
            throw new SiteException("In order to create account user-related data is required.");
        }

        return accountService.createUser(userDao);
    }

    @RequestMapping(value = "usersAccounts", method = RequestMethod.GET)
    public ModelAndView displayUserData(Principal principal, @RequestParam(name = "page", defaultValue = "1") int page, ModelAndView mav) {
        log.info("User {} gets list of users starting from page number {}", principal.getName(), page);
        Page<User> userPageResults = accountService.page(page);
        mav.getModel().put("results", userPageResults);
        mav.getModel().put("page_title", "Users accounts display");
        mav.getModel().put("status_h1", "Accounts list");
        mav.setViewName("view-users");

        return mav;
    }

    @RequestMapping(value = "deleteUser", method = RequestMethod.GET)
    public ModelAndView displayUserData(Principal principal, @RequestParam(name = "id") long userId, ModelAndView mav) {
        final String username = principal.getName();
        log.info("User {} tries to remove account identified by id {}", username, userId);
        hasAdminRole(username);
        tokenService.removeByUser(userId);
        profileService.removeByUserId(userId);
        accountService.remove(userId);
        mav.setViewName("redirect:/usersAccounts");

        return mav;
    }

    @RequestMapping(value = "editUser", method = RequestMethod.GET)
    public ModelAndView prepareUserData(Principal principal, @RequestParam(name = "id") long userId, ModelAndView mav) {
        final String username = principal.getName();
        log.info("User {} retrieves data for account identified by id {}", username, userId);
        hasAdminRole(username);
        mav.getModel().put("page_title", "Edit user account");
        mav.getModel().put("status_h1", "Account details");
        User user = accountService.findUserById(userId);
        UserDao userDao = new UserDao(user);
        mav.getModel().put("entry", userDao);
        mav.setViewName("edit-user");

        return mav;
    }

    @RequestMapping(value = "editUser", method = RequestMethod.POST)
    public ModelAndView processUserDataEditRequest(ModelAndView mav, @ModelAttribute(name = "entry") @Validated UserDao userDao, BindingResult results) {
        log.info("An attempt to persist changes to User: {}", userDao.getUsername());
        if (results.hasErrors()) {
            final String errorMessage = getErrorMessage(results);
            mav.getModel().put("errors", errorMessage);
            mav.getModel().put("page_title", "Edit user account");
            mav.getModel().put("status_h1", "Account details");
            mav.getModel().put("entry", userDao);
            mav.setViewName("edit-user");

            return mav;
        }

        User user = accountService.convertToUser(userDao);
        log.info("Updating user id: {}, username: {}, authorities: {}", user.getId(), user.getUsername(), user.getAuthorities());
        accountService.save(user);
        mav.setViewName("redirect:/usersAccounts");

        return mav;
    }

    private void hasAdminRole(String username) {
        if (!accountService.hasAdminRole(username)) {
            throw new SiteException(String.format("User %s does not have admin role assigned.", username));
        }
    }

    @RequestMapping(value = "changePassword", method = RequestMethod.GET)
    public ModelAndView preparePasswordChangeForm(Principal principal, @RequestParam(name = "id") long userId, ModelAndView mav) {
        final String username = principal.getName();
        log.info("User {} wants to change password for account identified by id {}", username, userId);
        hasAdminRole(username);
        User user = accountService.findUserById(userId);
        UserDao userDao = new UserDao();
        userDao.setUsername(user.getUsername());

        mav.getModel().put("status_h1", "Please provide a new password");
        mav.getModel().put("page_title", "Change user's password");
        mav.getModel().put("entry", userDao);
        mav.setViewName("edit-password");

        return mav;
    }

    @RequestMapping(value = "changePassword", method = RequestMethod.POST)
    public ModelAndView processPasswordChange(ModelAndView mav,
                                              @ModelAttribute(name = "entry") @Validated({ValidateMatchingPassword.class, ValidatePassword.class}) UserDao userDao,
                                              BindingResult results) {
        log.info("An attempt to persist changes to User: {}", userDao.getUsername());

        if (results.hasErrors()) {
            String errorMessage = getErrorMessage(results);
            log.error("Provided passwords do not match");
            mav.getModel().put("errors", errorMessage);
            mav.getModel().put("page_title", "Change user's password");
            mav.getModel().put("status_h1", "Please provide a new password");
            mav.getModel().put("entry", userDao);
            mav.setViewName("edit-password");
            return mav;
        }

        updatePassword(userDao);
        mav.setViewName("redirect:/usersAccounts");

        return mav;
    }

    private void updatePassword(UserDao entity) {
        User user = accountService.getUserByUsername(entity.getUsername());
        user.setPassword(entity.getPassword());
        accountService.encodePassword(user);
        accountService.save(user);
    }

    @RequestMapping(value = "verifyEmail", method = RequestMethod.GET)
    public ModelAndView verifyEmail(ModelAndView mav) {
        mav.getModel().put("page_title", "Registration: Success");
        mav.getModel().put("message", "A verification e-mail was sent. Please check your inbox.");
        mav.getModel().put("status_h1", "Account was successfully registered");
        mav.setViewName("email-verification");

        return mav;
    }

    @RequestMapping(value = "registerAdmin", method = RequestMethod.GET)
    public ModelAndView registerAdminRedirect(ModelAndView mav) {
        mav.setViewName("redirect:register");

        return mav;
    }

    @RequestMapping(value = "confirmEmail", method = RequestMethod.GET)
    public ModelAndView emailConfirmationViaToken(ModelAndView mav, @RequestParam(value = "token") String token) {
        VerificationToken tokenInstance = tokenService.findByToken(token);
        User user = tokenInstance.getUser();

        log.info("Token {} was assigned to user {}", tokenInstance.getType(), user.getUsername());
        if (tokenInstance.getType() == TokenType.REGISTRATION) {
            if (tokenInstance.isValid()) {
                log.info("Enabling user account after registration");
                user.setEnabled(true);
                mav.getModel().put("page_title", "Registration: Email verified");
                mav.getModel().put("message", "Email was verified: " + user.getUsername());
                mav.getModel().put("status_h1", "Email was successfully confirmed.");
                accountService.save(user);
            } else {
                log.info("Token was invalid. Resending verification email");
                String newToken = tokenService.generateRegistrationToken(user);
                mailingService.sendConfirmationMail(user, newToken);
                mav.getModel().put("page_title", "Registration: Email - Token has expired");
                mav.getModel().put("message", "Provided token was outdated. New token was sent to your email.");
                mav.getModel().put("status_h1", "Invalid tokens");
            }
            tokenService.remove(tokenInstance.getId());
        } else {
            mav.getModel().put("page_title", "Registration: Email - Unknown token type");
            mav.getModel().put("message", "Unknown token type was received");
            mav.getModel().put("status_h1", "Something went wrong;/");
        }


        mav.setViewName("email-verification");

        return mav;
    }

    @RequestMapping(value = "resetPassword", method = RequestMethod.GET)
    public ModelAndView prepareResetPasswordForm(@RequestParam(name = "token") String token, ModelAndView mav) {
        log.info("Token {} was received for password reset", token);
        VerificationToken verificationToken = tokenService.findByToken(token);
        if (verificationToken.isValid()) {
            log.info("Provided token is valid");
            User user = verificationToken.getUser();
            user.setPassword("password1234");
            user.setLocked(false);
            accountService.persistNewPassword(user);
            mav.getModel().put("page_title", "Password Reset for " + user.getUsername());
            mav.getModel().put("status_h1", "Set new password");
            mav.getModel().put("message", "By default your password was set to 'password1234'");
            mav.setViewName("info");
        } else {
            log.info("Provided Reset Password token is expired");
            mav.setViewName("redirect:/login");
        }

        tokenService.remove(verificationToken.getId());

        return mav;
    }

    @RequestMapping(value = "resetPasswordRequest", method = RequestMethod.GET)
    public ModelAndView prepareResetPasswordRequestForm(ModelAndView mav) {
        mav.getModel().put("page_title", "Password Reset Request");
        mav.getModel().put("status_h1", "Please provide username");
        mav.setViewName("reset-password-request");

        return mav;
    }

    @RequestMapping(value = "resetPasswordRequest", method = RequestMethod.POST)
    public ModelAndView processResetPasswordRequestForm(ModelAndView mav, @RequestParam("username") String username) {
        log.info("Reset Password Request for username: {}", username);
        mav.getModel().put("page_title", "Password Reset Request");
        mav.getModel().put("status_h1", "Please provide username");
        mav.setViewName("reset-password-request");
        if (accountService.userExists(username)) {
            User user = accountService.getUserByUsername(username);
            String token = tokenService.generatePasswordResetToken(user);
            user.setLocked(true);
            accountService.save(user);
            mailingService.sendResetPasswordMail(user, token);

            mav.getModel().put("resetMessage", "Request url was sent on your e-mail");
        } else {
            log.info("{} does not exist", username);
            mav.getModel().put("errorMsg", "Only existing usernames are allowed");
        }

        return mav;
    }
}
