create table status_update(
    status_id number(9),
    text_content varchar2(256) not null,
    date_added timestamp not null,
    date_modified timestamp not null,
    constraint status_pk primary key (status_id)
);

create sequence status_update_seq minvalue 1 maxvalue 999999999 increment by 1 start with 1 nocycle cache 20;

create or replace trigger status_update_trig_bi before insert on status_update for each row
when(new.status_id is null)
declare
begin
    :new.status_id := status_update_seq.nextval;
end;
/

create table user_authorities(
    auth_id number(9) not null,
    authority_name VARCHAR2(60) UNIQUE,
    date_created TIMESTAMP NOT NULL,
    date_modified TIMESTAMP NOT NULL,
    constraint user_authorities_PK primary key (auth_id)
);

create sequence user_authorities_sequence minvalue 1 maxvalue 999999999 start with 1 increment by 1 nocycle cache 20;

create or replace trigger user_authorites_trigger_bi before insert on user_authorities for each row
when(new.auth_id is null)
begin
    :new.auth_id := user_authorities_sequence.nextval;
end;
/

create table users_data(
    user_id number(9) not null,
    user_name VARCHAR2(21) NOT NULL,
    user_password VARCHAR2(255) NOT NULL,
    last_modified_date TIMESTAMP NOT NULL,
    credentials_modification_date  TIMESTAMP NOT NULL,
    account_enabled CHAR(1) DEFAULT 'Y',
    account_locked CHAR(1) DEFAULT 'N',
    constraint user_data_pk primary key (user_id)
);

create sequence users_data_sequence minvalue 1 maxvalue 999999999 start with 1 increment by 1 nocycle cache 20;

create or replace trigger user_data_trigger_bi before insert on users_data for each row
when(new.user_id is null)
begin
    :new.user_id := users_data_sequence.nextval;
end;
/

create table user_authority_join(
    id number(9),
    user_id number(9),
    authority_id number(9),
    constraint user_auth_join_pk primary key(id)
);

alter table user_authority_join add constraint auth_rel_fk foreign key (authority_id) references user_authorities(auth_id);
alter table user_authority_join add constraint user_rel_fk foreign key (user_id) references users_data(user_id);
alter table user_authority_join add constraint user_auth_uq unique (authority_id, user_id);

create sequence user_auth_join_seq minvalue 1 maxvalue 999999999 start with 1 increment by 1 nocycle cache 20;

create or replace trigger user_auth_join_tg_bi before insert on user_authority_join for each row
when(new.id is null)
begin
    :new.id := user_auth_join_seq.nextval;
end;
/

create table verification_tokens(
    token_id number(9),
    token VARCHAR2(255) NOT NULL,
    user_id number(9) not null,
    token_type varchar2(60) not null,
    expiration_date Timestamp not null,
    constraint verification_token_pk primary key (token_id)
);

create sequence verification_token_sequence minvalue 1 maxvalue 999999999 start with 1 increment by 1 nocache nocycle;
alter table verification_tokens add constraint token_user_fk foreign key (user_id) references users_data(user_id);

create or replace trigger verification_token_tr_bi before insert on verification_tokens for each row
when(new.token_id is null)
begin
    :new.token_id := verification_token_sequence.nextval;
end;
/

create table profiles_data(
    id number(9),
    user_id number(9) not null,
    profile_desc varchar2(256) default '',
    constraint profile_pk primary key (id)
);

alter table profiles_data add constraint profile_user_fk foreign key(user_id) references users_data(user_id);

create sequence profiles_data_seq minvalue 1 maxvalue 999999999 start with 1 increment by 1 nocycle cache 20;

create or replace trigger profiles_data_tr_bi before insert on profiles_data for each row
when(new.id is null)
begin
    :new.id := profiles_data_seq.nextval;
end;
/