package com.udemy.boot.service;

import com.udemy.boot.model.User;
import com.udemy.boot.model.UserAuthority;
import com.udemy.boot.model.UserDao;
import com.udemy.boot.repository.UserAuthorityRepository;
import com.udemy.boot.repository.UserRepository;
import com.udemy.boot.service.handling.SiteException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;


class AccountServiceBeanTest {
    private static final String TEST_BASIC_USER = "user";
    private static final String TEST_ADMIN_USER = "admin";

    private UserRepository userRepository;
    private UserAuthorityRepository authorityRepository;
    private PasswordEncoder passwordEncoder;

    private AccountService service;

    @BeforeEach
    void setUp() {
        this.userRepository = mock(UserRepository.class);
        this.passwordEncoder = mock(PasswordEncoder.class);
        this.authorityRepository = mock(UserAuthorityRepository.class);
        this.service = new AccountServiceBean(userRepository, authorityRepository, passwordEncoder);
    }

    @Test
    void basicUserShouldBeCreatedFromDao() {
        //given
        final Long expectedId = 1L;
        final String expectedPassword = "test-password";
        final String expectedUsername = "Marco Polo";

        UserAuthority authority = new UserAuthority(TEST_BASIC_USER);
        authority.setId(expectedId);

        UserDao userDao = new UserDao();
        userDao.setUsername(expectedUsername);
        userDao.setPassword(expectedPassword);

        when(passwordEncoder.encode(anyString())).thenReturn(expectedPassword);
        when(authorityRepository.getAuthorityByNames(List.of(TEST_BASIC_USER))).thenReturn(List.of(authority));

        //when
        User user = service.createUser(userDao);
        //then

        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo(expectedUsername);
        assertThat(user.getPassword()).isEqualTo(expectedPassword);
        assertThat(user.getAuthorities()).size().isEqualTo(1);
        assertThat(user.getAuthorities()).isEqualTo(Set.of(authority));
    }

    @Test
    void userWithAdminRoleShouldBeCreatedFromDao() {
        //given
        final String expectedPassword = "test-password";
        final String expectedUsername = "Marco Polo";

        UserAuthority authority1 = new UserAuthority(TEST_BASIC_USER);
        UserAuthority authority2 = new UserAuthority(TEST_ADMIN_USER);

        UserDao userDao = new UserDao();
        userDao.setAdmin(true);
        userDao.setUsername(expectedUsername);
        userDao.setPassword(expectedPassword);

        when(passwordEncoder.encode(anyString())).thenReturn(expectedPassword);
        when(authorityRepository.getAuthorityByNames(List.of(TEST_BASIC_USER, TEST_ADMIN_USER))).thenReturn(List.of(authority1, authority2));

        //when
        User user = service.createUser(userDao);
        //then

        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo(expectedUsername);
        assertThat(user.getPassword()).isEqualTo(expectedPassword);
        assertThat(user.getAuthorities()).size().isEqualTo(2);
        assertThat(user.getAuthorities()).isEqualTo(Set.of(authority1, authority2));
    }

    @Test
    void siteExceptionIsThrownOnNullUserDaoReference() {
        //given
        //when
        //then
        assertThatThrownBy(() -> service.createUser(null))
        .hasMessage("Initial data is required to create an user account instance.")
        .isInstanceOf(SiteException.class);
    }

    @Test
    void retrievesAllUserAuthorities() {
        //given
        UserAuthority authority1 = new UserAuthority(TEST_ADMIN_USER);
        UserAuthority authority2 = new UserAuthority(TEST_BASIC_USER);

        when(authorityRepository.findAll()).thenReturn(List.of(authority1, authority2));

        //when
        List<UserAuthority> allAuthorities = service.getAllAuthorities();

        //then
        assertThat(allAuthorities).isNotNull();
        assertThat(allAuthorities).isNotEmpty();
        assertThat(allAuthorities).contains(authority1, authority2);
    }

    @Test
    void retrieveAuthorityByNames() {
        //given
        UserAuthority authority1 = new UserAuthority(TEST_ADMIN_USER);
        UserAuthority authority2 = new UserAuthority(TEST_BASIC_USER);
        List<String> args = List.of(TEST_BASIC_USER, TEST_ADMIN_USER, "PUSTAK");
        when(authorityRepository.getAuthorityByNames(args)).thenReturn(List.of(authority1, authority2));

        //when
        List<UserAuthority> userAuthorities = service.getAuthorityByNames(args);

        assertThat(userAuthorities).isNotNull();
        assertThat(userAuthorities).isNotEmpty();
        assertThat(userAuthorities).contains(authority1, authority2);
    }

    @Test
    void retrieveAuthorityByName() {
        //given
        UserAuthority authority = new UserAuthority(TEST_BASIC_USER);
        when(authorityRepository.getAuthorityByName(TEST_BASIC_USER)).thenReturn(authority);

        //when
        UserAuthority userAuthority = service.getAuthorityByName(TEST_BASIC_USER);

        assertThat(userAuthority).isNotNull();
        assertThat(userAuthority).isEqualTo(authority);
    }

    @Test
    void userShouldBeSaved() {
        //given
        User user = new User("Marco", "Polo");

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        //when
        service.save(user);

        //then
        verify(userRepository).save(userArgumentCaptor.capture());
        assertThat(userArgumentCaptor.getValue()).isEqualTo(user);
    }

    @Test
    void anAttemptToRetrieveUnexistingUserThrowsException() {
        //given
        final String user = "jakis tam nie sprecyzowany uzytkownik";

        //when
        //then
        assertThatThrownBy(() -> service.loadUserByUsername(user))
                .hasMessage(String.format("User '%s' does not exist", user))
                .isInstanceOf(UsernameNotFoundException.class);
    }

    @Test
    void retrieveForExistingUserReturnsItsInstance() {
        //given
        final String user = "Marco Polo";
        final User expectedUser = new User(user, "costam");
        when(userRepository.findUserByUsername(user)).thenReturn(Optional.of(expectedUser));

        //when
        UserDetails userDetails = service.loadUserByUsername(user);
        //then
        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(user);
    }

    @Test
    void whenUserHasAdminRoleTrueIsReturned() {
        //given
        final boolean expectedResult = true;
        final String username = "test-user-having-admin-role";
        UserAuthority adminAuthority = new UserAuthority("admin");
        User user = new User();
        user.addAuthority(adminAuthority);
        when(userRepository.findUserByUsername(username)).thenReturn(Optional.of(user));

        //when
        final boolean result = service.hasAdminRole(username);

        //then
        assertThat(result).isTrue();
        verify(userRepository, atMostOnce()).findUserByUsername(username);
    }

    @Test
    void whenUserDoesNotHaveAdminRoleFalseIsReturned() {
        //given
        final boolean expectedResult = false;
        final String username = "test-user-having-admin-role";
        UserAuthority nonAdminAuthority = new UserAuthority("user");
        User user = new User();
        user.addAuthority(nonAdminAuthority);
        when(userRepository.findUserByUsername(username)).thenReturn(Optional.of(user));

        //when
        final boolean result = service.hasAdminRole(username);

        //then
        assertThat(result).isFalse();
        verify(userRepository, atMostOnce()).findUserByUsername(username);
    }

    @Test
    void whenUserIsRemovedRepoIsUsed() {
        //given
        final long testUserId = 1L;

        //when
        service.remove(testUserId);

        //then
        verify(userRepository, times(1)).deleteById(testUserId);
    }

    @Test
    void whenUserIsFoundByIdItsInstanceIsReturned() {
        //given
        final User dummyUser = new User("testowy", "testowy");
        final long id = 1L;
        when(userRepository.findById(id)).thenReturn(Optional.of(dummyUser));

        //when
        final User result = service.findUserById(id);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(dummyUser);
        verify(userRepository, times(1)).findById(id);
    }

    @Test
    void whenUserIsNotFoundByIdExceptionIsThrown() {
        //given
        final long id = 1L;

        //when
        //then
        assertThatThrownBy(() -> service.findUserById(id))
        .isInstanceOf(SiteException.class)
        .hasMessage(String.format("Account identified by id {} does not exist", id));
    }

    @Test
    void whenUserIsFoundByNameItsInstanceIsReturned() {
        //given
        final String username = "jakis_ludek";
        final User dummyUser = new User(username, "testowy");
        when(userRepository.findUserByUsername(username)).thenReturn(Optional.of(dummyUser));

        //when
        final User result = service.getUserByUsername(username);

        //then
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(dummyUser);
        verify(userRepository, times(1)).findUserByUsername(username);
    }

    @Test
    void whenUserIsNotFoundByNameExceptionIsThrown() {
        //given
        final String username = "ludek";

        //when
        //then
        assertThatThrownBy(() -> service.getUserByUsername(username))
                .isInstanceOf(SiteException.class)
                .hasMessage(String.format("User %s does not exist", username));
    }

    @Test
    void whenUserInstanceIsPassedUserPasswordIsEncoded() {
        //given
        final String expectedPassword = "scisle-tajne-haslo";
        final String plainText = "haslo";

        User user = new User("", plainText);
        when(passwordEncoder.encode(plainText)).thenReturn(expectedPassword);

        //when
        service.encodePassword(user);

        //then
        assertThat(user.getPassword()).isEqualTo(expectedPassword);
    }

    @Test
    void whenUserInstanceIsNullNullPointerExceptionIsThrown() {
        //given
        final String plainText = "haslo";

        final User user = null;

        //when
        //then
        assertThatThrownBy(() -> service.encodePassword(user))
        .isInstanceOf(NullPointerException.class);
    }
}