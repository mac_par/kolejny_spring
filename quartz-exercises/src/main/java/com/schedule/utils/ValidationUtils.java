package com.schedule.utils;

import com.schedule.model.exception.AppException;

import static java.util.UUID.fromString;

public class ValidationUtils {
    private ValidationUtils() {
    }

    public static void isUuid(String id) {
        try {
            fromString(id);
        } catch (IllegalArgumentException ex) {
            throw new AppException(String.format("[%s] is not a valid UUID", id), ex);
        }
    }

}
