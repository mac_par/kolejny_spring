package com.schedule.utils;

public class SchedulerFields {
    private SchedulerFields() {

    }

    public static final String START_TIME = "START_TIME";
    public static final String END_TIME = "END_TIME";
    public static final String CREATION_TIME = "CREATION_TIME";
    public static final String LAG_FIELD = "LAG";
    public static final String DEPTH_FIELD = "DEPTH";
    public static final String EXPRESSION_FIELD = "CRON_EXPRESSION";
    public static final String NEW_JOB = "INITIAL_VALUE";
    public static final String NEW_JOB_INITIAL_VALUE = "true";
    public static final String SKIP_EXECUTION_FIELD = "SKIP_EXECUTION";
    public static final String SKIP_EXECUTION_ON = "true";
    public static final String SKIP_EXECUTION_OFF = "false";
}
