package com.schedule.utils;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ScheduleUtils {
    public static final String HISTORICAL_JOB_POSTFIX = "-H";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private static final DateTimeFormatter SCHEDULE_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);

    private ScheduleUtils() {

    }

    public static String convertLocalDateTimeToString(LocalDateTime dateTime) {
        return dateTime == null ? null : dateTime.format(SCHEDULE_FORMATTER);
    }

    public static Date convertLocalDateTimeToDate(LocalDateTime dateTime) {
        return dateTime == null ? null : Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime convertDateToLocalDateTime(Date date) {
        return date == null ? null : date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalDateTime convertStringToLocalDateTime(String dateString) {
        return dateString == null ? null : LocalDateTime.parse(dateString, SCHEDULE_FORMATTER);
    }

    public static String convertToHistoricalGroupName(String packageId) {
        return packageId + HISTORICAL_JOB_POSTFIX;
    }

    public static String extractPackageIdFromHistoricGroupName(String historicalPackageId) {
        return StringUtils.removeEnd(historicalPackageId, HISTORICAL_JOB_POSTFIX);
    }
}
