package com.schedule.service;

import com.schedule.jobs.HistoricalJob;
import com.schedule.jobs.StandardJob;
import com.schedule.model.CronTriggerDto;
import com.schedule.model.DepthType;
import com.schedule.model.ScheduleDto;
import com.schedule.model.exception.AppException;
import com.schedule.service.jobscheduler.HistoricalJobScheduler;
import com.schedule.service.jobscheduler.JobSchedulerService;
import com.schedule.service.jobscheduler.StandardJobScheduler;
import com.schedule.utils.ScheduleUtils;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.schedule.utils.ScheduleUtils.convertLocalDateTimeToDate;
import static com.schedule.utils.ScheduleUtils.convertToHistoricalGroupName;
import static com.schedule.utils.SchedulerFields.*;
import static com.schedule.utils.ValidationUtils.isUuid;

@Service
@Slf4j
public class SchedulerService {
    private static final Predicate<TriggerKey> EMPTY_TRIGGER_KEY_PREDICATE = triggerKey -> true;

    private Scheduler scheduler;
    private StandardJobScheduler standardJobScheduler;
    private HistoricalJobScheduler historicalJobScheduler;

    @Autowired
    public SchedulerService(Scheduler scheduler, StandardJobScheduler standardJobScheduler, HistoricalJobScheduler historicalJobScheduler) {
        this.scheduler = scheduler;
        this.standardJobScheduler = standardJobScheduler;
        this.historicalJobScheduler = historicalJobScheduler;
    }

    public CronTriggerDto schedule(CronTriggerDto cronTriggerDto) throws SchedulerException {
        log.info("Received CronTriggerDtos: {}", cronTriggerDto);

        Objects.requireNonNull(cronTriggerDto, "CronTriggerDto must be provided");
        isUuid(cronTriggerDto.getPackageId());
        cronTriggerDto.getSchedules().forEach(scheduleDto -> isUuid(scheduleDto.getId()));

        String packageId = cronTriggerDto.getPackageId();
        return scheduleTriggersByClassAndPredicate(cronTriggerDto, packageId,
                ScheduleDto::isNonHistorical, StandardJob.class);
    }

    public CronTriggerDto scheduleHistorical(CronTriggerDto cronTriggerDto) throws SchedulerException {
        log.info("Received CronTriggerDtos: {}", cronTriggerDto);

        Objects.requireNonNull(cronTriggerDto, "CronTriggerDto must be provided");
        isUuid(cronTriggerDto.getPackageId());
        cronTriggerDto.getSchedules().forEach(scheduleDto -> isUuid(scheduleDto.getId()));

        String packageId = ScheduleUtils.convertToHistoricalGroupName(cronTriggerDto.getPackageId());

        return scheduleTriggersByClassAndPredicate(cronTriggerDto, packageId,
                ScheduleDto::isHistorical, HistoricalJob.class);
    }

    private CronTriggerDto scheduleTriggersByClassAndPredicate(CronTriggerDto cronTriggerDto,
                                                               String packageId,
                                                               Predicate<ScheduleDto> predicate,
                                                               Class<? extends Job> jobClass) throws SchedulerException {
        CronTriggerDto scheduledCronTrigger = CronTriggerDto.builder().packageId(packageId)
                .schedules(cronTriggerDto.getSchedules().stream()
                        .filter(predicate).collect(Collectors.toList())).build();
        for (ScheduleDto scheduleDto : scheduledCronTrigger.getSchedules()) {
            this.scheduleJob(packageId, scheduleDto, jobClass);
        }
        return scheduledCronTrigger;
    }

    public CronTriggerDto getCronTriggerByPackageAndScheduleId(String packageId, String scheduleId) throws SchedulerException {
        isUuid(packageId);
        isUuid(scheduleId);
        Optional<Trigger> optionalTrigger = Optional.ofNullable(getTrigger(TriggerKey.triggerKey(scheduleId, packageId)));

        Optional<ScheduleDto> scheduleOptional = optionalTrigger.map(this::convertTriggerToScheduleDto)
                .orElseThrow(() -> new AppException(
                        String.format("Schedule with packageId: %s, scheduleId: %s does not exist", packageId, scheduleId)
                ));

        return CronTriggerDto.builder().packageId(packageId).schedules(Collections.singletonList(scheduleOptional.get())).build();
    }

    public CronTriggerDto getCronTriggersByPackageId(String packageId) throws SchedulerException {
        isUuid(packageId);
        List<ScheduleDto> scheduleDtos = getSchedulesByGroupName(packageId);
        return CronTriggerDto.builder().packageId(packageId).schedules(scheduleDtos).build();
    }

    private Optional<ScheduleDto> convertTriggerToScheduleDto(Trigger trigger) {
        return convertTriggerKeyToScheduleDto(trigger.getKey());
    }

    private Optional<ScheduleDto> convertTriggerKeyToScheduleDto(TriggerKey triggerKey) {
        try {
            JobDetail jobDetail = scheduler.getJobDetail(JobKey.jobKey(triggerKey.getName(), triggerKey.getGroup()));
            return Optional.of(generateScheduleDtoFromJobDetail(jobDetail));
        } catch (SchedulerException ex) {
            return Optional.empty();
        }
    }

    private ScheduleDto generateScheduleDtoFromJobDetail(JobDetail jobDetail) {
        ScheduleDto.ScheduleDtoBuilder builder = ScheduleDto.builder();
        builder.id(jobDetail.getKey().getName());
        JobDataMap jobDataMap = jobDetail.getJobDataMap();
        if (jobDataMap.containsKey(START_TIME)) {
            builder.start(ScheduleUtils.convertStringToLocalDateTime(jobDataMap.getString(START_TIME)));
        }

        if (jobDataMap.containsKey(END_TIME)) {
            builder.end(ScheduleUtils.convertStringToLocalDateTime(jobDataMap.getString(END_TIME)));
        }

        if (jobDataMap.containsKey(EXPRESSION_FIELD)) {
            builder.cron(jobDataMap.getString(EXPRESSION_FIELD));
        }

        builder.depth(DepthType.getType(jobDataMap.getString(DEPTH_FIELD)));
        builder.lag(jobDataMap.getString(LAG_FIELD));

        return builder.build();
    }

    public void scheduleJob(String packageId, ScheduleDto scheduleDto, Class<? extends Job> jobClass) throws SchedulerException {
        JobSchedulerService<? extends Job> jobSchedulerService = getJobSchedulerInstance(jobClass);
        JobDetail jobDetail = jobSchedulerService.createJobDetail(packageId, scheduleDto);
        Trigger trigger = jobSchedulerService.createTrigger(packageId, scheduleDto, jobDetail);
        if (scheduler.checkExists(jobDetail.getKey())) {
            log.info("Exists packageId: {}, scheduleId: {} - overriding", packageId, scheduleDto.getId());
            Trigger oldTrigger = scheduler.getTrigger(trigger.getKey());
            JobDataMap jobDataMap = trigger.getJobDataMap();
            oldTrigger.getJobDataMap().forEach((key, value) -> {
                if (!jobDataMap.containsKey(key)) {
                    jobDataMap.put(key, value);
                }
            });
            scheduler.addJob(jobDetail, true, true);
            scheduler.rescheduleJob(trigger.getKey(), trigger);
        } else {
            log.info("Scheduling new job: packageId: {}, scheduleId: {}", packageId, scheduleDto.getId());
            scheduler.scheduleJob(jobDetail, trigger);
        }
    }

    private JobSchedulerService<? extends Job> getJobSchedulerInstance(Class<? extends Job> jobClass) {
        return Stream.of(standardJobScheduler, historicalJobScheduler).filter(instance -> instance.isSupported(jobClass))
                .findFirst().orElseThrow(() -> new AppException("Provided class is not supported"));
    }

    public void unscheduleJobsByPackageId(String packageId) throws SchedulerException {
        log.info("Unscheduling all jobs for packageId: {}", packageId);
        getTriggerKeysByPackageIdAndPredicate(packageId, EMPTY_TRIGGER_KEY_PREDICATE);
    }

    public void unscheduleJob(String packageId, String scheduleId) throws SchedulerException {
        log.info("Unscheduling Jobs for packageId: {}, scheduleId: {}", packageId, scheduleId);
        getTriggerKeysByPackageIdAndPredicate(packageId, triggerKey -> scheduleId.equals(triggerKey.getName()));
    }

    private void getTriggerKeysByPackageIdAndPredicate(String packageId, Predicate<TriggerKey> predicate) throws SchedulerException {
        List<TriggerKey> triggerKeysList = getTriggerKeysByPackageId(packageId)
                .stream().filter(predicate).collect(Collectors.toList());

        scheduler.unscheduleJobs(triggerKeysList);
    }

    public CronTriggerDto getHistoricalCronTriggersByPackageId(String packageId) throws SchedulerException {
        isUuid(packageId);
        packageId = convertToHistoricalGroupName(packageId);
        List<ScheduleDto> scheduleDtos = getSchedulesByGroupName(packageId);
        return CronTriggerDto.builder().packageId(packageId).schedules(scheduleDtos).build();
    }

    private List<ScheduleDto> getSchedulesByGroupName(String packageId) throws SchedulerException {
        return getTriggerKeysByPackageId(packageId)
                .stream()
                .map(this::convertTriggerKeyToScheduleDto)
                .flatMap(Optional::stream)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private Set<TriggerKey> getTriggerKeysByPackageId(String packageId) throws SchedulerException {
        return scheduler.getTriggerKeys(GroupMatcher.groupEquals(packageId));
    }

    private Trigger getTrigger(TriggerKey triggerKey) throws SchedulerException {
        return scheduler.getTrigger(triggerKey);
    }

    public void enableSchedulesForPackageId(String packageId) throws SchedulerException {
        skipExecutionToggle(packageId, jobDataMap -> true, SKIP_EXECUTION_OFF);
    }

    public void disableScheduleForPackage(String packageId) throws SchedulerException {
        skipExecutionToggle(packageId, jobDataMap -> jobDataMap.containsKey(SKIP_EXECUTION_ON), SKIP_EXECUTION_ON);
    }

    private void skipExecutionToggle(String packageId, Predicate<JobDataMap> predicate, String setTo) throws SchedulerException {
        for (TriggerKey triggerKey : getTriggerKeysByPackageId(packageId)) {
            Trigger trigger = getTrigger(triggerKey);
            if (predicate.test(trigger.getJobDataMap())) {
                if (setTo.equals(SKIP_EXECUTION_ON) && trigger.getJobDataMap().containsKey(SKIP_EXECUTION_OFF)) {
                    trigger.getJobDataMap().remove(SKIP_EXECUTION_OFF);
                }
                trigger.getJobDataMap().put(SKIP_EXECUTION_FIELD, setTo);
                trigger.getJobDataMap().put(setTo, ScheduleUtils.convertLocalDateTimeToString(LocalDateTime.now()));
                scheduler.rescheduleJob(triggerKey, trigger);
            }
        }
    }

    public CronTriggerDto fireSchedules(String groupName) throws SchedulerException {
        List<ScheduleDto> scheduleDtos = new LinkedList<>();
        for (TriggerKey triggerKey : getTriggerKeysByPackageId(groupName)) {
            Trigger trigger = getTrigger(triggerKey);
            ScheduleDto scheduleDto = convertTriggerKeyToScheduleDto(triggerKey).get();
            LocalDateTime endDate = retrieveScheduleEndDate(scheduleDto);
            Trigger newTrigger = trigger.getTriggerBuilder().startAt(convertLocalDateTimeToDate(endDate)).build();
            scheduler.rescheduleJob(triggerKey, newTrigger);
            scheduleDtos.add(scheduleDto);
        }
        return CronTriggerDto.builder().packageId(groupName).schedules(scheduleDtos).build();
    }

    private LocalDateTime retrieveScheduleEndDate(ScheduleDto scheduleDto) {
        LocalDateTime now = LocalDateTime.now();
        if (scheduleDto.getEnd() == null || scheduleDto.getEnd().isAfter(now)) {
            scheduleDto.setEnd(now);
        }

        return scheduleDto.getEnd();
    }
}
