package com.schedule.service.jobscheduler;

import com.schedule.model.ScheduleDto;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;

public interface JobSchedulerService<T extends Job> {
    JobDetail createJobDetail(String packageId, ScheduleDto scheduleDto);

    Trigger createTrigger(String packageId, ScheduleDto scheduleDto, JobDetail jobDetail);

    default JobDataMap generateScheduleDataMap(ScheduleDto scheduleDto) {
        return new JobDataMap(scheduleDto.getScheduleMap());
    }

    default boolean isSupported(Class<? extends Job> jobClass) {
        return getClassType() == jobClass;
    }

    Class<T> getClassType();
}
