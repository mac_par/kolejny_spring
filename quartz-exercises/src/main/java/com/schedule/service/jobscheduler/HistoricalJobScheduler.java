package com.schedule.service.jobscheduler;

import com.schedule.jobs.HistoricalJob;
import com.schedule.model.ScheduleDto;
import com.schedule.utils.SchedulerFields;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static com.schedule.utils.ScheduleUtils.convertLocalDateTimeToString;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

@Service
public class HistoricalJobScheduler implements JobSchedulerService<HistoricalJob> {

    @Override
    public JobDetail createJobDetail(String packageId, ScheduleDto scheduleDto) {
        JobDataMap jobDataMap = generateScheduleDataMap(scheduleDto);
        jobDataMap.put(SchedulerFields.NEW_JOB, SchedulerFields.NEW_JOB_INITIAL_VALUE);
        return newJob(getClassType()).withIdentity(scheduleDto.getId(), packageId)
                .requestRecovery().usingJobData(jobDataMap)
                .storeDurably(false).build();
    }

    @Override
    public Trigger createTrigger(String packageId, ScheduleDto scheduleDto, JobDetail jobDetail) {
        return newTrigger().withIdentity(scheduleDto.getId(), packageId)
                .forJob(jobDetail)
                .usingJobData(SchedulerFields.CREATION_TIME, convertLocalDateTimeToString(LocalDateTime.now()))
                .build();
    }

    @Override
    public Class<HistoricalJob> getClassType() {
        return HistoricalJob.class;
    }
}
