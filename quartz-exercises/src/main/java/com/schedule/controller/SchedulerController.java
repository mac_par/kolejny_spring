package com.schedule.controller;

import com.schedule.model.CronTriggerDto;
import com.schedule.service.SchedulerService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.schedule.utils.ScheduleUtils.convertToHistoricalGroupName;

@Slf4j
@RestController
public class SchedulerController {

    private final SchedulerService schedulerService;

    @Autowired
    public SchedulerController(SchedulerService schedulerService) {
        this.schedulerService = schedulerService;
    }

    @PostMapping("cron")
    ResponseEntity<CronTriggerDto> scheduleCron(@RequestBody CronTriggerDto cronTriggerDto) throws SchedulerException {
        log.info("Scheduling: {}", cronTriggerDto);

        CronTriggerDto scheduledTriggers = schedulerService.schedule(cronTriggerDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(scheduledTriggers);
    }

    @PostMapping("historical")
    ResponseEntity<CronTriggerDto> scheduleHistoricalCron(@RequestBody CronTriggerDto cronTriggerDto) throws SchedulerException {
        log.info("Scheduling Historical: {}", cronTriggerDto);

        CronTriggerDto scheduledTriggers = schedulerService.scheduleHistorical(cronTriggerDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(scheduledTriggers);
    }

    @PutMapping("historical/{packageId}")
    ResponseEntity<CronTriggerDto> fireHistorical(@PathVariable String packageId) throws SchedulerException {
        log.info("Firing Historical schedules for packageId: {}", packageId);

        CronTriggerDto firedCronTriggers = schedulerService.fireSchedules(convertToHistoricalGroupName(packageId));
        return ResponseEntity.status(HttpStatus.OK).body(firedCronTriggers);
    }

    @GetMapping("/{packageId}")
    ResponseEntity<CronTriggerDto> getTriggers(@PathVariable String packageId) throws SchedulerException {
        log.info("Retrieving schedules for packageId: {}", packageId);


        CronTriggerDto cronTrigger = schedulerService.getCronTriggersByPackageId(packageId);
        return ResponseEntity.ok(cronTrigger);
    }

    @GetMapping("/historical/{packageId}")
    ResponseEntity<CronTriggerDto> getHistoricalTriggers(@PathVariable String packageId) throws SchedulerException {
        log.info("Retrieving historical schedules for packageId: {}", packageId);


        CronTriggerDto cronTrigger = schedulerService.getHistoricalCronTriggersByPackageId(packageId);
        return ResponseEntity.ok(cronTrigger);
    }


    @GetMapping("/{packageId}/{scheduleId}")
    ResponseEntity<CronTriggerDto> getTrigger(@PathVariable String packageId,
                                              @PathVariable String scheduleId) throws SchedulerException {
        log.info("Retrieving Schedule by packageId: {}, scheduleId: {}", packageId, scheduleId);

        CronTriggerDto triggerDto = schedulerService.getCronTriggerByPackageAndScheduleId(packageId, scheduleId);
        return ResponseEntity.ok(triggerDto);
    }


    @DeleteMapping("/{packageId}")
    ResponseEntity<?> deleteTriggers(@PathVariable String packageId) throws SchedulerException {
        log.info("Deleting schedules for packageId: {}", packageId);

        schedulerService.unscheduleJobsByPackageId(packageId);
        return ResponseEntity.noContent().build();
    }


    @DeleteMapping("/{packageId}/{scheduleId}")
    ResponseEntity<?> deleteTrigger(@PathVariable String packageId,
                                    @PathVariable String scheduleId) throws SchedulerException {
        log.info("Deleting schedule - packageId: {}, scheduleId: {}", packageId, scheduleId);

        schedulerService.unscheduleJob(packageId, scheduleId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/redlight/{packageId}")
    ResponseEntity<?> redlight(@PathVariable String packageId) throws SchedulerException {
        log.info("Setting schedules for packageId: {} into disabled mode", packageId);

        schedulerService.disableScheduleForPackage(packageId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/greenlight/{packageId}")
    ResponseEntity<?> greenlight(@PathVariable String packageId) throws SchedulerException {
        log.info("Setting schedules for packageId: {} into disabled mode", packageId);

        schedulerService.enableSchedulesForPackageId(packageId);
        return ResponseEntity.noContent().build();
    }
}
