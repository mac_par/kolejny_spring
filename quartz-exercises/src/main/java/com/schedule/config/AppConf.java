package com.schedule.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.log4j.Log4j2;
import org.quartz.TriggerListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;

@Log4j2
@Configuration
public class AppConf {

    @Value("${quartz.config}")
    private String quartzConfig;

    @Bean
    DataSource dataSource(DbProperties dbProps) {
        HikariConfig poolConfig = new HikariConfig();
        poolConfig.setDriverClassName(dbProps.getDriverClass());
        poolConfig.setJdbcUrl(dbProps.getUrl());
        poolConfig.setUsername(dbProps.getUsername());
        poolConfig.setPassword(dbProps.getPassword());
        poolConfig.setPoolName(dbProps.getPoolName());
        poolConfig.setMinimumIdle(dbProps.getMinimumIdle());
        poolConfig.setMaximumPoolSize(dbProps.getMaximumPoolSize());
        HikariDataSource hikariDataSource = new HikariDataSource(poolConfig);
        return hikariDataSource;
    }

    @Bean
    SchedulerFactoryBean schedulerFactory(DataSource dataSource, TriggerListener triggerListener)
            throws Exception {
        SchedulerFactoryBean factoryBean = new SchedulerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setAutoStartup(true);
        factoryBean.setConfigLocation(new ClassPathResource(quartzConfig));
        factoryBean.setGlobalTriggerListeners(triggerListener);
        factoryBean.afterPropertiesSet();
        return factoryBean;
    }

    @Bean
    ObjectMapper objectMapper() {
        return new ObjectMapper().registerModule(new JavaTimeModule()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }
}
