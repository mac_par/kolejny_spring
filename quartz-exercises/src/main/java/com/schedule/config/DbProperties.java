package com.schedule.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@NoArgsConstructor
@ConfigurationProperties(prefix = "database")
@Component
public class DbProperties {
    private String url;
    private String driverClass;
    private String username;
    private String password;
    private String poolName;
    private int minimumIdle = 10;
    private int maximumPoolSize = 15;
}
