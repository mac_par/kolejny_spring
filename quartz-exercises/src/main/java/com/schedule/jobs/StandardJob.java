package com.schedule.jobs;

import lombok.extern.slf4j.Slf4j;
import org.quartz.*;

import java.time.LocalDateTime;

import static com.schedule.utils.ScheduleUtils.convertDateToLocalDateTime;
import static com.schedule.utils.ScheduleUtils.convertLocalDateTimeToString;

@Slf4j
public class StandardJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("Executing 'Standard Job'");
        JobKey jobKey = context.getJobDetail().getKey();
        String packageId = jobKey.getGroup();
        String scheduleId = jobKey.getName();
        LocalDateTime scheduledFireTime = convertDateToLocalDateTime(context.getScheduledFireTime());
        LocalDateTime firedTime = convertDateToLocalDateTime(context.getFireTime());
        JobDataMap jobDataMap = context.getMergedJobDataMap();
        log.info("'Standard Job': packageId: {}, scheduleId: {}", packageId, scheduleId);
        try {
            log.info("ScheduledFireTime: {}, FiredAt: {}", convertLocalDateTimeToString(scheduledFireTime),
                    convertLocalDateTimeToString(firedTime));
            jobDataMap.forEach((key, value) -> log.info("DataMap: [{}, {}]", key, value));
            log.info("'StandardJob was executed'");
        } catch (Exception ex) {
            log.error("Exception on execution", ex);
            throw new JobExecutionException(ex.getMessage());
        }
    }
}
