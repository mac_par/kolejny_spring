package com.schedule.jobs;

import lombok.extern.slf4j.Slf4j;
import org.quartz.*;

import java.time.LocalDateTime;

import static com.schedule.utils.ScheduleUtils.*;

@Slf4j
public class HistoricalJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("Executing 'Historical Job'");
        JobKey jobKey = context.getJobDetail().getKey();
        String historicalGroupId = jobKey.getGroup();
        String packageId = extractPackageIdFromHistoricGroupName(historicalGroupId);
        String scheduleId = jobKey.getName();
        LocalDateTime scheduledFireTime = convertDateToLocalDateTime(context.getScheduledFireTime());
        LocalDateTime firedTime = convertDateToLocalDateTime(context.getFireTime());
        JobDataMap jobDataMap = context.getMergedJobDataMap();
        log.info("'Historical Job': historicalGroupId: {}, scheduleId: {}, packageId: {} ", historicalGroupId,
                scheduleId, packageId);
        try {
            log.info("ScheduledFireTime: {}, FiredAt: {}", convertLocalDateTimeToString(scheduledFireTime),
                    convertLocalDateTimeToString(firedTime));
            jobDataMap.forEach((key, value) -> log.info("DataMap: [{}, {}]", key, value));
            log.info("'HistoricalJob was executed'");
        } catch (Exception ex) {
            log.error("Exception on execution", ex);
            throw new JobExecutionException(ex.getMessage());
        }
    }
}
