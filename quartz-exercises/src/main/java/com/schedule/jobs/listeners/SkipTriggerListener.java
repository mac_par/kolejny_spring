package com.schedule.jobs.listeners;

import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.TriggerListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static com.schedule.utils.ScheduleUtils.convertDateToLocalDateTime;
import static com.schedule.utils.ScheduleUtils.convertStringToLocalDateTime;
import static com.schedule.utils.SchedulerFields.*;

@Slf4j
@Component
public class SkipTriggerListener implements TriggerListener {
    @Override
    public String getName() {
        return "SkipTriggerListener";
    }

    @Override
    public void triggerFired(Trigger trigger, JobExecutionContext context) {
    }

    @Override
    public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context) {
        boolean vetoed = false;
        if (trigger.getJobDataMap().containsKey(SKIP_EXECUTION_FIELD)) {
            vetoed = trigger.getJobDataMap().getBooleanFromString(SKIP_EXECUTION_FIELD);
            log.info("{} is defined: {}", SKIP_EXECUTION_FIELD, vetoed);
        }

        String group = trigger.getKey().getGroup();
        String name = trigger.getKey().getName();
        if (!vetoed && trigger.getJobDataMap().containsKey(SKIP_EXECUTION_ON) && trigger.getJobDataMap().containsKey(SKIP_EXECUTION_OFF)) {
            LocalDateTime disabledOn = convertStringToLocalDateTime(trigger.getJobDataMap().getString(SKIP_EXECUTION_ON));
            LocalDateTime enabledOn = convertStringToLocalDateTime(trigger.getJobDataMap().getString(SKIP_EXECUTION_OFF));
            LocalDateTime scheduledFireTime = convertDateToLocalDateTime(context.getScheduledFireTime());
            log.info("PackageId: {}, ScheduleId: {}. DisabledOn: {}, EnabledOn: {}, ScheduledOn:{}", group, name,
                    disabledOn, enabledOn, scheduledFireTime);
            vetoed = scheduledFireTime.compareTo(disabledOn) >= 0 && scheduledFireTime.compareTo(enabledOn) <= 0;
        }
        log.info("PackageId: {}, ScheduleId: {} is vetoed: {}", group, name, vetoed);
        return vetoed;
    }

    @Override
    public void triggerMisfired(Trigger trigger) {

    }

    @Override
    public void triggerComplete(Trigger trigger, JobExecutionContext context, Trigger.CompletedExecutionInstruction triggerInstructionCode) {

    }
}
