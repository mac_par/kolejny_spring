package com.schedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
public class QuartzApp {
    public static void main(String[] args) {
        SpringApplication.run(QuartzApp.class, args);
    }
}
