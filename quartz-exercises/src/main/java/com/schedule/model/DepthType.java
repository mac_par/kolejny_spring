package com.schedule.model;

import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.ObjectUtils;

public enum DepthType {
    EVERYTHING("everything"),
    DELTA("delta");

    private final String depthType;

    private DepthType(String depthType) {
        this.depthType = depthType;
    }

    @JsonValue
    public String getDepthType() {
        return depthType;
    }

    public static DepthType getType(String depthType) {
        if (ObjectUtils.isEmpty(depthType)) {
            throw new IllegalArgumentException("Depth Type must be provided");
        }
        for (DepthType instance : DepthType.values()) {
            if (instance.depthType.equals(depthType)) {
                return instance;
            }
        }

        throw new IllegalArgumentException("Provided DepthType does not exist");
    }

    public static void main(String[] args) {
        System.out.println(DepthType.getType("everything"));
    }
}
