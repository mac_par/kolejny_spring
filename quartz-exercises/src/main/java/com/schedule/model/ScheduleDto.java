package com.schedule.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.schedule.utils.ScheduleUtils;
import lombok.*;
import org.apache.commons.lang3.ObjectUtils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.schedule.model.DepthType.EVERYTHING;
import static com.schedule.utils.ScheduleUtils.DATE_TIME_FORMAT;
import static com.schedule.utils.SchedulerFields.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScheduleDto {
    @JsonIgnore
    private static final String DEFAULT_LAG = "PT5M";

    private String id;
    private String cron;
    @JsonFormat(pattern = DATE_TIME_FORMAT)
    private LocalDateTime start;
    @JsonFormat(pattern = DATE_TIME_FORMAT)
    private LocalDateTime end;
    private DepthType depth;
    private String lag;

    @JsonIgnore
    public boolean isNonHistorical() {
        return start == null || (end == null || end.isAfter(LocalDateTime.now()));
    }

    @JsonIgnore
    public boolean isHistorical() {
        return start != null;
    }

    @JsonIgnore
    public Map<String, String> getScheduleMap() {
        Map<String, String> map = new HashMap<>();
        if (ObjectUtils.isNotEmpty(cron)) {
            map.put(EXPRESSION_FIELD, cron);
        }
        if (!Objects.isNull(start)) {
            map.put(START_TIME, ScheduleUtils.convertLocalDateTimeToString(start));
        }
        if (!Objects.isNull(end)) {
            map.put(END_TIME, ScheduleUtils.convertLocalDateTimeToString(end));
        }

        if (Objects.isNull(depth)) {
            depth = EVERYTHING;
        }
        map.put(DEPTH_FIELD, depth.getDepthType());

        if (ObjectUtils.isEmpty(lag)) {
            lag = DEFAULT_LAG;
        }

        map.put(LAG_FIELD, lag);
        return map;
    }
}
