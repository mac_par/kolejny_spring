package com.schedule.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CronTriggerDto {
    private String packageId;
    private List<ScheduleDto> schedules;
}
